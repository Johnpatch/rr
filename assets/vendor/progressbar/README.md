# rapBar
Jquery clickable progress bar plugin can work also as slider.

Try it out <a href="https://thibor.github.io/jsRapBar/">here</a>.

More information about this can be found in this blog <a href="https://www.jqueryscript.net/loading/Dynamic-Progress-Bar-jsRapBar.html">article</a>.
