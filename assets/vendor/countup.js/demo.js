// import {CountUp} from "./dist/CountUp.js";

window.onload = function () {

    var endVal, options,demo;



    createCountUp();


    // COUNTUP AND CODE VISUALIZER
    function createCountUp() {
        establishOptionsFromInputs();
        demo = new CountUp('counter_section', endVal, options);
        console.log(demo);
        if (!demo.error) {
            demo.start();
        }
    }

    function establishOptionsFromInputs() {
        endVal = 5000;
        options = {
            startVal: 0,
            decimalPlaces: 2,
            duration: 5,
            useEasing: true,
            useGrouping:true,
            easingFn: null,
            separator: ',',
            decimal: '.',
            prefix: null,
            suffix: null,
            numerals: null
        };
        // unset null values so they don't overwrite defaults
        for (var key in options) {
            if (options.hasOwnProperty(key)) {
                if (options[key] === null) {
                    delete options[key];
                }
            }
        }
    }
}