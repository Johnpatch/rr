var current_pos = 0;
var roll_stop = 0;
var randomize_value = 0;

var defualt_roll_width = 109;
var roulette_bar_width = 42*defualt_roll_width;
var is_distance_short = 0;
var Roulette = function () {

	var maxDistance;
	var isSlowdown = false;
	var distance = 0;
	var runUpDistance;
	var slowDownTimer = null;
	var duriation;
	var start_pos;
	var isRunUp = true;
	var speed;
	var number_array = [];
	var pos_array = [];
	var stop_number;
	var hash;
	var isRunning = false;
	var type;
	var lastPoints;
	
	var roundResult;

	var checkagain = function(){
		var background_position = current_pos;

		$('div.case.new').css('background-position', background_position + 'px');

		data_backup = 0;
		roll_stop = 1;
	}

	var init = function(data){
		roll_stop = 0;
		if(isNaN(current_pos)){
			current_pos = 0;
		}
		
		current_pos = current_pos;
		start_pos = current_pos;
		runUpDistance = roulette_bar_width;
		
		speed = 30;	
		duriation = 1.5;
		is_distance_short = 0;
		number_array = [8,9,10,11,12,7,13,14,15,16,17,5,18,19,20,21,22,6,23,24,25,26,27,1,28,29,30,31,32,2,33,34,35,36,37,3,38,39,40,41,42,4];
		pos_array = [23,29,35,41,11,17,5,0,1,2,3,4,6,7,8,9,10,12,13,14,15,16,18,19,20,21,22,24,25,26,27,28,30,31,32,33,34,36,37,38,39,40];
		stop_number = data[4];
		roundResult = data;
	}
    // Handles quick sidebar toggler
    

    var reset = function () {
		current_pos %= roulette_bar_width;		
		
    	start_pos = current_pos;
		isStop = false;
		slowDownTimer = null;
		isRunUp = true;
		distance = 0;
		maxDistance = 0;
		
		speed = 30;	
		isSlowdown = false;
		is_distance_short = 0;
    };
	var pre_speed = 0;
    var roll = function () {
			
		speed_ = speed;
		
		if (isRunUp) {
			if (distance <= runUpDistance) {
				speed_ = 30;
			} else {
				isRunUp = false;
			}
		} else if (isSlowdown) {
			
			if(maxDistance - distance <= 1600 && maxDistance - distance >= 1100){
				if(is_distance_short == 0){
					maxDistance += 4578
					is_distance_short = 1;
				}else{
					speed_ = 14;	
				}
			}
			else if(maxDistance - distance <= 1100 && maxDistance - distance >= 600){
				if(is_distance_short == 0){
					maxDistance += 4578
					is_distance_short = 1;
				}else{
					speed_ = 8;	
				}
			}
			else if(maxDistance - distance <= 600 && maxDistance - distance >= 300){
				if(is_distance_short == 0){
					maxDistance += 4578
					is_distance_short = 1;
				}else{
					speed_ = 4;
				}
			}
			else if(maxDistance - distance <= 300 && maxDistance - distance >= 103){
				if(is_distance_short == 0){
					maxDistance += 4578
					is_distance_short = 1;
				}else{
					speed_ = 2;
				}
			}
			else if(maxDistance - distance <= 103 && maxDistance - distance >= 48){
				if(is_distance_short == 0){
					maxDistance += 4578
					is_distance_short = 1;
				}else{
					speed_ = 0.9;
				}
			}
			else if(maxDistance - distance < 48 && maxDistance - distance >= 18){
				if(is_distance_short == 0){
					maxDistance += 4578
					is_distance_short = 1;
				}else{
					speed_ = 0.6;
				}
			}
			else if(maxDistance - distance <  18){
				if(is_distance_short == 0){
					maxDistance += 4578
					is_distance_short = 1;
				}else{
					speed_ = 0.3;
				}
			}
			else{
				speed_ = 20;
				is_distance_short = 1;
			}
		}

		if (maxDistance && distance >= maxDistance) {
			return;

		}

		distance += speed_;

		current_pos -= speed_;
		if(pre_speed != speed_){
			pre_speed = speed_;
			//console.log(pre_speed);
		}
		var wheel_width = $('.wheel-container').width();
	
		var background_position = current_pos;
		
		$('div.case.new').css('background-position', background_position + 'px');


    };

    var start = function () {
    	//console.log(stop_number);
		isRunning = true;
		roll();

		slowDownTimer = setTimeout(function(){			
			slowDownSetup();
		}, duriation * 1000);
    };

    var slowDownSetup = function () {
		if(isSlowdown){
			return;
		}

		lastPoints = 0;
	    //p.slowDownCallback();
		
		isSlowdown = true;
		slowDownStartDistance = distance;
		
		var len , index2 , diff;

		len = distance - start_pos;

		index2 = pos_array[stop_number-1];
		
		var start_temp = roulette_bar_width - Math.abs(start_pos) - Math.ceil($('.wheel-container').width()/2);
		var temp = distance % roulette_bar_width;
		if(start_temp < temp)
			temp = roulette_bar_width - temp + start_temp;
		else
			temp = start_temp - temp;
		
		maxDistance = index2 * defualt_roll_width + randomize_value + distance + temp;
    };


    return {
        reset: function(){
        	reset();
        },
        checkagain: function(){
			checkagain();
		},
        roll: function(){
        	roll();
        },
      	start: function(){
    		start();
      	},
      	slowDownSetup: function(){
      		slowDownSetup();
      	} ,
      	init: function(data){
      		init(data);
      	}
    };

}();
