<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Home_Model');
    }
    
	public function index()
	{
		$ip = $this->input->ip_address();
		$this->Home_Model->reg_ip($ip);
		$this->load->view('Template/header');
		$this->load->view('home');
		$this->load->view('Template/footer');
	}	
	
	public function reg_nickname(){
		$data = $_POST;
		$is_exist = $this->Home_Model->is_exist_nickname($data['nickname']);
		if($is_exist['cnt'] == 0){
			$is_email_exist['cnt'] = 0;
			if(!empty($data['email']))
				$is_email_exist = $this->Home_Model->is_exist_email($data['email']);
			if($is_email_exist['cnt'] != 0){
				$data['success'] = "email_exist";
			}else{
				$data['password'] = md5($data['password']);
				$result = $this->Home_Model->reg_nickname($data);
				$data['info'] = $result;
				$data['success'] = "true";
				$this->session->set_userdata('user_profile',$result);	
			}
		}
		else{
			$data['success'] = "false";
		}
		echo json_encode($data);
	}
	
	public function register_instant($nickname){
		$is_exist = $this->Home_Model->is_exist_nickname($nickname);
		if($is_exist['cnt'] == 0){
			$this->Home_Model->reg_nickname1($nickname);
			$data['success'] = "true";
		}
		else{
			$data['success'] = "false";
		}
		echo json_encode($data);
	}
	
	public function reg_email(){
		$data = $_POST;
		if(!empty($data['email']))
			$is_exist = $this->Home_Model->is_exist_email($data['email']);
		else
			$is_exist['cnt'] = 0;
		if($is_exist['cnt'] == 0){
			$data['password'] = md5($data['password']);
			$result = $this->Home_Model->reg_email_pwd($data);
			$this->session->set_userdata('user_profile',$result);
			$data['info'] = $result;
			$data['success'] = "true";
		}
		else{
			$data['success'] = "false";
		}
		echo json_encode($data);
	}
	
	public function change_password(){
		$data = $_POST;
		$is_same = $this->Home_Model->is_same_password($data);
		if(empty($is_same)){
			$data['success'] = "not_same";
		}else{
			$this->Home_Model->update_password($data);
			$data['success'] = "true";
		}
		echo json_encode($data);
	}
	
	public function change_email(){
		$data = $_POST;
		$is_exist['cnt'] = 0;
		if(!empty($data['email']))
			$is_exist = $this->Home_Model->is_exist_email($data['email']);
		if($is_exist['cnt'] == 0){
			$result = $this->Home_Model->reg_email($data);
			$this->session->set_userdata('user_profile',$result);
			$data['success'] = "true";
		}
		else{
			$data['success'] = "email_exist";
		}
		echo json_encode($data);
	}
	
	public function login(){
		$data = $_POST;
		$user_info = $this->Home_Model->is_login($data);
		if(!empty($user_info)){
			if($user_info['password'] == md5($data['password'])){
				$data['success'] = "true";
				$data['data'] = $user_info;
				$this->session->set_userdata('user_profile',$user_info);
			}
			else
				$data['success'] = "wrong_pwd";
		}else{
			$data['success'] = "false";
		}
		echo json_encode($data);
	}
	
	public function get_user($user_id){
		$user_info = $this->Home_Model->get_user_info($user_id);
		$data['success'] = "true";
		$data['data'] = $user_info;
		echo json_encode($data);
	}
	
	public function update(){
		$data = $_POST;
		$this->Home_Model->update_user_info($data,$data['user_id']);
	}
	
	public function update_exp(){
		$data = $_GET;
		$user_info = $this->Home_Model->get_user_info($data['user_id']);
		$update_exp = (int)$data['experience'] + (int)$user_info['experience'];
		$level = $user_info['level'];
		if($update_exp < 5000)
			$level = floor($update_exp/100);
		else if($update_exp < 10000)
			$level = floor(($update_exp-5000)/500)+50;
		else if($update_exp < 35000)
			$level = floor(($update_exp-10000)/1000)+60;
		else if($update_exp < 110000)
			$level = floor(($update_exp-35000)/5000)+85;
		else if($update_exp < 360000)
			$level = floor(($update_exp-110000)/10000)+100;
		else
			$level = floor(($update_exp-360000)/50000)+125;
		
		$data['level'] = $level;
		$result = $this->Home_Model->update_user_exp_info($data,$data['user_id']);
		if($result['bet_time'] == 100){
			$this->Home_Model->update_free_spin($data['user_id']);
		}
		$data['data'] = $result;
		echo json_encode($data);
	}
	
	public function get_vgo_items(){
		//$trade_url = "https://trade.opskins.com/t/5503713/NbF0sfa9";
		//$trade_url = "https://trade.opskins.com/t/6271015/U0tnxf9L";
		$data = array();
		$trade_url = $_POST['trade_url'];
		if($_POST['type'] == 1){
			$this->Home_Model->update_trade_url($_POST);
		}
		if(!empty($trade_url)){
			$trade = explode('/',$trade_url);
			$uid = $trade[4];
			if(!empty($uid)){
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://api-trade.opskins.com/ITrade/GetUserInventory/v1?uid=$uid&app_id=1",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_SSL_VERIFYPEER => false,
				  CURLOPT_CUSTOMREQUEST => "GET",
				));
				
				$response = json_decode(curl_exec($curl));
				if(isset($response->response))
					$data['item'] = $response->response->items;
				$data['status'] = $response->status;
				if(isset($response->message))
					$data['message'] = $response->message;	
			}else{
				$data['item'] = array();
				$data['status'] = 1;
			}
		}
		else{
			$data['item'] = array();
			$data['status'] = 1;
		}
		echo json_encode($data);	
	}
	
	public function reg_nickname_instant(){
		$data = array();
		$is_exist = $this->Home_Model->is_name_exist($_POST['nickname']);
		if($is_exist)
			$data['status'] = false;
		else{
			$data['status'] = true;
		}
		echo json_encode($data);
	}
	
	public function logout(){
		session_destroy();
		redirect('');
	}
}
