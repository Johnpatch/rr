<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
        crossorigin="anonymous"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/countup.js/dist/countUp.js"></script>
<script src="<?php echo base_url();?>assets/js/socket.io.min.js" ></script>
<script src="<?php echo base_url();?>assets/js/roulette.js"></script>
<script src="<?php echo base_url();?>assets/js/soloRoulette.js"></script>
<script src="<?php echo base_url();?>assets/js/lottie.min.js"></script>
<script src="<?php echo base_url();?>assets/js/toastr.min.js"></script>
<script src="<?php echo base_url();?>assets/js/circle-progress.min.js"></script>
<script src="<?php echo base_url();?>assets/js/grasp_mobile_progress_circle-1.0.0.js"></script>
<script src="<?php echo base_url();?>assets/vendor/progressbar/jsRapBar.js"></script>
<script type="text/javascript">

	var status, root = "http://192.168.8.117/rr/", server = "http://192.168.8.117:3001", socket;
	//var status, root = "http://18.222.204.220/rr/", server = "http://18.222.204.220:3001", socket;

	var proxy = 'https://cors-anywhere.herokuapp.com/';
	var wager_list=[30,50,85,100,125,175,200,300,600,850,1000,1500,3000,6000,9000,15000,30000,60000,90000,150000,300000,600000,1000000];
	var wager_index = 7;
	var spin_available = false;
	var is_attend_round = false;
    var attend_amount = 0;
    var miss_html = shoot_html = "<div class='gun_shoot_animation'>";
    var game_type = 1;
    
    var is_cal_pre_round_result = 0;
    var is_cal_new_round_result = 0;
    var new_round_win_multipler_html = new_round_win_multipler_html_temp = "";
    var spin_round_win_multipler_html = spin_round_win_multipler_html_temp = "";
    var bonus_win_multipler_html = bonus_win_multipler_html_temp = "";
    var win_bonus_html = win_bonus_html_temp = "";
    var is_fire_spin_play_list = 0;
    var is_bonus_multipler = is_solo_bonus_multipler = 0;
    var is_bonus_multipler_result = is_solo_bonus_multipler_result = 0;
    var is_volume = 0;
    var win_countup_val = win_countup_val1 = solo_win_countup_val = solo_win_countup_val1 = total_multipler_random = 0;
    var win_countup_font_size = 0;
    var is_shoot_multipler = 0;
    var multipler_width = bonus_multipler_width = spin_multipler_width = spin_bonus_multipler_width = 0;
    var multipler_result_width = bonus__result_width = 0;
    var spin_multipler_result_width = spin_bonus_multipler_result_width = 0;
    var is_shot_animation = 0;
    var autoplay_spin_cnt = 0;
    var cur_autoplay_cnt = 0;
    var is_autoplay = 0;
    var layout_mode = 1;
    var is_show_spin_remain = 0;
    var is_freespin_round = is_solo_freespin_round = 0;
    var total_multipler = 0;
    var register_instant = '';
    var test_interval = '';
    var my_cash = 0;
    var my_exp_val = 0;
    var new_gun_animation_index = 0;
    var gun_index = 0;
    var current_size = 0;
    var animItem, animItem_shoot,animItem_progress;
    var solo_seed = '';
    var solo_wager = 0;
    var solo_remain_freespin_cnt = 0;
    var is_solo_attend = false;
    var total_solo_freespin_wager = 0;
    var pre_round_result = null;
    var mul_margin_width = spin_mul_margin_width = spin_bonus_mul_margin_width = 0;
    var percent_val = space_bet_mul = 0;
    var total_mul_font = 0;

    //==========This is loading progress bar functions======================
    /*var percent=0;
    var interval = setInterval(function(){
        if(percent<=1){
            $("#progress").jsRapBar({position:percent,enabled:false,barColor:'#F8BF60',backColor:'#191E24',width:'60%',height:'30px'});
            percent+=0.004;
        }else {
            clearInterval(interval);
            $("#progress").css('display','none');
        }
    },100)*/

    function start(){
        /*if(percent<1){
            percent = 1;
        }*/
	}
//=============================end of progress bar functions=================

	$(document).ready(function () {

        var miss_container = document.getElementById('iframe_section');
		var shoot_container = document.getElementById('iframe_section_shoot');

		bodymovin.setLocationHref(location.href);

		$('#security_email').val(sessionStorage.getItem('email'));

		animItem = bodymovin.loadAnimation({
		  wrapper: miss_container,
		  animType: 'svg',
		  loop: false,
		  autoplay: false,
		  path: root+'assets/js/Miss1/data.json'
		});	
		animItem_shoot = bodymovin.loadAnimation({
		  wrapper: shoot_container,
		  animType: 'svg',
		  loop: false,
		  autoplay: false,
		  path: root+'assets/js/Shoot1/data.json'
		});
		
		$('#loginmodal').on('shown.bs.modal', function() {
			$(this).find('#login_name').focus();
		});
		$('#signupmodal1').on('shown.bs.modal', function() {
			$(this).find('#signup_nickname').focus();
		});
		$('#accountmodal').on('shown.bs.modal', function() {
			$('#old_password').focus();
		});
		
		
		$('div[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("data-target") // activated tab
			if(target == '#security'){
				$('#old_password').focus();
			}else if(target == '#fa'){
				$('#fa_code').focus();
			}
		});

		//bodymovin.setSubframeRendering(false);
		animItem.addEventListener('data_ready',function(){
			$("#preloader").fadeOut("fast");
		});
		animItem_shoot.addEventListener('data_ready',function(){
			$("#preloader").fadeOut("fast");
		});
		resetRoll();
		if(game_type == 1){
			$('.group_game').css('color','#F8BF60');
			$('.solo_game').css('color','white');
			$('.group_game_mobile').css('color','#F8BF60');
			$('.solo_game_mobile').css('color','white');
		} else{
			$('.group_game').css('color','white');
			$('.solo_game').css('color','#F8BF60');
			$('.group_game_mobile').css('color','white');
			$('.solo_game_mobile').css('color','#F8BF60');
		}

		sessionStorage.setItem("brag", "0");

		socket = io.connect(server);

		
		socket.on('new animation',function(data){

			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1){
				if(data[0].index == 30 || is_cal_new_round_result == 0){
					new_round_win_multipler_html = new_round_win_multipler_html_temp = "";
					is_cal_new_round_result = 1;
					my_cash = my_exp_val = 0;
					if(data[0].is_pre_shoot == 0){
						
						animItem.goToAndPlay(data[0].index, true);
						animItem_shoot.stop();
						//animItem_progress.goToAndStop(1,true);
						$('#iframe_section').css('display','block');
						$('#iframe_section_shoot').css('display','none');
					}else{
						animItem_shoot.goToAndPlay(data[0].index, true);
						animItem.stop();	
						$('#iframe_section').css('display','none');
						$('#iframe_section_shoot').css('display','block');
					}
					
					$('.indivi_mul').css('display','inline-block');
					if(data[0].is_freespin_round == 0){
						sessionStorage.setItem('wager_index', -1);
						$('.play_bottom_part').css('display','block');
						if(is_autoplay == 0 && sessionStorage.getItem("status") != null){
							change_orange('.spin_btn');
							$('.spin_btn').css('display','block');
							$('.spin_btn .fa-sync-alt').css('color','black');
							$('.autoplay_stop_btn').css('display','none');
							change_orange('#wager_mobile_sel');
							change_orange('#spin2');
						}else if(is_autoplay != 0 || sessionStorage.getItem("status") == null){
							if(sessionStorage.getItem("status") == null){
								$('.spin_btn').css('display','block');
								$('.autoplay_stop_btn').css('display','none');
								//$('.spin_btn .fa-sync-alt').css('color','black');
								change_orange('.spin_btn');
								change_grey('.mobile_play_btn');
								change_grey('#wager_mobile_sel');
								change_grey('#spin2');
							}
							else{
								change_orange('.spin_btn');
								$('.spin_btn').css('display','none');
								$('.autoplay_stop_btn').css('display','block');
								$('.spin_btn .fa-sync-alt').css('color','black');
								change_orange('#wager_mobile_sel');
								change_orange('#spin2');
							}
						}
						change_orange('.max_bet_btn');
						$('.remain_cnt').css('display','block');
						$('.bonus_spin_cnt_section').css('display','none');
						
						
						$('.fa-stop').css('color','black');
						
						change_orange('.play_btn');
						change_orange('.mobile_play_btn');
						
						sessionStorage.setItem('is_freespin_round',0);
						is_freespin_round = 0;
						
						$('.bonus_wait_section').removeClass('bonus_wait_css');
						$('.bonus_wait').css('display','none');
						$('.bonus_wait1').css('display','none');
						$('.bonus_wait_section .title').css('display','block');
						$('.bonus_wait_section .cost').css('display','inline');
						$('.bonus_wait_section .wager_set').css('display','block');
						$('.bonus_wait_section .wager_amount').css('display','block');
						
						$('.normal_round .win_multi_title').html("WIN MULTIPLER");
						$('.normal_round').css('display','block');
						$('.spin_round').css('display','none');
						$('.spin_round_trophy').css('display','none');
						$('#revolver').css('background-color','#282A30');
						$('.bodypart2-2').css('background-color','#282A30');
						
						multipler_html(data[0].chance,data[0].multipler);
						//$(".case1.new").empty();
						$(".case1.new").html(new_round_win_multipler_html_temp);
					}else{
						var width = window.innerWidth;
						if(width > 1023 && data[0].is_freespin_round == 1){	
							$('.play_bottom_part').css('display','none');
							$('.spin_btn').css('display','none');
							$('.remain_cnt').css('display','none');
							$('.autoplay_stop_btn').css('display','none');
							$('.autoplay_spin').css("display",'none');
							if($('.bonus_spin_cnt_section').css('display') != 'block')
								$('.bonus_spin_cnt_section').css('display','block');
							$('.bonus_spin_cnt').html(parseInt(data[0].remain_freespin_cnt)+" REMAINING");	
						}
						
						is_freespin_round = 1;

						if(sessionStorage.getItem('is_freespin_round') != 1){
							$('.bonus_wait_section').addClass('bonus_wait_css');
							$('.bonus_wait').css('display','inline-block');
							$('.bonus_wait1').css('display','inline-block');
							$('.bonus_wait_section .title').css('display','none');
							$('.bonus_wait_section .cost').css('display','none');
							$('.bonus_wait_section .wager_set').css('display','none');
							$('.bonus_wait_section .wager_amount').css('display','none');
						}
						
						if(sessionStorage.getItem('wager_index') == null || sessionStorage.getItem('wager_index') == -1)
							$(".curwager").html(formatNumber(wager_list[wager_index]));
						else
							$(".curwager").html(formatNumber(wager_list[sessionStorage.getItem('wager_index')]));
						
						$('.total_spin').css('display','inline-block');
						$('.spin_round').css('display','block');
						total_mul_font = 0;					
						spin_multipler_html(data[0].chance,data[0].multipler,data[0].bonus_multipler);
						bonus_multipler_html(data[0].bonus_chance,data[0].bonus_multipler,data[0].bonus_multipler);
						$('.wheel-container-multi.total_left_mul').css('margin-left','0px');
						$('.wheel-container-multi.total_right_mul').css('margin-left','0px');
						$(".left_mul.new").empty();
						$(".left_mul.new").html(spin_round_win_multipler_html_temp);
						$(".right_mul.new").empty();
						$(".right_mul.new").html(bonus_win_multipler_html_temp);
						show_freespin_multipler(data[0].chance,data[0].multipler,data[0].bonus_chance,data[0].bonus_multipler);
						
						$('#revolver').css('background-color','#f5bc71');
						$('.bodypart2-2').css('background-color','#f5bc71');
						$('.normal_round').css('display','none');
						$('.spin_round_trophy').css('display','block');
					}
					
					if(data[0].spins.length == 0){
						$('#bodypart3 .player_list').empty();
						$('#bodypart2 .player_count').text(0);
						$('#bodypart2 .player_wager').text(0);
						$('#bodypart2 .player_win').text('---');
					}
					
					
					is_fire_spin_play_list = 0;
					attend_amount = 0;
					is_cal_pre_round_result = 1;
					spin_available = true;
					is_bonus_multipler = 0;
					is_bonus_multipler_result = 0;
					win_countup_val = win_countup_val1 = 0;
									
					$('.roulette').css('display','none');
					$(".counter span").css("font-size","10px");
					$(".counter_title span").css("font-size","10px");
					$('.counter_title span').text("");
					$('.count_time').css('display','none');
					$('.bonus_spin_result').css('display','none');
					$('.counter').css('display','none');
					$('.counter_title').css('display','none');
					
					$('.black_fade').css('display','none');
					$('.bonus_roll').css('display','none');
					$('#revolver').css('background-image','none');
					
					is_attend_round = false;
					round_result_list(data[0].round_result_list);
				}
				
				if(data[0].is_freespin_round == 0){
					if(data[0].index >=180 ){
						spin_available = false;
						if(game_type == 1){
							if(sessionStorage.getItem('status') == null){
								change_orange('.play_btn');
								change_orange('.mobile_play_btn');	
							}else{
								change_grey('.play_btn');
								change_grey('.mobile_play_btn');
							}
						}else{
							change_orange('.play_btn');
							change_orange('.mobile_play_btn');
						}
						if(sessionStorage.getItem('status') != null){
							change_grey('.max_bet_btn');	
						}
					}
					var distance = (multipler_width - $('.case1.new').width()) / 150;
					if(data[0].index >= 30 && data[0].index < 180){
						$('.case1.new').css('left',parseFloat(parseFloat($('.case1.new').css('left')) - distance)+"px");
					}
					else if(data[0].index == 180){
						$('.case1.new').append(new_round_win_multipler_html);
						$('.wheel-container-multi.indivi_mul').css('margin-left',mul_margin_width+'px');
					}
					else if(data[0].index > 180 && data[0].index <= 239){
						distance1 = $('.case1.new').width() / 59;		
						$('.case1.new').css('left', parseFloat(parseFloat($('.case1.new').css('left')) - distance1)+"px");
					}
					else{
						$('.case1.new').html(new_round_win_multipler_html);
						$('.case1.new').css('left',0);
						$('.normal_round .win_multi_center').css('color','#F8BF60');
					}
				}else{
					var distance = (spin_multipler_width - $('.left_mul.new').width()) / 150;
					var distance_bonus = (spin_bonus_multipler_width - $('.right_mul.new').width()) / 150;
					
					if(data[0].index >= 30 && data[0].index < 180){
						$('.left_mul.new').css('left',-1 * distance *parseInt(parseInt(data[0].index)-30)+"px");
						$('.right_mul.new').css('left',-1 * distance_bonus *parseInt(parseInt(data[0].index)-30)+"px");
						var step_amount = parseFloat(Math.random()*1000).toFixed(2);
						if(total_mul_font == 0)
							$('.center_mul.new').html('<span>x'+step_amount+'</span>');
						else
							$('.center_mul.new').html('<span style="font-size:'+total_mul_font+';">x'+step_amount+'</span>');
					}
					else if(data[0].index == 180){
						$('.left_mul.new').css('left',-1 * (spin_multipler_width - $('.left_mul.new').width())+"px");
						$('.right_mul.new').css('left',-1 * (spin_bonus_multipler_width - $('.right_mul.new').width())+"px");
						$('.left_mul.new').append(spin_round_win_multipler_html);
						$('.right_mul.new').append(bonus_win_multipler_html);
						$('.spin_round .win_multi_center').css('color','white');
                        $('.spin_round .spin_center_mul .spin_multipler').css('color','white');
						$('.wheel-container-multi.total_left_mul').css('margin-left',spin_mul_margin_width+'px');
						$('.wheel-container-multi.total_right_mul').css('margin-left',spin_bonus_mul_margin_width+'px');
					}
					else if(data[0].index > 180 && data[0].index <= 239){
						distance1 = ((spin_multipler_result_width + $('.left_mul.new').width()) / 59).toFixed(2);
						distance2 = ((spin_bonus_multipler_result_width + $('.right_mul.new').width()) / 59).toFixed(2);
						if(data[0].index == 239){
							$('.left_mul.new').css('left', (-1 * distance1 * parseInt(data[0].index - 180)).toFixed(2) - (spin_multipler_width - $('.left_mul.new').width())+"px");
							$('.right_mul.new').css('left', (-1 * distance2 * parseInt(data[0].index - 180)).toFixed(2) - (spin_bonus_multipler_width - $('.right_mul.new').width())+"px");
						}else{
							$('.left_mul.new').css('left', (-1 * distance1 * parseInt(data[0].index - 180)).toFixed(2) - (spin_multipler_width - $('.left_mul.new').width())+"px");
							$('.right_mul.new').css('left', (-1 * distance2 * parseInt(data[0].index - 180)).toFixed(2) - (spin_bonus_multipler_width - $('.right_mul.new').width())+"px");
						}
						var step_amount = parseFloat(Math.random()*1000).toFixed(2);
						$('.center_mul.new').html('<span>x'+step_amount+'</span>');
					}
					else{
						$('.left_mul.new').html(spin_round_win_multipler_html);
						$('.right_mul.new').html(bonus_win_multipler_html);
						$('.left_mul.new').css('left',0);
						$('.right_mul.new').css('left',0);
						$('.center_mul.new').css('display','block');
						if(total_mul_font == 0)
							$('.center_mul.new').html('<span>x'+total_multipler+'</span>');
						else
							$('.center_mul.new').html('<span style="font-size:'+total_mul_font+';">x'+total_multipler+'</span>');
                        $('.spin_round .spin_left_mul .win_multi_center').css('color','black');
						$('.spin_round .spin_right_mul .win_multi_center').css('color','black');
                        $('.spin_round .spin_center_mul .spin_multipler').css('color','black');
					}
				}
				if(data[0].index == 33){
					if(data[0].is_freespin_round == 1 && sessionStorage.getItem('is_freespin_round') == 1){
						spin('');
					}else if(is_autoplay == 1){
						if(cur_autoplay_cnt > 0){
							cur_autoplay_cnt = cur_autoplay_cnt - 1;
							$('.remain_cnt').html(formatNumber(cur_autoplay_cnt));
							spin('auto');
						} else{
							stop_autoplay();
						}
					}
				}
				if(data[0].index == 0 && data[0].is_pre_shoot == 0){
					animItem_shoot.stop();
					animItem.goToAndPlay(0,true);
					$('#iframe_section').css('display','block');
					$('#iframe_section_shoot').css('display','none');
				}
			}
		});
		
		socket.on('update exp', function(data){
			change_exp(data.user_info);
		});
		
		socket.on('solo new animation',async function(data){
			if(game_type == 2){
				new_round_win_multipler_html = new_round_win_multipler_html_temp = "";
				$('.indivi_mul').css('display','inline-block');
				$('.play_btn span').html('PLAY');
				$('.mobile_play_btn').html('PLAY');
				if(is_solo_freespin_round == 0){
					sessionStorage.setItem('wager_index', -1);
					$('.play_bottom_part').css('display','block');
					if(is_autoplay == 0 && sessionStorage.getItem("status") != null){
						change_orange('.spin_btn');
						change_orange('.max_bet_btn');
						$('.spin_btn').css('display','block');
						$('.spin_btn .fa-sync-alt').css('color','black');
						$('.autoplay_stop_btn').css('display','none');
						change_orange('#wager_mobile_sel');
						change_orange('#spin2');
					}else if(is_autoplay != 0){
						change_orange('.spin_btn');
						$('.spin_btn').css('display','none');
						$('.autoplay_stop_btn').css('display','block');
						$('.spin_btn .fa-sync-alt').css('color','black');
						change_orange('#wager_mobile_sel');
						change_orange('#spin2');
						change_grey('.max_bet_btn');
					}
					$('.remain_cnt').css('display','block');
					$('.bonus_spin_cnt_section').css('display','none');
					$('.fa-stop').css('color','black');
					change_orange('.play_btn');
					change_orange('.mobile_play_btn');
					$('.bonus_wait_section').removeClass('bonus_wait_css');
					$('.bonus_wait').css('display','none');
					$('.bonus_wait1').css('display','none');
					$('.bonus_wait_section .title').css('display','block');
					$('.bonus_wait_section .cost').css('display','inline');
					$('.bonus_wait_section .wager_set').css('display','block');
					$('.bonus_wait_section .wager_amount').css('display','block');
					
					multipler_html(data[0].solo_round_result[2],data[0].solo_round_result[0]);
					$(".case1.new").empty();
					$(".case1.new").html(new_round_win_multipler_html_temp);
					
					$('.normal_round .win_multi_title').html("WIN MULTIPLER");
					$('.normal_round').css('display','block');
					$('.spin_round').css('display','none');
					$('.spin_round_trophy').css('display','none');
					$('#revolver').css('background-color','#282A30');
					$('.bodypart2-2').css('background-color','#282A30');
				}else{
					var width = window.innerWidth;
					if(width > 1023 && is_solo_freespin_round == 1){	
						$('.play_bottom_part').css('display','none');
						$('.spin_btn').css('display','none');
						$('.remain_cnt').css('display','none');
						$('.autoplay_stop_btn').css('display','none');
						$('.autoplay_spin').css("display",'none');
						if($('.bonus_spin_cnt_section').css('display') != 'block')
							$('.bonus_spin_cnt_section').css('display','block');
						$('.bonus_spin_cnt').html(solo_remain_freespin_cnt+" REMAINING");	
					}
					
					$('.bonus_wait_section').addClass('bonus_wait_css');
					$('.bonus_wait').css('display','inline-block');
					$('.bonus_wait1').css('display','inline-block');
					$('.bonus_wait_section .title').css('display','none');
					$('.bonus_wait_section .cost').css('display','none');
					$('.bonus_wait_section .wager_set').css('display','none');
					$('.bonus_wait_section .wager_amount').css('display','none');
					
					
					if(sessionStorage.getItem('wager_index') == null || sessionStorage.getItem('wager_index') == -1)
						$(".curwager").html(formatNumber(wager_list[wager_index]));
					else
						$(".curwager").html(formatNumber(wager_list[sessionStorage.getItem('wager_index')]));
					
					$('.total_spin').css('display','inline-block');
					$('.spin_round').css('display','block');
					
					spin_multipler_html(data[0].solo_round_result[2],data[0].solo_round_result[0]);
					bonus_multipler_html(data[0].solo_round_result[7],data[0].solo_round_result[6]);
					$(".left_mul.new").empty();
					$(".left_mul.new").html(spin_round_win_multipler_html_temp);
					$(".right_mul.new").empty();
					$(".right_mul.new").html(bonus_win_multipler_html_temp);
					show_freespin_multipler(data[0].solo_round_result);
					
					$('#revolver').css('background-color','#f5bc71');
					$('.bodypart2-2').css('background-color','#f5bc71');
					$('.normal_round').css('display','none');
					$('.spin_round_trophy').css('display','block');
				}
				
				is_fire_spin_play_list = 0;
				attend_amount = 0;
				is_cal_pre_round_result = 1;
				spin_available = true;
				is_solo_bonus_multipler = 0;
				is_bonus_multipler_result = 0;
				solo_win_countup_val = solo_win_countup_val1 = 0;
								
				$('.roulette').css('display','none');
				$(".counter span").css("font-size","10px");
				$(".counter_title span").css("font-size","10px");
				$('.counter_title span').text("");
				
				$('.count_time').css('display','none');
				$('.bonus_spin_result').css('display','none');
				$('.counter').css('display','none');
				$('.counter_title').css('display','none');

				
				$('.black_fade').css('display','none');
				$('.bonus_roll').css('display','none');
				$('#revolver').css('background-image','none');
				
				round_result_list(data[0].round_result_list);
				
				if(is_solo_freespin_round == 0){
					for(var solo_index = 30;solo_index <= 299;solo_index++){
						await new Promise(done => setTimeout(function(){
							if(solo_index == 30 && data[0].is_pre_shoot == 0){
								$('#iframe_section').css('display','block');
								$('#iframe_section_shoot').css('display','none');
								animItem.goToAndPlay(30, true);
								animItem_shoot.stop();	
							}else if(solo_index == 30 && data[0].is_pre_shoot == 1){
								$('#iframe_section').css('display','none');
								$('#iframe_section_shoot').css('display','block');
								animItem_shoot.goToAndPlay(30, true);
								animItem.stop();	
							}
							if(solo_index == 33 && is_autoplay == 1){
								if(cur_autoplay_cnt > 0){
									cur_autoplay_cnt = cur_autoplay_cnt - 1;
									$('.remain_cnt').html(formatNumber(cur_autoplay_cnt));
									spin('auto');
								} else{
									stop_autoplay();
								}
							}
							if(solo_index ==180 ){
								if(my_exp_val != 0){
									socket.emit('change exp',{cash: my_cash, user_id: sessionStorage.getItem("status"), experience: my_exp_val});
								}
								$('.play_btn span').html('SKIP');
								$('.mobile_play_btn').html('SKIP');
								spin_available = false;
								change_grey('.max_bet_btn');
								
							}
							var distance = (multipler_width - $('.case1.new').width()) / 150;
							if(solo_index >= 30 && solo_index < 180){
								$('.case1.new').css('left',-1 * distance *parseInt(parseInt(solo_index)-30)+"px");
							}
							else if(solo_index == 180){
								$('.case1.new').css('left',-1 * (multipler_width - $('.case1.new').width())+"px");
								$('.case1.new').append(new_round_win_multipler_html);
							}
							else if(solo_index > 180 && solo_index <= 239){
								distance1 = (multipler_result_width+$('.case1.new').width()) / 58;
								if(solo_index == 239)
									$('.case1.new').css('left', (-1 * distance1 * parseInt(solo_index - 181)).toFixed(2) -(multipler_width - $('.case1.new').width())+2+"px");
								else
									$('.case1.new').css('left', (-1 * distance1 * parseInt(solo_index - 181)).toFixed(2) -(multipler_width - $('.case1.new').width())+"px");
							}
							else{
								$('.normal_round .win_multi_center').css('color','#F8BF60');
							}
							done();
						},16));
					}
					
				}else{
					for(var solo_index = 30;solo_index <= 299;solo_index++){
						await new Promise(done => setTimeout(function(){
							if(solo_index == 30 && data[0].is_pre_shoot == 0){
								$('#iframe_section').css('display','block');
								$('#iframe_section_shoot').css('display','none');
								animItem.goToAndPlay(30, true);
								animItem_shoot.stop();	
							}else if(solo_index == 30 && data[0].is_pre_shoot == 1){
								$('#iframe_section').css('display','none');
								$('#iframe_section_shoot').css('display','block');
								animItem_shoot.goToAndPlay(30, true);
								animItem.stop();	
							}
							var distance = spin_multipler_width / 150;
							var distance_bonus = spin_bonus_multipler_width / 150;
							$('.center_mul.new').css('display','none');
							if(solo_index >= 30 && solo_index < 180){
								$('.left_mul.new').css('left',-1 * distance *parseInt(parseInt(solo_index)-30)+"px");
								$('.right_mul.new').css('left',-1 * distance_bonus *parseInt(parseInt(solo_index)-30)+"px");
							}
							else if(solo_index == 180){
								$('.left_mul.new').css('left',-1 * spin_multipler_width+"px");
								$('.right_mul.new').css('left',-1 * spin_bonus_multipler_width+"px");
								$('.left_mul.new').append(spin_round_win_multipler_html);
								$('.right_mul.new').append(bonus_win_multipler_html);
							}
							else if(solo_index > 180 && solo_index <= 239){
								distance1 = (spin_multipler_result_width / 58).toFixed(2);
								distance2 = (spin_bonus_multipler_result_width / 58).toFixed(2);
								if(solo_index == 239){
									$('.left_mul.new').css('left', (-1 * distance1 * parseInt(solo_index - 181)).toFixed(2) - spin_multipler_width+2+"px");
									$('.right_mul.new').css('left', (-1 * distance2 * parseInt(solo_index - 181)).toFixed(2) - spin_bonus_multipler_width+2+"px");
								}else{
									$('.left_mul.new').css('left', (-1 * distance1 * parseInt(solo_index - 181)).toFixed(2) - spin_multipler_width+"px");
									$('.right_mul.new').css('left', (-1 * distance2 * parseInt(solo_index - 181)).toFixed(2) - spin_bonus_multipler_width+"px");
								}
							}
							else{
								$('.center_mul.new').css('display','block');
								$('.center_mul.new').html('<span>x'+total_multipler+'</span>');
								$('.spin_round .win_multi_center').css('color','black');
                                $('.spin_round .spin_center_mul .spin_multipler').css('color','black');
							}
							done();
						},16));
					}
					
				}
				
				if(data[0].is_shoot == 0){
					for(var solo_index = 0;solo_index <= 29;solo_index++){
						await new Promise(done => setTimeout(function(){
							if(solo_index == 0){	
								$('#iframe_section').css('display','block');
								$('#iframe_section_shoot').css('display','none');
								animItem.goToAndPlay(0, true);
								animItem_shoot.stop();	
							}
							if(solo_index == 26){
								animItem.goToAndStop(26,true);
							}
							done();
						},16));
					}	
				}else{
					for(var solo_index = 0;solo_index <= 12;solo_index++){
						await new Promise(done => setTimeout(function(){
							if(solo_index == 0){
								$('#iframe_section').css('display','none');
								$('#iframe_section_shoot').css('display','block');
								animItem_shoot.goToAndPlay(0, true);
								animItem.stop();
							}
							if(solo_index == 12){
								animItem_shoot.pause();
								animItem_shoot.goToAndStop(12,true);
								var total_win_amount = 0;
								
								$('.black_fade').css('display','block');
								$('.russian_roulette_players').each(function(i,obj){
									var spin_amount = parseInt(remove_character($(this).find('.player_list_center span').text()));
									var win_amount_temp = 0;
									if(is_solo_freespin_round == 0){
										win_amount_temp = data[0].solo_round_result[0];
										if(data[0].solo_round_result[2] == 1){
											win_amount_temp = parseFloat((data[0].solo_round_result[0] * 0.815).toFixed(2));
											win_amount_temp = parseFloat((spin_amount*win_amount_temp).toFixed(2));
										}
										else{
											win_amount_temp = parseFloat((data[0].solo_round_result[0] * data[0].solo_round_result[2]).toFixed(2));
											win_amount_temp = parseFloat((spin_amount*win_amount_temp).toFixed(2));
										}
										if(data[0].solo_round_result[1] == 20)
											win_amount_temp = win_amount_temp * 2;
									}else{
										win_amount_temp = parseFloat((spin_amount*total_multipler).toFixed(2));
									}
									win_amount_temp = parseInt(win_amount_temp);
									total_win_amount += win_amount_temp;
									$(this).find('.player_list_right span').html(formatNumber(win_amount_temp));
									$(this).find('.player_list_right').append('&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>');
								});
								if(total_win_amount >= 1000000000000)
									$('.player_win').html(parseFloat(parseFloat(total_win_amount/1000000000000).toFixed(2))+"T");
								else if(total_win_amount >= 1000000000)
									$('.player_win').html(parseFloat(parseFloat(total_win_amount/1000000000).toFixed(2))+"B");
								else if(total_win_amount >= 1000000)
									$('.player_win').html(parseFloat(parseFloat(total_win_amount/1000000).toFixed(2))+"M");
								else
									$('.player_win').html(formatNumber(total_win_amount.toFixed(2)));
								
								if(is_solo_attend == true && sessionStorage.getItem('status') != null && is_solo_freespin_round == 0){
									var wager = solo_wager;
									var win_amount = 0;
									if(data[0].solo_round_result[2] == 1){
										win_amount = parseInt(data[0].solo_round_result[0] * 0.815);
										win_amount = parseInt(wager*win_amount);
									}
									else{
										win_amount = parseInt(data[0].solo_round_result[0] * data[0].solo_round_result[2]);
										win_amount = parseInt(wager*win_amount);
									}
									if(data[0].solo_round_result[1] == 20)
										win_amount = win_amount * 2;
									sessionStorage.setItem('cash',win_amount+parseInt(sessionStorage.getItem('cash')));	
									socket.emit('update cash', {cash: win_amount, user_id:sessionStorage.getItem('status')});
								}else if(is_solo_freespin_round == 1){
									var wager = parseInt(solo_wager);
									var win_bonus = parseInt(wager * total_multipler);
									sessionStorage.setItem('cash',win_bonus+parseInt(sessionStorage.getItem('cash')));	
									socket.emit('update cash', {cash: win_bonus, user_id:sessionStorage.getItem('status')});
								}
							}				
							done();
						},16));
					}
					var multipler_val = 0;
					if(is_solo_freespin_round == 0){
						if(data[0].solo_round_result[2] == 1)
							multipler_val = parseFloat((data[0].solo_round_result[0] * 0.815).toFixed(2));
						else
							multipler_val = parseFloat((data[0].solo_round_result[0] * data[0].solo_round_result[2]).toFixed(2));
					}
					var total_spin_amount = multipler_index = duration_time = duration_time1 = 0;
					
					if(multipler_val >= 100){
						total_spin_amount = solo_wager * multipler_val;
						multipler_index = multipler_val;
						if(data[0].solo_round_result[1] == 20)
							total_spin_amount = total_spin_amount * 2;
						total_spin_amount = parseInt(total_spin_amount);
						duration_time = 20000;
						duration_time1 = 4000;
						await new Promise(done=>setTimeout(function(){
							win_countup(total_spin_amount,20000,multipler_index,data[0].solo_round_result[1]);
							done();
						},10));
					}else if(multipler_val >= 75){
						total_spin_amount = solo_wager * multipler_val;
						multipler_index = multipler_val;
						if(data[0].solo_round_result[1] == 20)
							total_spin_amount = total_spin_amount * 2;
						total_spin_amount = parseInt(total_spin_amount);
						duration_time = 15000;
						duration_time1 = 3500;
						await new Promise(done=>setTimeout(function(){
							win_countup(total_spin_amount,15000,multipler_index,data[0].solo_round_result[1]);
							done();
						},10));
					}else if(multipler_val >= 25){
						total_spin_amount = solo_wager * multipler_val;
						multipler_index = multipler_val;
						if(data[0].solo_round_result[1] == 20)
							total_spin_amount = total_spin_amount * 2;
						total_spin_amount = parseInt(total_spin_amount);
						duration_time = 10000;
						duration_time1 = 3000;
						await new Promise(done=>setTimeout(function(){
							win_countup(total_spin_amount,10000,multipler_index,data[0].solo_round_result[1]);
							done();
						},10));
					}else if(multipler_val >= 15){
						total_spin_amount = solo_wager * multipler_val;
						multipler_index = multipler_val;
						if(data[0].solo_round_result[1] == 20)
							total_spin_amount = total_spin_amount * 2;
						total_spin_amount = parseInt(total_spin_amount);
						duration_time = 7000;
						duration_time1 = 2500;
						await new Promise(done=>setTimeout(function(){
							win_countup(total_spin_amount,7000,multipler_index,data[0].solo_round_result[1]);
							done();
						},10));
					}else if(multipler_val >= 7.5){
						total_spin_amount = solo_wager * multipler_val;
						multipler_index = multipler_val;
						if(data[0].solo_round_result[1] == 20)
							total_spin_amount = total_spin_amount * 2;
						total_spin_amount = parseInt(total_spin_amount);
						duration_time = 5000;
						duration_time1 = 1500;
						await new Promise(done=>setTimeout(function(){
							win_countup(total_spin_amount,5000,multipler_index,data[0].solo_round_result[1]);
							done();
						},10));
					}else if(multipler_val >= 2){
						total_spin_amount = solo_wager * multipler_val;
						multipler_index = multipler_val;
						if(data[0].solo_round_result[1] == 20)
							total_spin_amount = total_spin_amount * 2;
						total_spin_amount = parseInt(total_spin_amount);
						duration_time = 2000;
						duration_time1 = 1000;
						await new Promise(done=>setTimeout(function(){
							win_countup(total_spin_amount,2000,multipler_index,data[0].solo_round_result[1]);
							done();
						},10));
					}else if(multipler_val > 0){
						total_spin_amount = solo_wager * multipler_val;
						multipler_index = multipler_val;
						if(data[0].solo_round_result[1] == 20)
							total_spin_amount = total_spin_amount * 2;
						total_spin_amount = parseInt(total_spin_amount);
						duration_time = 1000;
						duration_time1 = 500;
						win_countup(total_spin_amount,1000,multipler_index,data[0].solo_round_result[1]);
					}
					
					if(multipler_val > 0){
						await new Promise(done=>setTimeout(function(){
							end_solo_win_countup();
							done();
						},duration_time+2000));
					}
					for(var solo_index = 13;solo_index <= 26;solo_index++){
						await new Promise(done => setTimeout(function(){
							if(solo_index == 13){
								$('.counter').css('display','none');
								$('.counter_title').css('display','none');
								$('.black_fade').css('display','none');
								animItem.stop();
								animItem_shoot.goToAndPlay(13,true);
								$('#iframe_section').css('display','none');
								$('#iframe_section_shoot').css('display','block');
							}
							if(solo_index == 26){
								animItem_shoot.goToAndStop(29,true);
								//socket.emit('end solo new gun animation', {solo_round_result:data[0].solo_round_result, pre_round_result:pre_round_result, wager: solo_wager, user_id : sessionStorage.getItem('status'), is_shoot : 1});
							}
							done();
						},16));
					}	
				}
				if(data[0].solo_round_result[3] >= 17 && data[0].solo_round_result[3] <= 19){
					$('#iframe_section').css('display','none');
					$('#iframe_section_shoot').css('display','block');
					animItem_shoot.goToAndStop(12,true);
					change_orange('.play_btn');
					change_orange('.mobile_play_btn');
					change_grey('.max_bet_btn');
					
					$('#bodypart3 .count_time').css('display','none');
					
					$('.roulette').css('display','table');
					$('.counter').css('display','none');
					$('.counter_title').css('display','none');
					
					$('.bonus_roll').css('display','block');
					$('.black_fade').css('display','block');
					solo_randomize_value = Math.floor(Math.random() * defualt_roll_width);
					SoloRoulette.init(data[0].solo_round_result);
					SoloRoulette.start();
				} else{
					socket.emit('end solo new gun animation', {solo_round_result:data[0].solo_round_result, pre_round_result:pre_round_result, wager: solo_wager, user_id : sessionStorage.getItem('status'), is_shoot : data[0].is_shoot});	
				}
			}
		});
		
		socket.on('solo round result list', function(data){
			round_result_list(data[0].round_result_list);
		});
		
		socket.on('new solo round start', function(data){
			spin_available = true;
			pre_round_result = data.pre_round_result;
			my_cash = my_exp_val = 0;
			is_solo_attend = false;
			if(data.type == 0){
				$('.counter').css('display','table');
				$('.counter_title').css('display','table');
				$('.black_fade').css('display','block');
				
                $('.counter_title').html("<span style='font-size:10px;'>NO WIN</span>");
				$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
				$('.counter span').html("");
				$('.roulette').css('display','none');
			}
			if(is_solo_freespin_round == 1){
				setTimeout(function(){
					solo_remain_freespin_cnt = solo_remain_freespin_cnt - 1;
					$('.play_btn span').html('SKIP');
					$('.mobile_play_btn').html('SKIP');
					change_grey('.max_bet_btn');
					if(solo_remain_freespin_cnt > 0)
						spin('');
					else
						is_solo_freespin_round = 0;
					solo_wager = 0;
				},1500);
				
			}else{
				$('.play_btn span').html('PLAY');
				$('.mobile_play_btn').html('PLAY');
				change_orange('.max_bet_btn');
				solo_wager = 0;
				total_solo_freespin_wager = 0;
				$('.win_multi_title').removeClass('color-black');
				$('.win_multi_title').addClass('color-orange');
			}
			/*solo_seed = curDateTime() + " " + sessionStorage.getItem('status');
			socket.emit('play solo start',{client_seed : solo_seed});*/
		});
		
		socket.on('animation',function(data){
			if(game_type == 1){
				
				spin_available = false;
				if(is_freespin_round == 0 && is_shot_animation == 0){
					multipler_html(data[0].new_round_result[2],data[0].new_round_result[0]);
					$(".case1.new").css('left',"0px");
					$(".case1.new").html(new_round_win_multipler_html);
				} else if(is_freespin_round == 1 && is_shot_animation == 0){
					$('.total_spin').css('display','inline-block');
					spin_multipler_html(data[0].new_round_result[2],data[0].new_round_result[0]);
					bonus_multipler_html(data[0].new_round_result[7],data[0].new_round_result[6]);
					$(".left_mul.new").css('left',"0px");
					$(".left_mul.new").html(spin_round_win_multipler_html);
					$(".right_mul.new").css('left',"0px");
					$(".right_mul.new").html(bonus_win_multipler_html);
					show_freespin_multipler(data[0].new_round_result);
					$('.center_mul.new').css('display','block');
					$('.center_mul.new').html('<span>x'+total_multipler+'</span>');
				}
				if(data[0].index == 0 || is_shot_animation == 0){
					is_shot_animation = 1;
					$('.counter').css('display','none');
					$('.counter_title').css('display','none');
					
					$('.black_fade').css('display','none');
					$('.bonus_roll').css('display','none');
					$(".counter_title span").css("font-size","10px");
					if(sessionStorage.getItem("status") != null){
						change_grey('.play_btn');
						change_grey('.mobile_play_btn');
						change_grey('.max_bet_btn');	
					}
				}
				if($('.round_result').html() != ''){
					round_result_list(data[0].round_result_list);
				}
				
				if(data[0].index == 0){
					animItem.stop();
					animItem_shoot.goToAndPlay(data[0].index,true);
					$('#iframe_section').css('display','none');
					$('#iframe_section_shoot').css('display','block');
				}
				
				if(data[0].index == 12){
					var total_win_amount = 0;
					$('#iframe_section').css('display','none');
					$('#iframe_section_shoot').css('display','block');
					animItem_shoot.pause();
					$('.black_fade').css('display','block');
					var total_win_countup_val = duration = multipler_val = 0;
					if(data[0].new_round_result[2] == 1){
						multipler_val = parseFloat((data[0].new_round_result[0] * 0.815).toFixed(2));
					} else{
						multipler_val = parseFloat((data[0].new_round_result[0] * data[0].new_round_result[2]).toFixed(2));
					}
					if(multipler_val >= 100)
						duration = 20000;
					else if(multipler_val >= 75)
						duration = 15000;
					else if(multipler_val >= 25)
						duration = 10000;
					else if(multipler_val >= 15)
						duration = 7000;
					else if(multipler_val >= 7.5)
						duration = 5000;
					else if(multipler_val >= 2)
						duration = 2000;
					else
						duration = 1000;
						
					$('.russian_roulette_players').each(function(i,obj){
						var spin_amount = parseInt(remove_character($(this).find('.player_list_center span').text()));
						var win_amount_temp = 0;
						if(is_freespin_round == 0){
							if(data[0].new_round_result[2] == 1){
								win_amount_temp = parseFloat((data[0].new_round_result[0] * 0.815).toFixed(2));
								win_amount_temp = parseFloat((spin_amount*win_amount_temp).toFixed(2));
							}
							else{
								win_amount_temp = parseFloat((data[0].new_round_result[0] * data[0].new_round_result[2]).toFixed(2));
								win_amount_temp = parseFloat((spin_amount*win_amount_temp).toFixed(2));
							}
							if(data[0].new_round_result[1] == 20)
								win_amount_temp = win_amount_temp * 2;
						}else{
							win_amount_temp = parseFloat(spin_amount*total_multipler);
						}
						
						total_win_amount += win_amount_temp;
						var each_total_win_countup_val = 0;
						$({ countNum3: each_total_win_countup_val}).animate({countNum3: win_amount_temp},
						{
						    duration: duration,
						    easing:'linear',
						    step: function() {
						    	each_total_win_countup_val = win_amount_temp;
						    	if(this.countNum3 >= 1000000000000){
						      		if(parseFloat(this.countNum3/1000000000000) % 1 == 0)
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000000).toFixed(0)+"T");
									else
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000000).toFixed(2)+"T");
								}
						      	else if(this.countNum3 >= 1000000000){
						      		if(parseFloat(this.countNum3/1000000000) % 1 == 0)
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000).toFixed(0)+"B");
									else
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000).toFixed(2)+"B");
								}
						      	else if(this.countNum3 >= 1000000){
						      		if(parseFloat(this.countNum3/1000000) % 1 == 0)
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000).toFixed(0)+"M");
									else
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000).toFixed(2)+"M");
								}
						      	else
						      		$('.player_list_right span').html(formatNumber(this.countNum3.toFixed(2)));
						    },
						    complete: function() {
						    	if(this.countNum3 >= 1000000000000){
						    		if(parseFloat(this.countNum3/1000000000000) % 1 == 0)
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000000).toFixed(0)+"T");
									else
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000000).toFixed(2)+"T");
								}
						    	else if(this.countNum3 >= 1000000000){
						    		if(parseFloat(this.countNum3/1000000000) % 1 == 0)
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000).toFixed(0)+"B");
									else
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000000).toFixed(2)+"B");
								}
						      	else if(this.countNum3 >= 1000000){
						      		if(parseFloat(this.countNum3/1000000) % 1 == 0)
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000).toFixed(0)+"M");
									else
										$('.player_list_right span').html(parseFloat(this.countNum3/1000000).toFixed(2)+"M");
								}
						      	else
						      		$('.player_list_right span').html(formatNumber(this.countNum3.toFixed(2)));
						    }
						});
					
					});
					
					$({ countNum2: total_win_countup_val}).animate({countNum2: total_win_amount},
					{
					    duration: duration,
					    easing:'linear',
					    step: function() {
					    	total_win_countup_val = total_win_amount;
					    	if(this.countNum2 >= 1000000000000){
					      		if(parseFloat(this.countNum2/1000000000000) % 1 == 0)
									$('.player_win').html(parseFloat(this.countNum2/1000000000000).toFixed(0)+"T");
								else
									$('.player_win').html(parseFloat(this.countNum2/1000000000000).toFixed(2)+"T");
							}
					      	else if(this.countNum2 >= 1000000000){
					      		if(parseFloat(this.countNum2/1000000000) % 1 == 0)
									$('.player_win').html(parseFloat(this.countNum2/1000000000).toFixed(0)+"B");
								else
									$('.player_win').html(parseFloat(this.countNum2/1000000000).toFixed(2)+"B");
							}
					      	else if(this.countNum2 >= 1000000){
					      		if(parseFloat(this.countNum2/1000000) % 1 == 0)
									$('.player_win').html(parseFloat(this.countNum2/1000000)+"M");
								else
									$('.player_win').html(parseFloat(this.countNum2/1000000).toFixed(2)+"M");
							}
					      	else
					      		$('.player_win').html(formatNumber(this.countNum2.toFixed(2)));
					    },
					    complete: function() {
					    	if(this.countNum2 >= 1000000000000){
					    		if(parseFloat(this.countNum2/1000000000000) % 1 == 0)
									$('.player_win').html(parseFloat(this.countNum2/1000000000000).toFixed(0)+"T");
								else
									$('.player_win').html(parseFloat(this.countNum2/1000000000000).toFixed(2)+"T");
							}
					    	else if(this.countNum2 >= 1000000000){
					    		if(parseFloat(this.countNum2/1000000000) % 1 == 0)
									$('.player_win').html(parseFloat(this.countNum2/1000000000).toFixed(0)+"B");
								else
									$('.player_win').html(parseFloat(this.countNum2/1000000000).toFixed(2)+"B");
							}	
					      	else if(this.countNum2 >= 1000000){
					      		if(parseFloat(this.countNum2/1000000) % 1 == 0)
									$('.player_win').html(parseFloat(this.countNum2/1000000).toFixed(0)+"M");
								else
									$('.player_win').html(parseFloat(this.countNum2/1000000).toFixed(2)+"M");
							}
					      	else
					      		$('.player_win').html(formatNumber(this.countNum2.toFixed(2)));
					    }
					});
					
					if(is_attend_round == true && sessionStorage.getItem('status') != null && is_freespin_round == 0){
						var wager = attend_amount;
						var win_amount = 0;
						if(data[0].new_round_result[2] == 1){
							win_amount = parseFloat((data[0].new_round_result[0] * 0.815).toFixed(2));
							win_amount = parseFloat((wager*win_amount).toFixed(2));
						}
						else{
							win_amount = parseFloat((data[0].new_round_result[0] * data[0].new_round_result[2]).toFixed(2));
							win_amount = parseFloat((wager*win_amount).toFixed(2));
						}
						if(data[0].new_round_result[1] == 20)
							win_amount = win_amount * 2;
						sessionStorage.setItem('cash',win_amount+parseInt(sessionStorage.getItem('cash')));	
						socket.emit('update cash', {cash: win_amount, user_id:sessionStorage.getItem('status')});
					}else if(is_freespin_round == 1 && sessionStorage.getItem('is_freespin_round') == 1){
						var wager = parseInt(attend_amount);
						var win_bonus = parseFloat((wager * total_multipler).toFixed(2));
						sessionStorage.setItem('cash',win_bonus+parseInt(sessionStorage.getItem('cash')));	
						socket.emit('update cash', {cash: win_bonus, user_id:sessionStorage.getItem('status')});
					}
				}
				if(data[0].index == 13){
					$('.counter').css('display','none');
					$('.counter_title').css('display','none');
					$('.black_fade').css('display','none');
					animItem.stop();
					animItem_shoot.goToAndPlay(data[0].index,true);
					$('#iframe_section').css('display','none');
					$('#iframe_section_shoot').css('display','block');
				}
			}
		});
		
		socket.on('win countup',function(data){

			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1){
				$('.counter').css('display','table');
				$('.counter_title').css('display','table');
				$('.black_fade').css('display','block');

				if(sessionStorage.getItem("status") != null){
					change_grey('.play_btn');
					change_grey('.mobile_play_btn');
					change_grey('.max_bet_btn');
				}

				animItem_shoot.goToAndStop(12, true);

				var counter_width = $('.counter_section span').width();
				var counter_height = $('.counter_section span').height();
				var pos_left = ($('#revolver').width() - counter_width)/2;
				var pos_top = ($('#revolver').height() - counter_height)/2;
				var show_anim = 0;
				
				$({ countNum: win_countup_val}).animate({countNum: data[0].total_spin_amount,fontSize:win_countup_font_size+"px"},
				{
				    duration: data[0].duration,
				    easing:'linear',
				    step: function(no, tw) {
				    	$('.counter_section').css('position','relative');
						$('.counter_section').css('top',pos_top+'px');
						$('.counter_section').css('left',pos_left+'px');
				    	win_countup_val = data[0].total_spin_amount;
				      	$('.counter span.counter_section').html(formatNumber(this.countNum.toFixed(2)) + " <i class='fas fa-gem'></i>");
				    },
				    progress: function(an, pr, re){
				    	if(pr >= 1/data[0].multipler_index*100 && show_anim == 3){
				    		show_anim = 4;
							$('.counter_title').html("<span style='font-size:10px;'>LEGENDARY WIN</span>");
							$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
						} else if(pr >= 1/data[0].multipler_index*75 && show_anim == 2){
							show_anim = 3;
							$('.counter_title').html("<span style='font-size:10px;'>EPIC WIN</span>");
							$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
						} else if(pr >= 1/data[0].multipler_index*25 && show_anim == 1){
							show_anim = 2;
							$('.counter_title').html("<span style='font-size:10px;'>MEGA WIN</span>");
							$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
						} else if(pr >= 1/data[0].multipler_index*15 && show_anim == 0){
							show_anim = 1;
							$('.counter_title').html("<span style='font-size:10px;'>BIG WIN</span>");
							$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
						}
					},
				    complete: function() {
				        var i = 0;
				        var interval = setInterval(wait,1000);
				        function wait(i) {
				            if (i < 6) {
				                $("#counter_section").html(i);
                                i+=1;
                            }else{
				                clearInterval(interval);
                            }
                        }
                    }
				});
				var step_amount = parseFloat((attend_amount *data[0].multipler_index).toFixed(2));
				if(data[0].double == 20)
					step_amount = step_amount * 2;
				if(step_amount != 0){
					$({ countNum1: win_countup_val1}).animate({countNum1: step_amount},
					{
					    duration: data[0].duration,
					    easing:'linear',
					    step: function() {
					    	win_countup_val1 = step_amount;
					      	if(sessionStorage.getItem('status') != null){
					      		if(parseFloat(this.countNum1) >= 1000000000000)
									$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000000000).toFixed(2)+"T");
								else if(parseFloat(this.countNum1) >= 1000000000)
									$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000000).toFixed(2)+"B");
								else if(parseFloat(this.countNum1) >= 10000000)
									$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000).toFixed(2)+"M");
								else
									$('.bestwager').text(formatNumber(this.countNum1.toFixed(2)));
							}
					    },
					    complete: function() {
					      	if(sessionStorage.getItem('status') != null){
					      		if(parseFloat(this.countNum1) >= 1000000000000)
									$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000000000).toFixed(2)+"T");
					      		else if(parseFloat(this.countNum1) >= 1000000000)
									$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000000).toFixed(2)+"B");
								else if(parseFloat(this.countNum1) >= 10000000)
									$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000).toFixed(2)+"M");
								else
									$('.bestwager').text(formatNumber(this.countNum1.toFixed(2)));
							}
					    }
					});
				}
				
				$('.counter span').animate({fontSize:win_countup_font_size+"px"},500);

				
			}
		});
		
		socket.on('end win countup',function(data){
			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1){
				change_grey('.play_btn');
				change_grey('.mobile_play_btn');
				change_grey('.max_bet_btn');
				if(sessionStorage.getItem("status") != null){
					if(parseFloat(sessionStorage.getItem("cash")) >= 1000000000000)
						$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash"))/1000000000000).toFixed(2)+"T");
					else if(parseFloat(sessionStorage.getItem("cash")) >= 1000000000)
						$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash"))/1000000000).toFixed(2)+"B");
					else if(parseFloat(sessionStorage.getItem("cash")) >= 100000000)
						$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash"))/1000000).toFixed(2)+"M");
					else{
						//var width = window.innerWidth;
						//if(width > 1400)
							$(".totalgems").html(formatNumber(parseFloat(sessionStorage.getItem("cash")).toFixed(2)));
						//else
						//	$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash"))/1000).toFixed(2)+"K");
					}
						
				}
			}
		});
		
		socket.on('roulette roll',function(data){
			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1){
				$('#iframe_section').css('display','none');
				$('#iframe_section_shoot').css('display','none');
				animItem_shoot.goToAndStop(16,true);
				if(game_type == 1){
					change_grey('.play_btn');
					change_grey('.mobile_play_btn');
				}
				change_grey('.max_bet_btn');
				
				$('#bodypart3 .count_time').css('display','none');
				
				$('.roulette').css('display','table');
				$('.counter').css('display','none');
				$('.counter_title').css('display','none');
				
				$('.bonus_roll').css('display','block');
				$('.black_fade').css('display','block');
				randomize_value = Math.floor(data[0].random_value * defualt_roll_width);
				current_pos = data[0].current_pos;
				Roulette.init(data[0].new_round_result);
				Roulette.start();
			}
		});
		
		socket.on('move roll',function(data){
			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1)
				Roulette.roll();
		});
		
		socket.on('reset roll', function(data){
			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1)
				Roulette.reset();
		});
		
		socket.on('stop roll', function(data){
			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1)
				 Roulette.checkagain();
		});
		
		socket.on('show spin result', function(data){
			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1){
				var width = window.innerWidth;
				$('.total_spin').css('display','inline-block');
				$('.spin_round').css('display','block');
				
				$('#revolver').css('background-color','#f5bc71');
				$('.bodypart2-2').css('background-color','#f5bc71');
				$('.normal_round').css('display','none');
						
						
				$('#iframe_section').css('display','none');
				$('#iframe_section_shoot').css('display','none');
						
				$('.spin_round_trophy').css('display','none');
				if($('.roulette').css('display') != 'none')
					$('.roulette').css('display','none');
				if($('.black_fade').css('display') != 'block')
					$('.black_fade').css('display','block');
				if($('.count_time').css('display') != 'none')
					$('.count_time').css('display','none');
				if(game_type == 1){
					change_grey('.play_btn');
					change_grey('.mobile_play_btn');
				}else{
					change_orange('.play_btn');
					change_orange('.mobile_play_btn');
				}
				change_grey('.max_bet_btn');
				
				
				if(is_bonus_multipler == 0){
					is_bonus_multipler = 1;
					$('#revolver .bonus_spin_result').css('display','table');
					var spin_html = spin_cnt_html(data[0].new_round_result);
					$(".bonus_win .case3.new").empty();
					$(".bonus_win .case3.new").html(win_bonus_html_temp);
					if(is_freespin_round == 0){
						$('.bonus_spin_result .fa-trophy').addClass('color-orange');
					}
					if(sessionStorage.getItem('wager_index') == -1)
						sessionStorage.setItem('wager_index',wager_index);
				}
				var distance = bonus_multipler_width / 171;
				if(data[0].index < 170){
					$('.bonus_win .case3.new').css('left',-1 * distance *parseInt(data[0].index)+"px");
				}
				else if(data[0].index == 170){
					$('.bonus_win .case3.new').css('left',-1 * bonus_multipler_width+"px");
				}
				else if(data[0].index <= 250){
					distance1 = bonus__result_width / 81;
					if(data[0].index == 250)
						$('.case3.new').css('left', (-1 * distance1 * parseInt(data[0].index - 170)).toFixed(2) -bonus_multipler_width+2+"px");
					else
						$('.case3.new').css('left', (-1 * distance1 * parseInt(data[0].index - 170)).toFixed(2) -bonus_multipler_width+"px");
				}
				else{
					if(is_attend_round == true && sessionStorage.getItem('status') != null){
						sessionStorage.setItem('is_freespin_round',1);
					}
					$('.win_bonus_center').css('color','#F8BF60');
				}
				if(data[0].index == 300){
					var total_spins = data[0].remain_freespin_cnt;
					sessionStorage.setItem('free_spins',total_spins);
					
					$('#reward_spin_cnt').text(total_spins);
					$('#account_spins_cnt').text(total_spins);
					
					var width = window.innerWidth;
					if(width > 1023 && is_freespin_round == 1 && sessionStorage.getItem('is_freespin_round') == 1){	
						$('.play_bottom_part').css('display','none');
						$('.spin_btn').css('display','none');
						$('.remain_cnt').css('display','none');
						$('.autoplay_stop_btn').css('display','none');
						$('.bonus_spin_cnt_section').css('display','block');
						$('.bonus_spin_cnt').html(total_spins+" REMAINING");	
					}
				}
			}
		});
		
		socket.on('show bonus result', function(data){
			if(is_shot_animation == 0)
				is_shot_animation = 1;
			if(game_type == 1){
				$('#iframe_section').css('display','none');
				$('#iframe_section_shoot').css('display','none');
				is_show_spin_remain = 1;
				$('.black_fade').css('display','block');
				if($('.roulette').css('display') != 'none')
					$('.roulette').css('display','none');
				change_grey('.play_btn');
				change_grey('.mobile_play_btn');
				change_grey('.max_bet_btn');
				if(is_bonus_multipler_result == 0){
					is_bonus_multipler_result = 1;
					$('.counter').css('display','none');
					$('.counter_title').css('display','none');
					var html = "<span class='color-orange each_result'>TOTAL WIN<br><br><span class='total_win_gem'>"+formatNumber(data[0].total_wager_free_round)+"</span></span>";
					$('#revolver .count_time').css('display','table');
					$('#revolver .count_time').html(html);
					
					$({ countNum: 0}).animate({countNum: data[0].total_wager_free_round},
					{
					    duration: 2000,
					    easing:'linear',
					    step: function() {
					    	win_countup_val = data[0].total_wager_free_round;
					      	$('.total_win_gem').html(formatNumber(Math.floor(this.countNum)) + ' <i class="fas fa-gem"></i>');
					    },
					    complete: function() {
					      	$('.total_win_gem').html(formatNumber(this.countNum) + ' <i class="fas fa-gem"></i>');
					    }
					});
				}
			}
			else{
				change_orange('.play_btn');
				change_orange('.mobile_play_btn');
			}
			
		});
		
		socket.on('initial spin player list',function(data){
			var buffer = '';
			var color = ["inactive", "red-orange", "dark-blue", "dark-pink", "red", "orange"];
			var cur_step = data[0].cur_step;
			var max_win_amount = 0;
			var my_bet_html = "";
			if(data[0].spins.length >= 1){
				for(var i =data[0].spins.length-1;i>=0;i--){
					var level = parseInt(data[0].spins[i].level);
					
					var color_name = '';
					if(level < 50)
						color_name = color[0];
					else if(level < 60)
						color_name = color[1];
					else if(level < 85)
						color_name = color[2];
					else if(level < 100)
						color_name = color[3];
					else if(level < 125)
						color_name = color[4];
					else
						color_name = color[5];
					if(sessionStorage.getItem("status") == data[0].spins[i].userid)
						my_bet_html += '<div class="flexible russian_roulette_players"> <div class="text-left player_list_left color-'+color_name+'">';
					else
						buffer += '<div class="flexible russian_roulette_players"> <div class="text-left player_list_left color-'+color_name+'">';
					
					if (level < 50) {
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '<span class="color-'+color[0]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-inactive.png"/>';
						else
							buffer += '<span class="color-'+color[0]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-inactive.png"/>';
					} else if (level < 60) {
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '<span class="color-'+color[1]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-orange.png"/>';
						else
							buffer += '<span class="color-'+color[1]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-orange.png"/>';
					} else if (level < 85) {
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '<span class="color-'+color[2]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-blule.png"/>';
						else
							buffer += '<span class="color-'+color[2]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-blule.png"/>';
					} else if (level < 100) {
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '<span class="color-'+color[3]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-pink.png"/> ';
						else
							buffer += '<span class="color-'+color[3]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-pink.png"/> ';
					} else if (level < 125) {
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '<span class="color-'+color[4]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red.png"/>';
						else
							buffer += '<span class="color-'+color[4]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red.png"/>';
					} else {
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '<span class="color-'+color[5]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-orange.png"/>';
						else
							buffer += '<span class="color-'+color[5]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-orange.png"/>';
					}
					if(sessionStorage.getItem("status") == data[0].spins[i].userid)
						my_bet_html += '<span>&nbsp;'+data[0].spins[i].user+'</span></span></div>';
					else
						buffer += '<span>&nbsp;'+data[0].spins[i].user+'</span></span></div>';
					
					
					if(sessionStorage.getItem("status") == data[0].spins[i].userid){
						my_bet_html += '<div class="text-right player_list_center">';
						if(data[0].spins[i].wager >= 1000000000000){
							if(parseFloat(data[0].spins[i].wager/1000000000000) % 1 == 0)
								my_bet_html += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000000)+'T</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
							else
								my_bet_html += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000000).toFixed(2)+'T</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						}
						else if(data[0].spins[i].wager >= 1000000000){
							if(parseFloat(data[0].spins[i].wager/1000000000) % 1 == 0)
								my_bet_html += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000)+'B</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
							else
								my_bet_html += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000).toFixed(2)+'B</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						}
						else if(data[0].spins[i].wager >= 1000000){
							if(parseFloat(data[0].spins[i].wager/1000000) % 1 == 0)
								my_bet_html += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000)+'M</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
							else
								my_bet_html += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000).toFixed(2)+'M</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						}
						else
							my_bet_html += '	<span class="color-active">'+formatNumber(data[0].spins[i].wager)+'</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						my_bet_html += '</div>';
						my_bet_html += '<div class="text-right player_list_right">';
					}else{
						buffer += '<div class="text-right player_list_center">';
						if(data[0].spins[i].wager >= 1000000000000){
							if(parseFloat(data[0].spins[i].wager/1000000000000) % 1 == 0)
								buffer += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000000)+'T</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
							else
								buffer += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000000).toFixed(2)+'T</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						}
						else if(data[0].spins[i].wager >= 1000000000){
							if(parseFloat(data[0].spins[i].wager/1000000000) % 1 == 0)
								buffer += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000)+'B</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
							else
								buffer += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000000).toFixed(2)+'B</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						}
						else if(data[0].spins[i].wager >= 1000000){
							if(parseFloat(data[0].spins[i].wager/1000000) % 1 == 0)
								buffer += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000)+'M</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
							else
								buffer += '	<span class="color-active">'+parseFloat(data[0].spins[i].wager/1000000).toFixed(2)+'M</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						}
						else
							buffer += '	<span class="color-active">'+formatNumber(data[0].spins[i].wager)+'</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
							
						buffer += '</div>';
						buffer += '<div class="text-center player_list_right">';
					}
					
					if(cur_step == 4 || cur_step == 8){
						var spin_amount = data[0].spins[i].wager;
						var win_amount = data[0].new_round_result[0];
						if(data[0].new_round_result[2] == 1){
							win_amount = parseFloat((data[0].new_round_result[0] * 0.815).toFixed(2));
							win_amount = parseFloat((spin_amount*win_amount).toFixed(2));
						}
						else{
							win_amount = parseFloat((data[0].new_round_result[0] * data[0].new_round_result[2]).toFixed(2));
							win_amount = parseFloat((spin_amount*win_amount).toFixed(2));
						}
						if(data[0].new_round_result[1] == 20)
							win_amount = win_amount * 2;
						if(max_win_amount < win_amount)
							max_win_amount = win_amount;
						if(sessionStorage.getItem("status") == data[0].spins[i].userid){
							if(win_amount >= 1000000000000){
								if(parseFloat(win_amount/1000000000000) % 1 == 0)
									my_bet_html += '<span class="color-orange">'+parseFloat(win_amount/1000000000000)+'T <i class="fas fa-gem color-orange"></i></span>';
								else
									my_bet_html += '<span class="color-orange">'+parseFloat(win_amount/1000000000000).toFixed(2)+'T <i class="fas fa-gem color-orange"></i></span>';
							}
							else if(win_amount >= 1000000000){
								if(parseFloat(win_amount/1000000000) % 1 == 0)
									my_bet_html += '<span class="color-orange">'+parseFloat(win_amount/1000000000)+'B <i class="fas fa-gem color-orange"></i></span>';
								else
									my_bet_html += '<span class="color-orange">'+parseFloat(win_amount/1000000000).toFixed(2)+'B <i class="fas fa-gem color-orange"></i></span>';
							}								
							else if(win_amount >= 1000000){
								if(parseFloat(win_amount/1000000) % 1 == 0)
									my_bet_html += '<span class="color-orange">'+parseFloat(win_amount/1000000)+'M <i class="fas fa-gem color-orange"></i></span>';
								else
									my_bet_html += '<span class="color-orange">'+parseFloat(win_amount/1000000).toFixed(2)+'M <i class="fas fa-gem color-orange"></i></span>';
							}
							else
								my_bet_html += '<span class="color-orange">'+formatNumber(win_amount)+' <i class="fas fa-gem color-orange"></i></span>';
						}
						else{
							if(win_amount >= 1000000000000){
								if(parseFloat(win_amount/1000000000000) % 1 == 0)
									buffer += '	<span class="color-orange">'+parseFloat(win_amount/1000000000000)+'T <i class="fas fa-gem color-orange"></i></span>';
								else
									buffer += '	<span class="color-orange">'+parseFloat(win_amount/1000000000000).toFixed(2)+'T <i class="fas fa-gem color-orange"></i></span>';
							}
							else if(win_amount >= 1000000000){
								if(parseFloat(win_amount/1000000000) % 1 == 0)
									buffer += '	<span class="color-orange">'+parseFloat(win_amount/1000000000)+'B <i class="fas fa-gem color-orange"></i></span>';
								else
									buffer += '	<span class="color-orange">'+parseFloat(win_amount/1000000000).toFixed(2)+'B <i class="fas fa-gem color-orange"></i></span>';
							}
							else if(win_amount >= 1000000){
								if(parseFloat(win_amount/1000000) % 1 == 0)
									buffer += '	<span class="color-orange">'+parseFloat(win_amount/1000000)+'M <i class="fas fa-gem color-orange"></i></span>';
								else
									buffer += '	<span class="color-orange">'+parseFloat(win_amount/1000000).toFixed(2)+'M <i class="fas fa-gem color-orange"></i></span>';
							}
							else
								buffer += '	<span class="color-orange">'+formatNumber(win_amount)+' <i class="fas fa-gem color-orange"></i></span>';
						}
					} else if(cur_step == 5 || cur_step == 7){
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '	<span class="color-orange">0</span> <i class="fas fa-gem color-orange"></i>';
						else
							buffer += '	<span class="color-orange">0</span> <i class="fas fa-gem color-orange"></i>';
					}else{
						if(sessionStorage.getItem("status") == data[0].spins[i].userid)
							my_bet_html += '	<span class="color-orange">---</span> <i class="fas fa-gem color-orange"></i>';
						else
							buffer += '	<span class="color-orange">---</span> <i class="fas fa-gem color-orange"></i>';
					}
					if(sessionStorage.getItem("status") == data[0].spins[i].userid)
						my_bet_html += '</div></div>';
					else
						buffer += '</div></div>';
				}
				$("#bodypart3 .player_list").html(my_bet_html+buffer);
				//var width = window.innerWidth;
				//if(width > 1023)
				$("#bodypart3 .player_list").get(0).scrollIntoView(false);
				//$('.player_win').html(formatNumber(max_win_amount));
			}
			$('#bodypart2 .player_count').text(formatNumber(data[0].spin_round_player.count));
			if(data[0].spin_round_player.total_wager >= 1000000000000){
				if(parseFloat(data[0].spin_round_player.total_wager/1000000000000) % 1 == 0)
					$('#bodypart2 .player_wager').html(parseFloat(data[0].spin_round_player.total_wager/1000000000000)+"T");
				else
					$('#bodypart2 .player_wager').html(parseFloat(data[0].spin_round_player.total_wager/1000000000000).toFixed(2)+"T");
			}
			else if(data[0].spin_round_player.total_wager >= 1000000000){
				if(parseFloat(data[0].spin_round_player.total_wager/1000000000) % 1 == 0)
					$('#bodypart2 .player_wager').html(parseFloat(data[0].spin_round_player.total_wager/1000000000)+"B");
				else
					$('#bodypart2 .player_wager').html(parseFloat(data[0].spin_round_player.total_wager/1000000000).toFixed(2)+"B");
			}
			else if(data[0].spin_round_player.total_wager >= 1000000){
				if(parseFloat(data[0].spin_round_player.total_wager/1000000) % 1 == 0)
					$('#bodypart2 .player_wager').html(parseFloat(data[0].spin_round_player.total_wager/1000000)+"M");
				else
					$('#bodypart2 .player_wager').html(parseFloat(data[0].spin_round_player.total_wager/1000000).toFixed(2)+"M");
			}
			else
				$('#bodypart2 .player_wager').text(formatNumber(data[0].spin_round_player.total_wager));
			
		});

		socket.on('initial chats', function(data) {
			var width = window.innerWidth;
			$("#chat").empty();
			$("#chat1").empty();
			var color = ["", "inactive", "red-orange", "dark-blue", "dark-pink", "red", "orange"];
			var brags = [0, 0, 200, 400, 600, 800, 1000];
			for (var i = 0; i < data.length; i++) {
				var type = parseInt(data[i].type), buffer;
				if (type < 7) {
					// Normal message
					buffer = "<div class=\"chat-1 mb-1\">";
					buffer += ("<span class=\"color-" + color[type] + "\"><i class=\"fas fa-star\"></i> " + data[i].level + " </span><span class=\"color-" + color[type] + "\">" + data[i].user + ": </span><span>" + data[i].text + "</span></div>");
				} else if (type < 13) {
					// Brag message
					buffer = "<div class=\"chat-2 mb-1\"><div>";
					buffer += ("<span class=\"color-" + color[type - 6] + "\"><i class=\"fas fa-gem\"></i></span> " + data[i].user + "<br><label style='margin-left:17px;margin-bottom:0px;'>Sent a "+brags[type - 6]+"</label> <span class=\"color-" + color[type - 6] + "\"><i class=\"fas fa-gem\"></i> TIER "+parseInt(type-6)+" BRAG</span>");
					buffer += ("</div><span class=\"color-" + color[type - 6] + "\"><i class=\"fas fa-star\"></i> " + data[i].level + " " + data[i].user + ": " + data[i].text + "</span>");
					buffer += "</div>";
				} else if (type == 40) {
					// Moderator
					buffer = "<div class=\"chat-1 mb-1\">";
					buffer += ("<span class=\"color-green\">MOD <i class=\"fas fa-wrench\"></i> " + data[i].user + ": " + data[i].text + "</span>");
					buffer += "</div>";
				} else if (type == 99) {
					// Administrator
					buffer = "<div class=\"chat-1 mb-1\">";
					buffer += ("<span class=\"color-yellow\">ADMIN <i class=\"fas fa-user-tie\"></i> " + data[i].user + ": " + data[i].text + "</span>");
					buffer += "</div>";
				} else {
					// Information
					buffer = data[i].text;
				}
				$("#chat").append(buffer);
				$("#chat1").append(buffer);
			}
			if(width > 1023){
				$("#chat").get(0).scrollIntoView(false);
				$("#chat1").get(0).scrollIntoView(false);
			}
		});

		socket.on('new chat', function(data) {
			var width = window.innerWidth;
			var type = parseInt(data.type), buffer;
			var color = ["", "inactive", "red-orange", "dark-blue", "dark-pink", "red", "orange"];
			var brags = [0, 100, 200, 400, 600, 800, 1000];
			if (type < 7) {
				// Normal message
				buffer = "<div class=\"chat-1 mb-1\">";
				buffer += ("<span class=\"color-" + color[type] + "\"><i class=\"fas fa-star\"></i> " + data.level + " </span><span class=\"color-" + color[type] + "\">" + data.user + ":&nbsp;</span>" + data.text + "</div>");
			} else if (type < 13) {
				// Brag message
				buffer = "<div class=\"chat-2 mb-1\"><div>";
				buffer += ("<span class=\"color-" + color[type - 6] + "\"><i class=\"fas fa-gem\"></i></span> " + data.user + "<br><label style='margin-left:17px;'>Sent a "+brags[type - 6]+"</label> <span class=\"color-" + color[type - 6] + "\"><i class=\"fas fa-gem\"></i> TIER "+parseInt(type-6)+" BRAG</span>");
				buffer += ("</div><span class=\"color-" + color[type - 6] + "\"><i class=\"fas fa-star\"></i> " + data.level + " " + data.user + ": " + data.text + "</span>");
				buffer += "</div>";
			} else if (type == 40) {
				// Moderator
				buffer = "<div class=\"chat-1 mb-1\">";
				buffer += ("<span class=\"color-green\">MOD <i class=\"fas fa-wrench\"></i> " + data.user + ":&nbsp;" + data.text + "</span>");
				buffer += "</div>";
			} else if (type == 99) {
				// Administrator
				buffer = "<div class=\"chat-1 mb-1\">";
				buffer += ("<span class=\"color-yellow\">ADMIN <i class=\"fas fa-user-tie\"></i> " + data.user + ":&nbsp;" + data.text + "</span>");
				buffer += "</div>";
			} else {
				// Information
				buffer = data.text;
			}
			if(width > 1023){
				$("#chat").append(buffer);
				$("#chat").get(0).scrollIntoView(false);
			}
			else{
				$("#chat1").append(buffer);
			}
		});
		
		socket.on('spin user', function(data) {
			var color = ["inactive", "red-orange", "dark-blue", "dark-pink", "red", "orange"], buffer="";
			if(data['is_exist'] == 0){
				var level = parseInt(data['data']['level']), buffer;
				
				var color_name = '';
				if(level < 50)
					color_name = color[0];
				else if(level < 60)
					color_name = color[1];
				else if(level < 85)
					color_name = color[2];
				else if(level < 100)
					color_name = color[3];
				else if(level < 125)
					color_name = color[4];
				else
					color_name = color[5];
					
				buffer = '<div class="flexible russian_roulette_players"><div class="text-left player_list_left color-'+color_name+'">';
				
				if (level < 50) {				
					buffer += '<span class="color-'+color[0]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-inactive.png"/> ';
				} else if (level < 60) {
					buffer += '<span class="color-'+color[1]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red-orange.png"/> ';
				} else if (level < 85) {
					buffer += '<span class="color-'+color[2]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-blue.png"/> ';
				} else if (level < 100) {
					buffer += '<span class="color-'+color[3]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-pink.png"/> ';
				} else if (level < 125) {
					buffer += '<span class="color-'+color[4]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red.png"/> ';
				} else {
					buffer += '<span class="color-'+color[5]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-orange.png"/> ';
				}
				
				buffer += '<span>&nbsp;'+data['data']['user']+'</span></span>';
				buffer += '</div>';
				buffer += '<div class="text-right player_list_center">';
				buffer += '	<span class="color-active">'+formatNumber(data['data']['wager'])+'</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
				buffer += '</div>';
				buffer += '<div class="text-center player_list_right">';
				buffer += '	<span class="color-active">---</span>';
				buffer += '</div></div>';
				var html = $('#bodypart3 .player_list').html();
				$("#bodypart3 .player_list").empty();
				$("#bodypart3 .player_list").append(buffer+html);
			}else{
				
				$('.russian_roulette_players').each(function(i,obj){
					if($.trim($(this).find('.player_list_left span span').text()) == data['data']['user']){
						var spin_amount = parseInt(remove_character($(this).find('.player_list_center span').text()));
						spin_amount += parseInt(data['data']['wager']);
						
						var buffer = '';
						var level = parseInt(data['data']['level']);
						
						buffer = '<div class="flexible russian_roulette_players">';
						
						if (level < 50){
							buffer += '<span class="color-'+color[0]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-inactive.png"/> ';
						} else if (level < 60) {
							buffer += '<span class="color-'+color[1]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red-orange.png"/> ';
						} else if (level < 85) {
							buffer += '<span class="color-'+color[2]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-blue.png"/> ';
						} else if (level < 100) {
							buffer += '<span class="color-'+color[3]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-pink.png"/> ';
						} else if (level < 125) {
							buffer += '<span class="color-'+color[4]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red.png"/> ';
						} else {
							buffer += '<span class="color-'+color[5]+'"><i class="fas fa-star"></i>&nbsp;'+level+'&nbsp;<img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-orange.png"/> ';
						}
						buffer += '<span>&nbsp;'+data['data']['user']+'</span></span>';
						$(this).find('.player_list_left').html(buffer);
						$(this).find('.player_list_center').empty();
						var temp = '<span class="color-active">'+formatNumber(spin_amount)+'</span>&nbsp;<span class="color-orange"><i class="fas fa-gem"></i></span>';
						$(this).find('.player_list_center').html(temp);
					}
					
				});
			}
		});
		
		socket.on('initial top winner', function(data) {
			var color = ["inactive", "red-orange", "dark-blue", "dark-pink", "red", "orange"],buffer="";
			for(var i =0;i<data.length;i++){
				var level = parseInt(data[i].player_level);
				buffer += '<div class="flexible top_winners_list"> <div class="text-center top_player_rank"><span class="color-orange small">'+parseInt(i+1)+'</span></div>';
				
				var color_name = '';
				if(level < 50)
					color_name = color[0];
				else if(level < 60)
					color_name = color[1];
				else if(level < 85)
					color_name = color[2];
				else if(level < 100)
					color_name = color[3];
				else if(level < 125)
					color_name = color[4];
				else
					color_name = color[5];
				
				buffer += '<div class="text-left top_player_info color-'+color_name+'">';
				if (level < 50) {
					buffer += '<span class="color-'+color_name+' small"><i class="fas fa-star"></i> '+level+' <img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-inactive.png"/> ';
				} else if (level < 60) {
					buffer += '<span class="color-'+color_name+' small"><i class="fas fa-star"></i> '+level+' <img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red-orange.png"/> ';
				} else if (level < 85) {
					buffer += '<span class="color-'+color_name+' small"><i class="fas fa-star"></i> '+level+' <img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-blue.png"/> ';
				} else if (level < 100) {
					buffer += '<span class="color-'+color_name+' small"><i class="fas fa-star"></i> '+level+' <img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-dark-pink.png"/> ';
				} else if (level < 125) {
					buffer += '<span class="color-'+color_name+' small"><i class="fas fa-star"></i> '+level+' <img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/cylinder-loaded-red.png"/> ';
				} else {
					buffer += '<span class="color-'+color_name+' small"><i class="fas fa-star"></i> '+level+' <img class="top_winner_cylinder" src="<?php echo base_url();?>assets/images/Gold_Cyl.svg"/> ';
				}
				
				buffer += '<span>'+data[i].player_name+'</span></span></div><div class="text-right top_player_wager">';
				if(data[i].wager >= 1000000000000){
					if(parseFloat(data[i].wager/1000000000000) % 1 == 0)
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].wager)+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(data[i].wager/1000000000000)+'T <i class="fas fa-gem"></i></span>';
					else
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].wager)+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(Math.floor(data[i].wager/10000000000))/100+'T <i class="fas fa-gem"></i></span>';
				}
				else if(data[i].wager >= 1000000000){
					if(parseFloat(data[i].wager/1000000000) % 1 == 0)
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].wager)+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(data[i].wager/1000000000)+'B <i class="fas fa-gem"></i></span>';
					else
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].wager)+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(Math.floor(data[i].wager/10000000))/100+'B <i class="fas fa-gem"></i></span>';
				}
				else if(data[i].wager >= 1000000){
					if(parseFloat(data[i].wager/1000000) % 1 == 0)
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].wager)+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(data[i].wager/1000000)+'M <i class="fas fa-gem"></i></span>';
					else
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].wager)+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(Math.floor(data[i].wager/10000))/100+'M <i class="fas fa-gem"></i></span>';
				}
				else
					buffer += '	<span class="color-orange small">'+formatNumber(data[i].wager)+' <i class="fas fa-gem"></i></span>';
				buffer += '</div><div class="text-right top_player_win">';
				
				if(data[i].won >= 1000000000000){
					if(parseFloat(data[i].won/1000000000000) % 1 == 0)
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].won.toFixed(2))+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(data[i].won/1000000000000)+'T <i class="fas fa-gem"></i></span>';
					else
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].won.toFixed(2))+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(Math.floor(data[i].won/10000000000))/100+'T <i class="fas fa-gem"></i></span>';
				}
				else if(data[i].won >= 1000000000){
					if(parseFloat(data[i].won/1000000000) % 1 == 0)
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].won.toFixed(2))+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(data[i].won/1000000000)+'B <i class="fas fa-gem"></i></span>';
					else
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].won.toFixed(2))+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(Math.floor(data[i].won/10000000))/100+'B <i class="fas fa-gem"></i></span>';
				}
				else if(data[i].won >= 1000000){
					if(parseFloat(data[i].won/1000000) % 1 == 0)
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].won.toFixed(2))+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(data[i].won/1000000)+'M <i class="fas fa-gem"></i></span>';
					else
						buffer += '	<span style="cursor:pointer;" class="color-orange small" data-toggle="tooltip" data-html="true" rel="tooltip" data-placement="auto" title="'+formatNumber(data[i].won.toFixed(2))+' <i class=\'fas fa-gem color-orange\'></i>">'+parseFloat(Math.floor(data[i].won/10000))/100+'M <i class="fas fa-gem"></i></span>';
				}
				else
					buffer += '	<span class="color-orange small">'+formatNumber(data[i].won)+' <i class="fas fa-gem"></i></span>';
				
				buffer += '</div></div>';
			}
			
			$("#bodypart3 .top_player_list").empty();
			$("#bodypart3 .top_player_list").html(buffer);
			$('[data-toggle="tooltip"]').tooltip();
		});

		socket.on('users connected', function(data){
			$("#onlineusers").html("<span class=\"color-orange\"><i class=\"fas fa-circle\"></i> </span>Online: " + formatNumber(data));
		});

		status = sessionStorage.getItem("status");
		if(status == "null")
			status = "";

		var formData = new FormData();
		
		if (status == "null" || status == "" || status == null) {
			status = "";
			$("#speedregisterregion").fadeIn(0);
			//$(".chattext").prop("placeholder", "Please log in ...");
			//$(".chattext").get(0).disabled = true;
			//$(".chattext1").get(0).disabled = true;
			//$("#preloader").fadeOut("fast");
		} else {
			
			$.ajax({
				url: '<?php echo site_url("Home/get_user/'+status+'");?>',
				success: function (data) {
					var obj = JSON.parse(data);
					if (obj.success == "true" && !jQuery.isEmptyObject(obj.data)) {
						change_exp(obj.data);
					}
					//$("#preloader").fadeOut("fast");
				},
				error: function (error) {
					$("#preloader").fadeOut("fast");
					message('error', 'Internal server error occurred. Please try again later.');
					console.log(error);
				}
			});
		}

		align();
		
		$('.brag_section').click(function(){
			$('.brag-menu').css('display','none');
		});
		
		$('.spin_btn').click(function(){
			if(sessionStorage.getItem("status") == null){
				$("#signupmodal1").modal("toggle");
				//message('warning', 'Please login!');
			}else{
				if($('.autoplay_spin').css('display') != 'none')
					$('.autoplay_spin').css('display','none');
				else
					$('.autoplay_spin').css('display','block');
			}
		});
		
		$('.play_btn').click(function(){
			if($('.max_bet_btn').css('color') == 'rgb(0, 0, 0)' && is_freespin_round == 0){
				if(sessionStorage.getItem("status") == null){
					$("#signupmodal1").modal("toggle");
				}
			}
		});
		
		$('.max_bet_btn').click(function(){
			if($('.max_bet_btn').css('color') == 'rgb(0, 0, 0)' && is_freespin_round == 0){
				if(sessionStorage.getItem("status") == null){
					$("#signupmodal1").modal("toggle");
				}
			}
		});
		
		$('.depositmodal_decline').click(function(){
			$('#crypto_deposit').css('display','block');
			$('#deposit').css('display','none');
		});
		
		$('.depositmodal_activate').click(function(){
			$('#crypto_deposit').css('display','block');
			$('#deposit').css('display','none');
		});
		
		$('#trade_url_confirm').click(function(){
			var trade_url = $('#user_trade_url').val();
			sessionStorage.setItem('vgo_trade_url',trade_url);
			$('#tradeUrl').css('display','none');
			$('#tradeUrl').removeClass("active show");
			$('#vgo_deposit').css('display','block');
			$('#vgo_deposit').addClass("active show");
			$('#preloader').fadeIn("fast");
			var html = "";
			$.ajax({
				url: '<?php echo site_url("Home/get_vgo_items");?>',
				data: {trade_url:sessionStorage.getItem("vgo_trade_url"),type:1,uid:sessionStorage.getItem('status')},
				success: function (data) {
					var obj = JSON.parse(data);
					if(obj.status == 1){
						for(var i = 0;i<obj.item.length;i++){
							html += '<div class="vgo_item_each_section" onclick="vgo_item_sel('+obj.item[i].id+')">';
							html += '	<div style="text-align:right;font-size:12px;" class="mr-1"><i class="fa fa-eye color-inactive"></i></div>';
							html += '	<div>';
							var item_name = obj.item[i].name;
							temp = item_name.split('(');
							temp[1] = temp[1].replace(")","");
							html += '		<p class="color-inactive margin-bottom-0" style="font-size:11px;">'+temp[0]+'</p>';
							html += '		<p class="color-inactive" style="font-size:11px;">'+temp[1]+'</p>';
							html += '	</div>';
							html += '	<div>';
							html += '		<img src="'+obj.item[i].image['300px']+'" style="width:80px;margin-top:-20px;"/>';
							html += '	</div>';
							html += '	<div >';
							var price = obj.item[i].suggested_price/Math.pow(10,obj.item[i].suggested_price_floor)*1000;
							html += '		<label class="color-active" style="font-size:12px;">'+formatNumber(price)+' <i class="fas fa-gem color-orange"></i></label>';
							html += '	</div>';
							html += '</div>';
						}
						$('#vgo_item_section').html(html);
					} else if(obj.status == 106){
						alert(obj.message);
					}
					//$('#preloader').fadeOut("fast");
				},
				error: function (error) {
					$('#preloader').fadeOut("fast");
				}
			});	
		});
		
		$('body').keyup(function(e){
			if(e.keyCode == 32){
				spin('');
		   }
		});
		
		$('body').click(function(evt){    
			$('.autoplay_spin').css('display','none');
			$('.autoplay_spin_mobile').css('display','none');
			$('.wager_mobile').css('display','none');
			
		});
		$('#spin1').on('click',function(event){event.stopPropagation();});
		$('#spin2').on('click',function(event){event.stopPropagation();});
		$('#spin3').on('click',function(event){event.stopPropagation();});
		$('#spin5').on('click',function(event){event.stopPropagation();});
		$('#wager_mobile_sel').on('click',function(event){event.stopPropagation();});
		
		
		$('#signup_nickname').on("change paste keyup focus",function(){
			var str = $('#signup_nickname').val();
			var letters = /^[0-9a-zA-Z]+$/;
			$('#signup_nickname').removeClass('white_background');
			if($('#signup_email').val().length < 1)
				$('#signup_email').addClass('white_background');
			if($('#signup_password').val().length < 1){
				$('.signup_show').css('color','black');
				$('.signup_pass').css('color','black');
				$('#signup_password').addClass('white_background');
			}
			
			if(str.length > 15 || str.length == 1 || !str.match(letters)){
				$('#signup_nickname').css('background-color','#191e24');
				$('#signup_nickname').css('color','white');
			}else{
				$('#signup_nickname').css('background-color','#f8bf60');
				$('#signup_nickname').css('color','black');
			}
		});
		
		$('#signup_password').on("change paste keyup focus",function(){
			var password = $('#signup_password').val();
			
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			$('#signup_password').removeClass('white_background');
			if($('#signup_email').val().length < 1)
				$('#signup_email').addClass('white_background');
			if($('#signup_nickname').val().length < 1)
				$('#signup_nickname').addClass('white_background');
			if(contain_number || contain_letter){
				$('#signup_password').css('background-color','#f8bf60');
				$('#signup_password').css('color','black');
			}else{
				$('#signup_password').css('background-color','#191e24');
				$('#signup_password').css('color','white');
			}
			$('.signup_show').css('color','white');
			$('.signup_pass').css('color','white');
		});
		
		$('#signup_email').on("change paste keyup focus",function(){
			var email = $("#signup_email").val();
			var emailregex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			$('#signup_email').removeClass('white_background');
			if($('#signup_nickname').val().length < 1)
				$('#signup_nickname').addClass('white_background');
			if($('#signup_password').val().length < 1){
				$('.signup_show').css('color','black');
				$('.signup_pass').css('color','black');
				$('#signup_password').addClass('white_background');
			}
				
			if(email.match(emailregex)){
				$('#signup_email').css('color','black');
				$('#signup_email').css('background-color','#f8bf60');
			}else{
				$('#signup_email').css('color','white');
				$('#signup_email').css('background-color','#191e24');
			}
			
		});
		
		$('#login_name').on("change paste keyup focus",function(){
			var str = $('#login_name').val();
			$('#login_name').removeClass('white_background');
			if($('#password').val().length < 1){
				$('#password').addClass('white_background');
				$('.login_show_passwd').css('color','black');
				$('.login_show').css('color','black');
			}
				
			var letters = /^[0-9a-zA-Z]+$/;
			if(str.length > 15 || str.length == 1 || !str.match(letters)){
				$('#login_name').css('background-color','#191e24');
				$('#login_name').css('color','white');
			}else{
				$('#login_name').css('background-color','#f8bf60');
				$('#login_name').css('color','black');
			}
			
		});
		
		$('#password').on("change paste keyup focus",function(){
			var password = $('#password').val();
			$('#password').removeClass('white_background');
			if($('#login_name').val().length < 1)
				$('#login_name').addClass('white_background');
			
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			if(contain_number || contain_letter){
				$('#password').css('color','black');
				$('#password').css('background-color','#f8bf60');
			}else{
				$('#password').css('color','white');
				$('#password').css('background-color','#191e24');
			}
			$('.login_show_passwd').css('color','white');
			$('.login_show').css('color','white');
		});
		
		$('#old_password').on("change paste keyup focus",function(){
			var password = $('#old_password').val();
			$('#old_password').removeClass('white_background');
			if($('#change_new_pwd').val().length < 1){
				$('.security_pwd').css('color','black');
				$('.security_show').css('color','black');
				$('#change_new_pwd').addClass('white_background');
			}
			
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			if(contain_number || contain_letter){
				$('#old_password').css('color','black');
				$('#old_password').css('background-color','#f8bf60');
			}else{
				$('#old_password').css('color','white');
				$('#old_password').css('background-color','transparent');
			}	
		});
		$('#change_new_pwd').on("change paste keyup focus",function(){
			var password = $('#change_new_pwd').val();
			$('#change_new_pwd').removeClass('white_background');
			if($('#old_password').val().length < 1)
				$('#old_password').addClass('white_background');
			
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			if(contain_number || contain_letter){
				$('#change_new_pwd').css('color','black');
				$('#change_new_pwd').css('background-color','#f8bf60');
			}else{
				$('#change_new_pwd').css('color','white');
				$('#change_new_pwd').css('background-color','transparent');
			}
			$('.security_pwd').css('color','white');
			$('.security_show').css('color','white');
		});
		
		$('#fa_code').on("change paste keyup focus",function(){
			var password = $('#fa_code').val();
			$('#fa_code').removeClass('white_background');
			if($('#fa_pwd').val().length < 1){
				$('.fa_pwd').css('color','black');
				$('.fa_show').css('color','black');
				$('#fa_pwd').addClass('white_background');
			}
						
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			if(contain_number || contain_letter){
				$('#fa_code').css('color','black');
				$('#fa_code').css('background-color','#f8bf60');
			}else{
				$('#fa_code').css('color','white');
				$('#fa_code').css('background-color','transparent');
			}	
		});
		$('#fa_pwd').on("change paste keyup focus",function(){
			var password = $('#fa_pwd').val();
			$('#fa_pwd').removeClass('white_background');
			if($('#fa_code').val().length < 1)
				$('#fa_code').addClass('white_background');
			
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			if(contain_number || contain_letter){
				$('#fa_pwd').css('color','black');
				$('#fa_pwd').css('background-color','#f8bf60');
			}else{
				$('#fa_pwd').css('color','white');
				$('#fa_pwd').css('background-color','transparent');
			}
			$('.fa_pwd').css('color','white');
			$('.fa_show').css('color','white');
		});
		
		$('#signup2_email1').on("change paste keyup",function(){
			var email = $("#signup2_email1").val();
			var emailregex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(email.match(emailregex)){
				$('#signup2_email1').css('background-color','#f8bf60');
				$('#signup2_email1').css('color','black');
				$('.form-control-placeholder.email1_put').css('color','black');
			}else{
				$('#signup2_email1').css('background-color','#191e24');
				$('#signup2_email1').css('color','white');
				$('.form-control-placeholder.email1_put').css('color','#a3a09b');
			}
		});
		
		$('#signup_password2_1').on("change paste keyup",function(){
			var password = $('#signup_password2_1').val();
			var re_password = $('#signup_password2_2').val();
			
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			if((contain_number || contain_letter) && password == re_password){
				$('#signup_password2_1').css('background-color','#f8bf60');
				$('#signup_password2_1').css('color','black');
				$('#signup_password2_2').css('background-color','#f8bf60');
				$('#signup_password2_2').css('color','black');
				$('.form-control-placeholder.pwd2_put').css('color','black');
				$('.form-control-placeholder.pwd3_put').css('color','black');
			}else{
				$('#signup_password2_1').css('background-color','#191e24');
				$('#signup_password2_1').css('color','white');
				$('#signup_password2_2').css('background-color','#191e24');
				$('#signup_password2_2').css('color','white');
				$('.form-control-placeholder.pwd2_put').css('color','#a3a09b');
				$('.form-control-placeholder.pwd3_put').css('color','#a3a09b');
			}
		});
		
		
		
		$('#adv_nickname').on("change paste keyup",function(){
			var adv_nick_name = $('#adv_nickname').val();
			var letters = /^[0-9a-zA-Z]+$/;
			$('#adv_nickname1').val(adv_nick_name);
			if(adv_nick_name.length > 15 || adv_nick_name.length == 1 || !adv_nick_name.match(letters)){
				$('#adv_nickname').css('background-color','#191e24');
				$('#adv_nickname').css('color','white');
				$('.form-control-adv.name_put').css('color','white');
			}else{
				$('#adv_nickname').css('background-color','#f8bf60');
				$('#adv_nickname').css('color','black');
				$('.form-control-adv.name_put').css('color','black');
			}
		});
		
		$('#left_reg_name').on("change paste keyup",function(){
			var adv_nick_name = $('#left_reg_name').val();
			var letters = /^[0-9a-zA-Z]+$/;
			$('#adv_nickname1').val(adv_nick_name);
			if(adv_nick_name.length > 15 || adv_nick_name.length == 1 || !adv_nick_name.match(letters)){
				$('#left_reg_name').css('background-color','#191e24');
				$('#left_reg_name').css('color','white');
				$('.form-control-left.login_name').css('color','white');
			}else{
				$('#left_reg_name').css('background-color','#f8bf60');
				$('#left_reg_name').css('color','black');
				$('.form-control-left.login_name').css('color','black');
			}
		});
		
		$('#adv_nickname1').on("change paste keyup",function(){
			var adv_nick_name = $('#adv_nickname1').val();
			var letters = /^[0-9a-zA-Z]+$/;
			if(adv_nick_name.length > 15 || adv_nick_name.length == 1 || !adv_nick_name.match(letters)){
				$('#adv_nickname1').css('background-color','#191e24');
				$('#adv_nickname1').css('color','white');
				$('.form-control-adv.name_put').css('color','white');
			}else{
				$('#adv_nickname1').css('background-color','#f8bf60');
				$('#adv_nickname1').css('color','black');
				$('.form-control-adv.name_put').css('color','black');
			}
		});
		
		$('#signup_password2_2').on("change paste keyup",function(){
			var password = $('#signup_password2_1').val();
			var re_password = $('#signup_password2_2').val();
			
			var contain_number = hasLetterNumber(password);
			var contain_letter = hasLetterSpecial(password);
			if((contain_number || contain_letter) && password == re_password){
				$('#signup_password2_1').css('background-color','#f8bf60');
				$('#signup_password2_1').css('color','black');
				$('#signup_password2_2').css('background-color','#f8bf60');
				$('#signup_password2_2').css('color','black');
				$('.form-control-placeholder.pwd2_put').css('color','black');
				$('.form-control-placeholder.pwd3_put').css('color','black');
			}else{
				$('#signup_password2_1').css('background-color','#191e24');
				$('#signup_password2_1').css('color','white');
				$('#signup_password2_2').css('background-color','#191e24');
				$('#signup_password2_2').css('color','white');
				$('.form-control-placeholder.pwd2_put').css('color','#a3a09b');
				$('.form-control-placeholder.pwd3_put').css('color','#a3a09b');
			}
		});
		
		$("#adv_nickname").keydown(function(e) {
			if (e.keyCode == 13)
			{
			    e.preventDefault();
			}
		});
	});

	$(window).resize(function() {
		align();
	});

	function align() {
		var width = window.innerWidth;
		var height = window.innerHeight;
		if(current_size == 0){
			current_size = width;
		}
		
		defualt_roll_width = 109;
		roulette_bar_width = 42*defualt_roll_width;
		if (status != "null" && status != "" && status != null){
			$('.before_login').css('display','none');
			$('.before_login_mobile').css('display','none');
		}
		
		if(width > 1023){
			if (status != "null" && status != "" && status != null) {
				$("#mainboard").css('overflow-y','hidden');
				if(width > 1580)
					$("div.tab-content").css("height", (height - $('.chat-header-part').height() - $('.personal-level-part').height() - $('.left-chat').height() - $('#chatbox').height() - $('#status').height() - 70) + "px");
				else
					$("div.tab-content").css("height", (height - $('.chat-header-part').height() - $('.personal-level-part').height() - $('.left-chat').height() - $('#chatbox').height() - $('#status').height() - 55) + "px");
					
				$('.chat-header-part').css('display','block');
				$('.personal-level-part').css('display','block');
				
				$("#mainboard").css("width", (width - $('.left-side').width() - 12 - width/100) + "px");
				
				var width_bodypart = parseInt($("#mainboard").css("width"));
				$("#exppart").css("width", (width_bodypart - $(".levelpart1-section").width() - $(".levelpart2-section").width() - 43) + "px");

				if(width > 1516){
					$("#bodypart1").children().eq(1).css("width", (width_bodypart - 10 - 2*width_bodypart/100*20) + "px");
					$("#bodypart2").children().eq(1).css("width", (width_bodypart - 10 - 2*width_bodypart/100*20) + "px");
					$("#bodypart3").children().eq(1).css("width", (width_bodypart - 10 - 2*width_bodypart/100*20) + "px");	
					$(".footerbody").css("width", (width_bodypart - width_bodypart/100*20*2-5) + "px");
					$('.bonus_roll').css("width",(width_bodypart - 10 - 2*width_bodypart/5) + "px");
				}else if(width > 1200){
					$("#bodypart1").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart2").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart3").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");	
					$(".footerbody").css("width", (width_bodypart - 500-5) + "px");
					$('.bonus_roll').css("width",(width_bodypart - 10 - 500) + "px");
				}else{
					$("#bodypart1").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart2").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart3").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");	
					$(".footerbody").css("width", (width_bodypart - $('.footer-1').width() - $('.footer-3').width() - 15 - width/100*0.8) + "px");
					$('.bonus_roll').css("width",(width_bodypart - 10 - 500) + "px");
				}
				
				$("#bodypart3").css("height", (height - $(".footer").height() - $('#bodypart1').height() - $('#bodypart2').height() - $('.top_header').height() - 63) + "px");
				var height_revolver = parseInt($("#bodypart3").children().eq(1).css("height")) - parseInt($('.round_result').height()) - 5;
				$("#revolver").css("height", height_revolver + "px");
				
				if($('#revolver').width() < height_revolver){
					$('.gun_shot_image').css('width',$('#revolver').width());
					$('.gun_shot_image').css('height','auto');
				}else{
					$('.gun_shot_image').css('width','auto');
					$('.gun_shot_image').css('height',height_revolver + "px");
				}
				var revolver_width = $('#revolver').width();
				win_countup_font_size = revolver_width/13;
				
				$(".counter").css("height",height_revolver + "px");
				$(".counter span").css("font-size",win_countup_font_size+"px");
				if(width > 1502){
					$(".counter_title").css("width",(width_bodypart - 10 - 2*width_bodypart/5) + "px");	
					$(".counter").css("width",(width_bodypart - 10 - 2*width_bodypart/5) + "px");
				}else{
					$(".counter_title").css("width",(width_bodypart - 510) + "px");	
					$(".counter").css("width",(width_bodypart - 510) + "px");
				}
				
				$(".counter_title").css("height",parseInt(height_revolver-win_countup_font_size*2 - 10) + "px");
				$(".counter_title span").css("font-size",win_countup_font_size+"px");
				$('.black_fade').css('top',$('.top_header').height()+$('#bodypart0').height()+10);
				$('.top_header').css("position",'static');
			} else {
				$("#mainboard").css('overflow-y','scroll');
				if(width > 1580)
					$("div.tab-content").css("height", (height - $('.chat-header-part').height() - $('#speedregisterregion').height() - $('.left-chat').height() - $('#chatbox').height() - $('#status').height() - 44) + "px");
				else
					$("div.tab-content").css("height", (height - $('.chat-header-part').height() - $('#speedregisterregion').height() - $('.left-chat').height() - $('#chatbox').height() - $('#status').height() - 33) + "px");
				$("#mainboard").css("width", (width - $('.left-side').width() - 12 - width/100) + "px");
				$('.top_header').css("width", (width - $('.left-side').width() - 12 - width/100) + "px");
				
				var width_bodypart = parseInt($("#bodypart1").css("width"));
				
				if(width > 1516){
					$("#bodypart1").children().eq(1).css("width", (width_bodypart - 10 - 2*width_bodypart/100*20) + "px");
					$("#bodypart2").children().eq(1).css("width", (width_bodypart - 10 - 2*width_bodypart/100*20) + "px");
					$("#bodypart3").children().eq(1).css("width", (width_bodypart - 10 - 2*width_bodypart/100*20) + "px");	
					$(".footerbody").css("width", (width_bodypart - 2*width_bodypart/100*20-5) + "px");
					$('.bonus_roll').css("width",(width_bodypart - 10 - 2*width_bodypart/5) + "px");
				}else if(width > 1200){
					$("#bodypart1").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart2").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart3").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");	
					$(".footerbody").css("width", (width_bodypart - 500-5) + "px");
					$('.bonus_roll').css("width",(width_bodypart - 10 - 500) + "px");
				} else{
					$("#bodypart1").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart2").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");
					$("#bodypart3").children().eq(1).css("width", (width_bodypart - 10 - 500) + "px");	
					$(".footerbody").css("width", (width_bodypart - $('.footer-1').width() - $('.footer-3').width() - 15 - width/100*0.8) + "px");
					$('.bonus_roll').css("width",(width_bodypart - 10 - 500) + "px");
				}
				
				$("#bodypart3").css("height", (height - $(".footer").height() - $('#bodypart1').height() - $('#bodypart2').height() - $('.top_header').height() - 20) + "px");
				var height_revolver = parseInt($("#bodypart3").children().eq(1).css("height")) - parseInt($('.round_result').height()) - 5;
				
				$("#revolver").css("height", height_revolver + "px");
				
				if($('#revolver').width() < height_revolver){
					$('.gun_shot_image').css('width',$('#revolver').width());
					$('.gun_shot_image').css('height','auto');
				}else{
					$('.gun_shot_image').css('width','auto');
					$('.gun_shot_image').css('height',height_revolver + "px");
				}
				var revolver_width = $('#revolver').width();
				win_countup_font_size = revolver_width/13;
				
				$(".counter").css("height",height_revolver + "px");
				$(".counter span").css("font-size",win_countup_font_size+"px");
				if(width > 1502){
					$(".counter_title").css("width",(width_bodypart - 10 - 2*width_bodypart/5) + "px");	
					$(".counter").css("width",(width_bodypart - 10 - 2*width_bodypart/5) + "px");
				}else{
					$(".counter_title").css("width",(width_bodypart - 510) + "px");	
					$(".counter").css("width",(width_bodypart - 510) + "px");
				}
				
				$(".counter_title").css("height",parseInt(height_revolver-win_countup_font_size*2 - 10) + "px");
				$(".counter_title span").css("font-size",win_countup_font_size+"px");
				$('.black_fade').css('top',$('.top_header').height()+25+$('.before_login').height());
				
				$('.top_header').css("position",'fixed');
				$('#mainboard').css('overflow-x','hidden');
				
			}
			$('.bonus_roll').css('font-size',$('#revolver').width()/8);
			$('.bonus_roll_top').css('margin-top',parseInt(height_revolver/4)+'px');
			$('.bonus_roll_bottom').css('margin-top',parseInt(height_revolver/4*3)+'px');
			
			$('.bonus_won_top').css('margin-top',parseInt(height_revolver/6)+'px');
			$('.bonus_won_bottom').css('margin-top',parseInt(height_revolver/6*5)+'px');
			
			$('#loggedout').css('display','none');
			if(layout_mode == 1 && width > 1023){
				$('.layout_1').css('display','flex');
				$('.layout_2').css('display','none');
				$('.layout_3').css('display','none');
			}
			else if(layout_mode == 2 && width > 1023){
				$('.layout_1').css('display','none');
				$('.layout_2').css('display','flex');
				$('.layout_3').css('display','none');
			}
			else if(layout_mode == 3 && width > 1023){
				$('.layout_1').css('display','none');
				$('.layout_2').css('display','none');
				$('.layout_3').css('display','flex');
			}
			$('#loggedin').css('display','none');
		} else{
			$('.top_header').css('width','100%');
			$('.top_header').css('position','fixed');
			var width_bodypart = parseInt($("#bodypart1").css("width"));
			$("#exppart").css("width", (width_bodypart - $(".levelpart1-section").width() - $(".levelpart2-section").width() - 43) + "px");
			var height_revolver = height/2  - 150;
			
			$("#revolver").css("height", height/2  - 187 + "px");
			
			$('.gun_shot_image').css("height", height/2  - 135 + "px");
			var revolver_width = $('#revolver').width();
			win_countup_font_size = revolver_width/15;
			
			if($('#revolver').width() > $('#revolver').height())
				$('.bonus_roll').css('font-size',$('#revolver').height()/5);
			else
				$('.bonus_roll').css('font-size',$('#revolver').width()/10);
				
			$('#mainboard').css('overflow-x','hidden');
			
			$(".counter_title").css("height",height/2 - 200 - win_countup_font_size + "px");
			$(".counter").css("height",height/2  - 187 + win_countup_font_size + "px");
			$(".counter_title").css("width",$('#bodypart3').width() + "px");
			$(".counter_title span").css("font-size",win_countup_font_size+"px");
			$(".counter").css("width",$('#bodypart3').width() + "px");
			$(".counter span").css("font-size",win_countup_font_size+"px");
			$('#loggedin').css('display','flex');
			if (status != "null" && status != "" && status != null){
				$('.black_fade').css('top','77px');
				$('#loggedin li').css('display','list-item');
			}
			else{
				$('.black_fade').css('top',$('.top_header').height() + $('.before_login_mobile').height()+16);
				$('#loggedout').css('display','block');
				$('#loggedin li:nth-child(3)').css('display','none');
				$('#loggedin li:nth-child(4)').css('display','none');
				$('#loggedin li:nth-child(5)').css('display','none');
				$('#loggedin li:nth-child(6)').css('display','none');
			}
			$('.bonus_roll_top').css('margin-top',parseInt($('.bonus_roll').css('font-size').replace('px',''))/2);
			$('.bonus_roll_bottom').css('margin-top',height/2  - 175 - parseInt($('.bonus_roll').css('font-size').replace('px',''))/2);
			$('.bonus_won_top').css('margin-top',parseInt($('.bonus_roll').css('font-size').replace('px',''))/2);
			$('.bonus_won_bottom').css('margin-top',height/2  - 175 - parseInt($('.bonus_roll').css('font-size').replace('px',''))/2);
				
			$('.bonus_roll_top').css('width','100%');
			$('.bonus_roll_bottom').css('width','100%');
			$('.bonus_won_top').css('width','100%');
			$('.bonus_won_bottom').css('width','100%');
			if (status != "null" && status != "" && status != null) {
				if(width > 600){
					$("div.tab-content").css("height", (height/2 - 111 - height*10.5/100 - $('.left-side-mobile ul.nav.nav-tabs').height()) + "px");
				}
				else
					$("div.tab-content").css("height", (height/2 - 106 - height*10.5/100 - $('.left-side-mobile ul.nav.nav-tabs').height()) + "px");
			}else{
				//$('.before_login').css('margin-top','0px');
				if(width > 600)
					$("div.tab-content").css("height", (height/2 - 210) + "px");
				else
					$("div.tab-content").css("height", (height/2 - 194) + "px");
				
			}
			
		}
		if (status != "null" && status != "" && status != null) {
			$('#loginbutton').css('display','none');
			$('#signupbutton').css('display','none');
			$('#header_depositbutton').css('display','block');
			$('#withdrawbutton').css('display','block');
			$('#accountbutton').css('display','block');
			$('#rewardsbutton').css('display','block');
			$('#bodypart0').css('display','flex');
		}else{
			if(width>1023){
				$('#loginbutton').css('display','block');
				$('#signupbutton').css('display','block');
			}else{
				$('#loginbutton').css('display','none');
				$('#signupbutton').css('display','none');
			}
			
			$('#header_depositbutton').css('display','none');
			$('#withdrawbutton').css('display','none');
			$('#accountbutton').css('display','none');
			$('#rewardsbutton').css('display','none');
			$('#bodypart0').css('display','none');
		}
		if(width > 1023)
			$('.black_fade').css('height',$('#mainbodypart').height()+2);
		else
			$('.black_fade').css('height',$('#mainbodypart').height());
		
		if(layout_mode == 1 && width > 1023){
			$('.layout_1').css('display','flex');
			$('.layout_2').css('display','none');
			$('.layout_3').css('display','none');
		}
		socket.emit("reset top winner");
	}

	function formatNumber(numberString) {
		return ("" + numberString).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	$("#speedregisterbutton").click(function() {
		var letters = /^[0-9a-zA-Z]+$/;
		var nickname = $("#speedregisterregion input").val();
		if(nickname.length > 15 || nickname.length < 2){
			validate('Username max character limit is 15 and min limit is 2.','left_reg_name');
		}
		else if (nickname.match(letters)) {
			//$("#preloader").fadeIn("fast");
			$.ajax({
				url: '<?php echo site_url("Home/reg_nickname_instant");?>',
				type: "POST",
				data: {
					nickname: nickname
				},
				success: function (data) {
					var obj = JSON.parse(data);
					if (obj.status == true) {
						register_instant = nickname;
						$("#signupmodal2 span").eq(1).text(nickname);
						$("#signupmodal2-1").css("left", (45.5 - 3.5 * (nickname.length - 1) / 13) + "%");
						//$("#signupmodal1").modal("toggle");
						$("#signupmodal2").modal("toggle"); 
						$("#signup_modal2_name").text(nickname+"!");
					} else {
						validate('Name is already existed.','left_reg_name','left_reg_name');
						$("#preloader").fadeOut("fast");
					}
				},
				error: function (error) {
					message('Internal server error occurred. Please try again later.','reft_reg_name');
				}
			});
		} else {
			validate('You can use alphanumerics only.','reft_reg_name');
		}
	});
	
	$(".adv_login_btn").click(function() {
		var letters = /^[0-9a-zA-Z]+$/;
		var nickname = $("#adv_nickname").val();
		if(nickname.length > 15 || nickname.length < 2){
			validate('Username max character limit is 15 and min limit is 2.','adv_nickname1');
		}
		else if (nickname.match(letters)) {
			$.ajax({
				url: '<?php echo site_url("Home/reg_nickname_instant");?>',
				type: "POST",
				data: {
					nickname: nickname
				},
				success: function (data) {
					var obj = JSON.parse(data);
					
					if (obj.status == true) {
						register_instant = nickname;
						$("#signupmodal2 span").eq(1).text(nickname);
						$("#signupmodal2-1").css("left", (45.5 - 3.5 * (nickname.length - 1) / 13) + "%");
						$("#signupmodal2").modal("toggle"); 
						$("#signup_modal2_name").text(nickname+"!");
					} else {
						validate('error', 'Name is already existed.','adv_nickname');
					}
				},
				error: function (error) {
					message('error', 'Internal server error occurred. Please try again later.');
				}
			});
		} else {
			validate('You can use alphanumerics only.','adv_nickname');
		}
	});

	$("#button-to-second").click(function() {
		var letters = /^[0-9a-zA-Z]+$/;

        var nickname = $("#signup_nickname").val();
        var email = $("#signup_email").val();
        var password = $('#signup_password').val();
		
		var contain_number = hasLetterNumber(password);
		var contain_letter = hasLetterSpecial(password);
		var is_error = 0;
		$('.signup_valid_username').css('display','none');
		$('.signup_valid_username1').css('display','none');
		$('.signup_valid_exist').css('display','none');
		
		$('.signup_valid_password').css('display','none');
		$('.signup_wrong_password').css('display','none');
		$('.signup_valid_password_strong').css('display','none');
		
		$('.signup_valid_email').css('display','none');
		$('.signup_valid_emailexist').css('display','none');
		
		$('.signup_input').css('border','2px solid #F8BF60');
		$('.signup_input1').css('border','2px solid #F8BF60');
		
		if(nickname.length > 15 || nickname.length < 2){
			$('#signup_nickname').css('border', '2px solid #bd2929');
			$('.signup_valid_username').css('display','block');
			is_error = 1;
		} else if(!nickname.match(letters)){
			is_error = 1;
			$('#signup_nickname').css('border', '2px solid #bd2929');
			$('.signup_valid_username1').css('display','block');
		}
		
		if(email.length > 0){
			var fileter = /^[\w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
			if(!fileter.test(email)){
				$('#signup_email').css('border', '2px solid #bd2929');
				$('.signup_valid_email').css('display','block');
				is_error = 1;
			}
		}
		
		if(password.length < 8){
			$('#signup_password').css('border', '2px solid #bd2929');
			$('.signup_valid_password').css('display','block');
			is_error = 1;
		}
		else if(!contain_number && !contain_letter){
			$('#signup_password').css('border', '2px solid #bd2929');
			$('.signup_valid_password_strong').css('display','block');
			is_error = 1;
		}
		if(is_error == 0) {
			$("#preloader").fadeIn("fast");
			$.ajax({
				url: '<?php echo site_url("Home/reg_nickname");?>',
				type: "POST",
				data: {
					email:email,
					password: password,
					nickname: nickname
				},
				success: function (data) {
					var obj = JSON.parse(data);
					//$("#preloader").fadeOut("fast");
					if (obj.success == "true") {
						sessionStorage.setItem("nickname", obj.info.nickname);
						sessionStorage.setItem("email", email);
						sessionStorage.setItem("status",obj.info.user_id);
						sessionStorage.setItem("free_spins",obj.info.freespins);
						sessionStorage.setItem("vgo_trade_url","");
						$("#signupmodal1").modal("toggle");
						$("#signupmodal3").modal("toggle");
						$("#signupmodal3_name").text(nickname);
					} else if(obj.success == "email_exist"){
						$('#signup_email').css('border', '2px solid #bd2929');
						$('.signup_valid_emailexist').css('display','block');
					} else {
						$("#preloader").fadeOut("fast");
						$('#signup_nickname').css('border', '2px solid #bd2929');
						$('.signup_valid_exist').css('display','block');
					}
				},
				error: function (error) {
					$("#preloader").fadeOut("fast");
					message('error', 'Internal server error occurred. Please try again later.');
					console.log(error);
				}
			});
		}
		
	});
	
	$("#button-to-login-1").click(function() {
		$("#signupmodal1").modal("toggle");
		$("#loginmodal").modal("toggle");
	});
	
	$("#logout-cancel").click(function() {
		$("#logoutmodal").modal("toggle");
	});
	
	$("#logout-accept").click(function() {
		$("#logoutmodal").modal("toggle");
		sessionStorage.clear();
		location.href = '<?php echo site_url("Home/logout");?>';
	});
	
	
	
	$("#button-to-login-2").click(function() {
		$("#signupmodal2").modal("toggle");
		$("#loginmodal").modal("toggle");
	});

	$("#button-to-third").click(function() {
		var formData = new FormData();
		
		var letters = /^[0-9a-zA-Z]+$/;
		var emailregex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		var email = $("#signupmodal2 input[type=email]").val();
		var password = $("#signup_password2_1").val();
		var re_password = $('#signup_password2_2').val();
		
		//sessionStorage.setItem("password", calcMD5(password));
		var contain_number = hasLetterNumber(password);
		var contain_letter = hasLetterSpecial(password);
		
		if(!contain_number && !contain_letter){
			validate('Password must contain at leaset 6 characters, including letters and 1 digit or special character','signup_password2_1');
		}else if(password != re_password){
			validate('error', "Those passwords didn't match. Try again!",'signup_password2_1');
		}
		else{
			
			$("#preloader").fadeIn("fast");
			$.ajax({
				url: '<?php echo site_url("Home/reg_email");?>',
				type: "POST",
				data: {
					email:email,
					password: password,
					nickname: register_instant
				},
				success: function (data) {
					var obj = JSON.parse(data);
					//sessionStorage.setItem("password", calcMD5(password));
					if (obj.success == "true") {	
						sessionStorage.setItem("email", email);
						sessionStorage.setItem("status",obj.info.user_id);
						sessionStorage.setItem("free_spins",obj.info.freespins);
						sessionStorage.setItem("vgo_trade_url","");
						sessionStorage.setItem("nickname", obj.info.nickname);
						$("#signupmodal2").modal("toggle");
						$("#signupmodal3").modal("toggle");
						$('#signupmodal3_name').text(register_instant);
					}
					else {
						$("#preloader").fadeOut("fast");
						validate('error', 'This email is already taken. Please choose another.','signup2_email1');
					}
				},
				error: function (error) {
					$("#preloader").fadeOut("fast");
					message('error', 'Internal server error occurred. Please try again later.');
					console.log(error);
				}
			});
		} 
	});

	$("#button-to-login-2").click(function() {
		$("#signupmodal2").modal("toggle");
		$("#loginmodal").modal("toggle");
	});

	$("#signupmodal3").on("show.bs.modal", function () {
		$("#signupmodal3").modal("toggle");
	});

	$("#button-to-final").click(function() {
		$("#signupmodal3").modal("toggle");
		setTimeout(function() { location.href = root; }, 300);
	});

	$("#button-login").click(function() {
		var username = $("#login_name").val();
		var password = $("#password").val();
		var is_error = 0;
		$('.login_valid_username').css('display','none');
		$('.login_valid_passwd').css('display','none');
		$('.login_valid_wrong_passwd').css('display','none');
		$('.login_valid_nouser').css('display','none');
		$('.login_valid_length').css('display','none');
		$('#login_name').css("border", "2px solid #f8bf60");
		if(username == ''){
			$('#login_name').css("border", "2px solid #bd2929");
			$('.login_valid_username').css('display','block');
			is_error = 1;
		}
		else if(username.length < 2 || username.length > 15){
			$('#login_name').css("border", "2px solid #bd2929");
			$('.login_valid_length').css('display','block');
			is_error = 1;
		}
		if(password == ''){
			is_error = 1;
			$('#password').css("border", "2px solid #bd2929");
			$('.login_valid_passwd').css('display','block');
		}
		if(is_error == 0){
			$.ajax({
				url: '<?php echo site_url("Home/login");?>',
				type: "POST",
				data: {
					username: username,
					password: password
				},
				success: function (data) {
					var obj = JSON.parse(data);
					if (obj.success == "true") {
						sessionStorage.setItem("nickname", obj.data.nickname);
						sessionStorage.setItem("status", obj.data.user_id);
						sessionStorage.setItem("free_spins",obj.data.freespins);
						sessionStorage.setItem("vgo_trade_url",obj.data.trade_url);
						sessionStorage.setItem("bonus",obj.data.bonus);
						sessionStorage.setItem("email",obj.data.email);
						setTimeout(function() { location.href = root; }, 500);
					} else if(obj.success == "wrong_pwd"){
						$('#password').css("border", "2px solid #bd2929");
						$('.login_valid_wrong_passwd').css('display','block');
					} else{
						$('#login_name').css("border", "2px solid #bd2929");
						$('.login_valid_nouser').css('display','block');
					}
				},
				error: function (error) {
					message('error', 'Internal server error occurred. Please try again later.');
					console.log(error);
				}
			});
		}else{
			
		}
		event.preventDefault();
	});
	
	
	$("#security_password").click(function() {
		var cur_password = $('#old_password').val();
        var password = $('#change_new_pwd').val();
		
		var contain_number = hasLetterNumber(password);
		var contain_letter = hasLetterSpecial(password);
		var is_error = 0;
		
		$('.security_old_password').css('display','none');
		$('.signup_valid_password').css('display','none');
		$('.signup_valid_password_strong').css('display','none');
		
		$('#old_password').css('border','2px solid #F8BF60');
		$('#change_new_pwd').css('border','2px solid #F8BF60');
		
		if(password.length < 8){
			$('#change_new_pwd').css('border', '2px solid #bd2929');
			$('.signup_valid_password').css('display','block');
			is_error = 1;
		}
		else if(!contain_number && !contain_letter){
			$('#change_new_pwd').css('border', '2px solid #bd2929');
			$('.signup_valid_password_strong').css('display','block');
			is_error = 1;
		}
		if(is_error == 0) {
			$("#preloader").fadeIn("fast");
			$.ajax({
				url: '<?php echo site_url("Home/change_password");?>',
				type: "POST",
				data: {
					user_id:sessionStorage.getItem('status'),
					password: password,
					old_password: cur_password
				},
				success: function (data) {
					var obj = JSON.parse(data);
					$("#preloader").fadeOut("fast");
					if (obj.success == "not_same") {
						$('#old_password').css('border', '2px solid #bd2929');
						$('.security_old_password').css('display','block');
					}else {
						$('.security_old_password').css('display','none');
						$('.signup_valid_password').css('display','none');
						$('.signup_valid_password_strong').css('display','none');
						
						$('#old_password').css('border','2px solid #F8BF60');
						$('#change_new_pwd').css('border','2px solid #F8BF60');
						$('#old_password').val('');
						$('#change_new_pwd').val('');
						message('success', 'Successfully set!');
					}
				},
				error: function (error) {
					$("#preloader").fadeOut("fast");
					message('error', 'Internal server error occurred. Please try again later.');
					console.log(error);
				}
			});
		}
		
	});

	$("#button-to-signup").click(function() {
		$("#loginmodal").modal("toggle");
		$("#signupmodal1").modal("toggle");
	});

	$("#button-forgot").click(function() {
		$("#loginmodal").modal("toggle");
	});

	$("#chatbox > textarea").on("keypress", function(e) {
		var buffer = $(this).val();
		if (e.which == 13 && buffer.length > 0) {
			e.preventDefault();
			if(sessionStorage.getItem("status") == null){
				$("#signupmodal1").modal("toggle");
			} else{
				var brag = parseInt(sessionStorage.getItem("brag"));
				if (brag == 0)
					newChatText(sessionStorage.getItem("type"), buffer);
				else {
					var cash = parseFloat(sessionStorage.getItem("cash"));
					cash -= parseInt(sessionStorage.getItem("brag"));
					if(cash > 0){
						newChatText(7 + brag / 200, buffer);
						sessionStorage.setItem("cash",cash.toFixed(2));
						if(cash >= 1000000000000)
							$(".totalgems").html(parseFloat(cash/1000000000000).toFixed(2)+"T");
						else if(cash >= 1000000000)
							$(".totalgems").html(parseFloat(cash/1000000000).toFixed(2)+"B");
						else if(cash >= 100000000)
							$(".totalgems").html(parseFloat(cash/1000000).toFixed(2)+"M");
						else{
								$(".totalgems").html(formatNumber(cash.toFixed(2)));
						}
						
						var formData = new FormData();
						formData.append("cash", cash);
						formData.append("user_id",sessionStorage.getItem("status"));
						var queryString = new URLSearchParams(formData).toString();
						sessionStorage.setItem("brag", "0");
						$(".chattext").css("color", 'white');
						$.ajax({
							url: '<?php echo site_url("Home/update");?>',
							data: queryString,
							success: function (data) {
							},
							error: function (error) {
							}
						});	
					}else{
						message("error", "You don't have enough cash!");
						return;
					}
				}
			}
		}
	});

	$("#chatsubmit").click(function() {
		var buffer = $("#chatbox > textarea").val();
		if(sessionStorage.getItem("status") == null){
			$("#signupmodal1").modal("toggle");
		} else{
			if (buffer.length > 0) {
				var brag = parseInt(sessionStorage.getItem("brag"));
				if (brag == 0)
					newChatText(sessionStorage.getItem("type"), buffer);
				else {
					var cash = parseInt(sessionStorage.getItem("cash"));
					cash -= parseInt(sessionStorage.getItem("brag"));
					if(cash > 0){
						newChatText(7 + brag / 200, buffer);
						sessionStorage.setItem("cash",cash);
						if(cash >= 1000000000000)
							$(".totalgems").html(parseFloat(cash/1000000000000).toFixed(2)+"T");
						else if(cash >= 1000000000)
							$(".totalgems").html(parseFloat(cash/1000000000).toFixed(2)+"B");
						else if(cash >= 100000000)
							$(".totalgems").html(parseFloat(cash/1000000).toFixed(2)+"M");
						else{
							//var width = window.innerWidth;
							//if(width > 1400)
								$(".totalgems").html(formatNumber(cash.toFixed(2)));
							//else
							//	$(".totalgems").html(parseFloat(cash/1000).toFixed(2)+"K");
						}
						var formData = new FormData();
						formData.append("cash", cash);
						formData.append("user_id",sessionStorage.getItem("status"));
						var queryString = new URLSearchParams(formData).toString();
						sessionStorage.setItem("brag", "0");
						$(".chattext").css("color", 'white');
						$.ajax({
							url: '<?php echo site_url("Home/update");?>',
							data: queryString,
							success: function (data) {
							},
							error: function (error) {
							}
						});	
					}else{
						message("error", "You don't have enough cash!");
						return;
					}
				}
			}	
		}
		
	});

	$("li.brags").click(function() {
		var me = $(this).children().get(1);
		var cash = parseInt(sessionStorage.getItem("cash"));
		if (cash < parseInt(me.value)) {
			message("error", "You don't have enough cash!");
			return;
		}
		$(".chattext").css("color", $(this).children().eq(0).css("color"));
		sessionStorage.setItem("brag", me.value);
	});

	function newChatText(type, text) {
		$("#chatbox > textarea").val("");
		socket.emit('new chat', {level: sessionStorage.getItem("level"), user: sessionStorage.getItem("nickname"), type: type, text: text});
	}

	function validate(errormsg,validate_target){
	    var indicator = '#' + validate_target;
	    $(indicator).after('<div class = "validator">'+errormsg+'</div>');
	    $(indicator).next().css('display','block');
        $(indicator).next().animate({opacity:1},'slow');
        function disappear(){
            $(indicator).next().remove();
        }
        setTimeout(disappear,3000);
    }

	function message(type, text) {
		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": true,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		if(type == 'error')
			toastr.error(text);
		else if(type == 'warning'){
            toastr.warning(text);
        }

		else if(type == 'success')
			toastr.success(text);
	}
	
	function logout(){
		$("#accountmodal").modal("toggle");	
		$("#logoutmodal").modal("toggle");
	}
	
	function wager_count(type){
		if(sessionStorage.getItem("status") != null){
			if((is_freespin_round == 0 && game_type == 1) || (is_solo_freespin_round == 0 && game_type == 2)){
				if(type == 0){
					wager_index = wager_index-1;
					if(wager_index < 0)
						wager_index = 0;
				} else{
					wager_index = wager_index+1;
					if(wager_index >22 )
						wager_index = 22;
				}
				$(".curwager").html(formatNumber(wager_list[wager_index]));	
			}
		} else{
			$("#signupmodal1").modal("toggle");
		}
	}
	
	function wager_count_mobile(){
		$('.autoplay_spin_mobile').css('display','none');
		if(sessionStorage.getItem("status") != null){
			
			if($('.max_bet_btn').css('color') == 'rgb(0, 0, 0)' && is_freespin_round == 0){
				if($('.wager_mobile').css('display') != 'none'){
					$('.wager_mobile').css('display','none');
				}
				else{
					$('.wager_mobile').css('display','block');
				}
			}else if(is_freespin_round == 0){
				if($('.wager_mobile').css('display') != 'none')
					$('.wager_mobile').css('display','none');
				else
					$('.wager_mobile').css('display','block');
			}
		} else{
			$("#signupmodal1").modal("toggle");
		}
	}
	
	function sel_wager(wager_index){
		if(sessionStorage.getItem("status") != null){
			if((is_freespin_round == 0 && game_type == 1) || (is_solo_freespin_round == 0 && game_type == 2)){
				$(".curwager").html(formatNumber(wager_list[wager_index]));	
			}
		} else{
			$("#signupmodal1").modal("toggle");
		}
	}
	
	function spin(type){
		if(sessionStorage.getItem("status") == null){
			//$("#signupmodal1").modal("toggle");
		}
		else{
			if((is_freespin_round == 1 && sessionStorage.getItem('is_freespin_round') == 1 && game_type == 1) ||
			(is_freespin_round == 0 && ($('.play_btn').css('color') == 'rgb(0, 0, 0)' || type == 'auto' || ($('.max_bet_btn').css('color') == 'rgb(0, 0, 0)' && type == 'max')) && game_type == 1) || 
			(game_type == 2 && is_solo_freespin_round == 0) ||
			(game_type == 2 && is_solo_freespin_round == 1 && is_solo_attend == false)){
				if(spin_available == false){
				}
				else{
					if(game_type == 1)
						is_attend_round = true;
					if(game_type == 2)
						is_solo_attend = true;
					var wager = 0;
					if(sessionStorage.getItem('wager_index') == -1)
						wager = parseInt(remove_character(wager_list[wager_index]));
					else
						wager = parseInt(remove_character(wager_list[sessionStorage.getItem('wager_index')]));
					var cash = parseFloat(sessionStorage.getItem("cash"));
					var cur_attend_amount = 0;
					if(type == 'max' || cash < wager){
						attend_amount += cash;
						cur_attend_amount = cash;
					}
					else{
						attend_amount += wager;
						cur_attend_amount = wager;
					}
					
					if(cash == 0){
						message("error", "No balance");
						return;
					} else if(type == 'max' && cash == 0){
						message("error", "No balance");
						return;
					} else{
						
						if((is_freespin_round == 0 && game_type == 1) || (game_type == 2 && is_solo_freespin_round == 0)){
							cash -= cur_attend_amount;
							sessionStorage.setItem("cash",cash);			
							if(cash >= 1000000000000)
								$(".totalgems").html(parseFloat(cash/1000000000000).toFixed(2)+"T");
							else if(cash >= 1000000000)
								$(".totalgems").html(parseFloat(cash/1000000000).toFixed(2)+"B");
							else if(cash >= 100000000)
								$(".totalgems").html(parseFloat(cash/1000000).toFixed(2)+"M");
							else{
								//var width = window.innerWidth;
								//if(width > 1400)
									$(".totalgems").html(formatNumber(cash.toFixed(2)));
								//else
								//	$(".totalgems").html(parseFloat(cash/1000).toFixed(2)+"K");
							}
						}
					
						var exp_val = cur_attend_amount * 0.8;
						
						socket.emit('change exp',{cash: cash, user_id: sessionStorage.getItem("status"), experience: exp_val});
						spin_user(cur_attend_amount);
						
						if((is_freespin_round == 0 || is_solo_freespin_round == 0) && cash < wager && type == 'auto'){
							$('.play_btn').css('color','black');
							$('.mobile_play_btn').css('color','black');
							$('.max_bet_btn').css('color','black');
							$('.autoplay_stop_btn').css('display','none');
							$('.spin_btn').css('display','block');
							$('.play_btn span').html("PLAY");
							is_autoplay = 0;
						}
					}
				}
			}
		}
	}
	
	function spin_user(wager) {
		if(game_type == 1)
			socket.emit('spin user', {level: sessionStorage.getItem("level"), user: sessionStorage.getItem("nickname"), userid:sessionStorage.getItem("status"), wager:wager, game_type : game_type, is_first : 0});
		if(game_type == 2){
			var is_first = 0;
			if(solo_wager == 0 || is_solo_freespin_round == 1){
				$('#bodypart3 .player_list').empty();
				$('#bodypart2 .player_win').text('---');
				is_first = 0;
			}
			else
				is_first = 1;
			solo_wager += parseInt(wager);
			socket.emit('spin user', {level: sessionStorage.getItem("level"), user: sessionStorage.getItem("nickname"), wager:wager, game_type : game_type, is_first: is_first});
			$('#bodypart2 .player_count').text(1);
			if(solo_wager >= 1000000000000){
				$('#bodypart2 .player_wager').html(parseFloat(solo_wager/1000000000000).toFixed(2)+"T");
			}
			else if(solo_wager >= 1000000000){
				$('#bodypart2 .player_wager').html(parseFloat(solo_wager/1000000000).toFixed(2)+"B");
			}
			else if(solo_wager >= 1000000)
				$('#bodypart2 .player_wager').html(parseFloat(solo_wager/1000000).toFixed(2)+"M");
			else
				$('#bodypart2 .player_wager').text(formatNumber(solo_wager));
			
			if(is_first == 0){
				solo_seed = curDateTime() + " " + sessionStorage.getItem('status');
				socket.emit('play solo start',{client_seed : solo_seed, user_id : sessionStorage.getItem("status"), is_solo_freespin_round:is_solo_freespin_round, pre_round_result:pre_round_result});
			}
		}
	}
	
	function change_exp(data){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		var lvl = parseInt(data.level);
		sessionStorage.setItem("level", lvl);
		var exp = parseInt(data.experience), cur, nxt, per;
		$('#reward_spin_cnt').text(data.freespins);
		$('#account_spins_cnt').text(data.freespins);
		sessionStorage.setItem("free_spins",data.freespins);
		sessionStorage.setItem("cash",data.cash);
		$('.account_modal_userid').text('USER ID:'+data.user_id);
		$('#accountmodal #user_name').text(data.nickname);

		$("#levelpart1").removeClass();
		$('#reward_earning_star').removeClass();
		if(width > 1023)
			$("#levelpart1").html("Level " + lvl + " <i class=\"fas fa-star\"></i>");
		else
			$("#levelpart1").html("<i class=\"fas fa-star\"></i> " + lvl);
		if (lvl < 50) {
			cur = 100 * lvl;
			nxt = 100 * (lvl + 1);
			$("#levelpart1").addClass("color-inactive");
			$('#accountmodal .user_level').addClass("color-inactive");
			$('#left_menu_level').removeClass();
			$('#left_menu_level').addClass("color-inactive");
			$('#reward_earning_star').addClass("fas fa-star color-inactive");
			$('.level-icon').attr("src","<?php echo base_url();?>assets/images/cylinder-loaded-inactive.png");
			sessionStorage.setItem("type", "1");
		} else if (lvl < 60) {
			cur = 5000 + 500 * (lvl - 50);
			nxt = 5000 + 500 * (lvl - 49);
			$("#levelpart1").addClass("color-red-orange");
			$('#accountmodal .user_level').addClass("color-red-orange");
			$('#left_menu_level').removeClass();
			$('#left_menu_level').addClass("color-red-orange");
			$('#reward_earning_star').addClass("fas fa-star color-red-orange");
			$('.level-icon').attr("src","<?php echo base_url();?>assets/images/cylinder-loaded-red-orange.png");
			sessionStorage.setItem("type", "2");
		} else if (lvl < 85) {
			cur = 10000 + 1000 * (lvl - 60);
			nxt = 10000 + 1000 * (lvl - 59);
			$("#levelpart1").addClass("color-dark-blue");
			$('#accountmodal .user_level').addClass("color-dark-blue");
			$('#left_menu_level').removeClass();
			$('#left_menu_level').addClass("color-dark-blue");
			$('#reward_earning_star').addClass("fas fa-star color-dark-blue");
			$('.level-icon').attr("src","<?php echo base_url();?>assets/images/cylinder-loaded-dark-blue.png");
			sessionStorage.setItem("type", "3");
		} else if (lvl < 100) {
			cur = 35000 + 5000 * (lvl - 85);
			nxt = 35000 + 5000 * (lvl - 84);
			$("#levelpart1").addClass("color-dark-pink");
			$('#accountmodal .user_level').addClass("color-dark-pink");
			$('#left_menu_level').removeClass();
			$('#left_menu_level').addClass("color-dark-pink");
			$('#reward_earning_star').addClass("fas fa-star color-dark-pink");
			$('.level-icon').attr("src","<?php echo base_url();?>assets/images/cylinder-loaded-dark-pink.png");
			sessionStorage.setItem("type", "4");
		} else if (lvl < 125) {
			cur = 110000 + 10000 * (lvl - 100);
			nxt = 110000 + 10000 * (lvl - 99);
			$("#levelpart1").addClass("color-red");
			$('#accountmodal .user_level').addClass("color-red");
			$('#left_menu_level').removeClass();
			$('#left_menu_level').addClass("color-red");
			$('#reward_earning_star').addClass("fas fa-star color-red");
			$('.level-icon').attr("src","<?php echo base_url();?>assets/images/cylinder-loaded-red.png");
			sessionStorage.setItem("type", "5");
		} else {
			cur = 360000 + 50000 * (lvl - 125);
			nxt = 360000 + 50000 * (lvl - 124);
			$("#levelpart1").addClass("color-orange");
			$('#accountmodal .user_level').addClass("color-orange");
			$('#left_menu_level').removeClass();
			$('#left_menu_level').addClass("color-orange");
			$('#reward_earning_star').addClass("fas fa-star color-orange");
			$('.level-icon').attr("src","<?php echo base_url();?>assets/images/Gold_Cyl.svg");
			sessionStorage.setItem("type", "6");
		}
		$('#accountmodal .user_level').empty();
		$('#left_menu_level').empty();
		$('#accountmodal .user_level').append("<i class='fas fa-star mr-1'></i>"+lvl);
		$('#left_menu_level').append("<i class='fas fa-star mr-1'></i>"+lvl);

		$("#levelpart2").removeClass();
		if(width > 1023)
			$("#levelpart2").html("<i class=\"fas fa-star\"></i> Level " + (lvl + 1));
		else
			$("#levelpart2").html("<i class=\"fas fa-star\"></i> " + (lvl + 1));
		var color = "color-inactive";
		if (lvl+1 < 49) {
			$("#levelpart2").addClass("color-inactive");
			color = "#747883";
		} else if (lvl+1 < 59) {
			$("#levelpart2").addClass("color-red-orange");
			color = "#cd5334";
		} else if (lvl+1 < 84) {
			$("#levelpart2").addClass("color-dark-blue");
			color = "#2989ad";
		} else if (lvl+1 < 99) {
			$("#levelpart2").addClass("color-dark-pink");
			color = "#af60f8";
		} else if (lvl+1 < 124) {
			$("#levelpart2").addClass("color-red");
			color = "red";
		} else {
			$("#levelpart2").addClass("color-orange");
			color = "#f8bf60";
		}

		per = Math.floor(100.0 * (exp - cur) / (nxt - cur));
		if(lvl >= 100 && width <= 600){
			$("#expcur").html("<span>Current XP:<br>" + formatNumber(cur)+"</span>");
			$("#exptot").html("<span>"+formatNumber(exp) + " XP/<br>" + formatNumber(nxt) + " XP(" + per + "%)</span>");	
			$("#expnxt").html("<span>Next Reward:<br>" + formatNumber(nxt)+"</span>");
			if(width> 450){
				$('#expcur, #exptot, #expnxt').css('top','0px');	
			}else{
				$('#expcur, #exptot, #expnxt').css('top','3px');
				$('#expcur, #exptot, #expnxt').css('font-size','9px');
			}
			$('#expcur, #exptot, #expnxt').css('text-align','center');
			$('#expcur, #exptot, #expnxt').css('line-height','1.1');	
		} else{
			$("#expcur").html("<span>Current XP: " + formatNumber(cur)+"</span>");
			if(width > 500)
				$("#exptot").html("<span>Total XP: " + formatNumber(exp) + " / " + formatNumber(nxt) + " (" + per + "%)"+"</span>");
			else
				$("#exptot").html("<span>"+formatNumber(exp) + " XP / " + formatNumber(nxt) + " XP(" + per + "%)</span>");	
			$("#expnxt").html("<span>Next Reward: " + formatNumber(nxt)+"</span>");
		}
		
		
		//$("#expbar-fit").css("width", per + "%");
		$("#expbar-fit").animate({width: per+"%"},2000);
		$('#level_circel_prog').circleProgress({
		    value: parseFloat(per/100),
		    emptyFill: "#1c2127",
		    size: 800,
		    thickness: 39,
		    animationStartValue: parseFloat(percent_val/100),
		    animation: {
		    	duration: 2000
			},
		    lineCap: "round",
		    startAngle: Math.PI/2,
		    fill: {color: color}
		});
    
		$('#level_circel_prog canvas').css('width','100%');
		$('#level_circel_prog canvas').css('height','100%');
		
  		percent_val = per;
		//$("#expbar-left").css("width", (100 - per) + "%");

		if(parseFloat(data.cash) >= 1000000000000)
			$(".totalgems").html(parseFloat(data.cash/1000000000000).toFixed(2)+"T");
		else if(parseFloat(data.cash) >= 1000000000)
			$(".totalgems").html(parseFloat(data.cash/1000000000).toFixed(2)+"B");
		else if(parseFloat(data.cash) >= 100000000)
			$(".totalgems").html(parseFloat(data.cash/1000000).toFixed(2)+"M");
		else{
			//var width = window.innerWidth;
			//if(width > 1400)
				$(".totalgems").html(formatNumber(parseFloat(data.cash).toFixed(2)));
			//else
			//	$(".totalgems").html(parseFloat(parseFloat(data.cash)/1000).toFixed(2)+"K");
		}
		//$(".bestwager").html(formatNumber(data.bestwin) + " <i class=\"fas fa-gem color-orange\"></i>");
		var width_bodypart = parseInt($("#mainboard").css("width"));
		$("#exppart").css("width", (width_bodypart - $(".levelpart1-section").width() - $(".levelpart2-section").width() - 43) + "px");
	}
	
	function resetRoll(){
		var wheel_width = $('.wheel-container').width();
    	//var background_position = current_pos - (Math.ceil((2520 - wheel_width) / 2));
    	var background_position = current_pos;
    	$('div.case.new').css('background-position', background_position + 'px');
	}
	function setPos(pos) {
		current_pos = pos;
		resetRoll();
	}
	
	function copy_bitcoin_address(){
		var copyText = document.getElementById("my_bitcoin_address");
	  	copyText.select();
	  	document.execCommand("copy");
	  	$('#my_bitcoin_address').blur();
	}
	
	function deposit_menu(type){
		if(type == 1){
			$('#deposit_vgo').addClass("color-inactive");
			$('#deposit_crypto').removeClass("color-inactive");
		}else{
			if(sessionStorage.getItem("vgo_trade_url") == "" || sessionStorage.getItem("vgo_trade_url") == null || sessionStorage.getItem("vgo_trade_url") == "null"){
				$('#deposit_vgo').removeClass("color-inactive");
				$('#deposit_crypto').addClass("color-inactive");
				$('#crypto_deposit').css('display','none');
				$('#crypto_deposit').removeClass("active show");
				$('#tradeUrl').css('display','block');
				$('#tradeUrl').addClass("active show");
			}else{
				$('#deposit_vgo').removeClass("color-inactive");
				$('#deposit_crypto').addClass("color-inactive");
				$('#crypto_deposit').css('display','none');
				$('#crypto_deposit').removeClass("active show");
				$('#vgo_deposit').css('display','block');
				$('#vgo_deposit').addClass("active show");
				$('#preloader').fadeIn("fast");
				$.ajax({
					url: '<?php echo site_url("Home/get_vgo_items");?>',
					data: {trade_url:sessionStorage.getItem("vgo_trade_url"),type:0},
					success: function (data) {
						var obj = JSON.parse(data);
						var html = "";
						if(obj.status == 1){
							for(var i = 0;i<obj.item.length;i++){
								html += '<div style="background-color:#282a30;" class="vgo_item_each_section vgo_item_'+obj.item[i].id+'" onclick="vgo_item_sel('+obj.item[i].id+')">';
								html += '	<div class="vgo_eye_section mr-1"><i class="fa fa-eye color-inactive"></i></div>';
								html += '	<div>';
								var item_name = obj.item[i].name;
								temp = item_name.split('(');
								temp[1] = temp[1].replace(")","");
								html += '		<p class="color-inactive margin-bottom-0" style="font-size:11px;">'+temp[0]+'</p>';
								html += '		<p class="color-inactive" style="font-size:11px;">'+temp[1]+'</p>';
								html += '	</div>';
								html += '	<div>';
								html += '		<img src="'+obj.item[i].image['300px']+'" style="width:80px;margin-top:-20px;"/>';
								html += '	</div>';
								html += '	<div >';
								var price = obj.item[i].suggested_price/Math.pow(10,obj.item[i].suggested_price_floor)*1000;
								html += '		<label class="color-active" style="font-size:12px;">'+formatNumber(price)+' <i class="fas fa-gem color-orange"></i></label>';
								html += '	</div>';
								html += '</div>';
							}
							$('#vgo_item_section').html(html);
						} else if(obj.status == 106){
							alert(obj.message);
						}
						//$('#preloader').fadeOut("fast");
					},
					error: function (error) {
						$('#preloader').fadeOut("fast");
					}
				});		
			}
		}
	}
	
	function vgo_item_sel(item_id){
		if($('.vgo_item_'+item_id).css('background-color') == 'rgb(40, 42, 48)'){
			$('.vgo_item_'+item_id).css('background-color','#e4b05a');
			$('.vgo_item_'+item_id+' .vgo_eye_section svg').css('color','black');
			$('.vgo_item_'+item_id+' p').css('color','black');
			$('.vgo_item_'+item_id+' label').css('color','black');
			$('.vgo_item_'+item_id+' label svg').css('color','black');
		}
		else{
			$('.vgo_item_'+item_id).css('background-color','#282a30');
			$('.vgo_item_'+item_id+' .vgo_eye_section svg').css('color','#747883');
			$('.vgo_item_'+item_id+' p').css('color','#747883');
			$('.vgo_item_'+item_id+' label').css('color','white');
			$('.vgo_item_'+item_id+' label svg').css('color','#F8BF60');
		}
	}
	
	function game_type_sel(type){
		if(sessionStorage.getItem("status") == null){
			$("#signupmodal1").modal("toggle");
		}else{
			if(type == 1){
				game_type = 1;
				$('.group_game').css('color','#F8BF60');
				$('.solo_game').css('color','white');
				$('.group_game_mobile').css('color','#F8BF60');
				$('.solo_game_mobile').css('color','white');
				change_orange('.play_btn');
				change_orange('.mobile_play_btn');
				animItem.stop(50);
				animItem_shoot.stop();
				$('.play_btn span').html("PLAY");
				is_solo_freespin_round = 0;
				is_autoplay = 0;
				is_cal_new_round_result = 0;
			}
			else{
				game_type = 2;
				is_autoplay = 0;
				spin_available = true;
				$('.group_game').css('color','white');
				$('.solo_game').css('color','#F8BF60');
				$('.group_game_mobile').css('color','white');
				$('.solo_game_mobile').css('color','#F8BF60');
				change_orange('.play_btn');
				change_orange('.max_bet_btn');
				$('.play_btn span').html("PLAY");
				animItem.stop();
				animItem_shoot.stop();
				$('.case1.new').html('');
				$('.case2.new').html('');
				$('#bodypart3 .player_list').empty();
				$('#bodypart2 .player_win').text('---');
				$('#bodypart2 .player_count').text(0);
				$('#bodypart2 .player_wager').text(0);
				if(is_freespin_round == 1){
					$('.left_mul.new').html('');
					$('.center_mul.new').html('');
					$('.right_mul.new').html('');
					$('#revolver').css('background','#282A30');
					$('.bodypart2-2').css('background','#282A30');
					$('.spin_round_trophy').css('display','none');
					$('.normal_round').css('display','block');
					$('.spin_round').css('display','none');
					$('.win_multi_title').html("WIN MULTIPLER");
					
					$('.bonus_wait_section').removeClass('bonus_wait_css');
					$('.bonus_wait').css('display','none');
					$('.bonus_wait1').css('display','none');
					$('.bonus_wait_section .title').css('display','block');
					$('.bonus_wait_section .cost').css('display','inline');
					$('.bonus_wait_section .wager_set').css('display','block');
					$('.bonus_wait_section .wager_amount').css('display','block');
					
					$('.bonus_spin_cnt_section').css('display','none');
					$('.max_bet_btn').css('display','block');
					$('.play_btn').css('display','block');
					$('.spin_btn').css('display','block');
				}
				socket.emit('solo round result list',{user_id : sessionStorage.getItem('status')});
			}	
		}		
	}
	
	function curDateTime() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1;
		var yyyy = today.getFullYear();
		if (dd < 10)
			dd = "0" + dd;
		if (mm < 10)
			mm = "0" + mm;
		var buffer = yyyy + "-" + mm + "-" + dd;
		var hh = today.getHours();
		var ss = today.getSeconds();
		mm = today.getMinutes();
		if (hh < 10)
			hh = "0" + hh;
		if (mm < 10)
			mm = "0" + mm;
		if (ss < 10)
			ss = "0" + ss;
		buffer += (" " + hh + ":" + mm + ":" + ss);
		return buffer;
	}
	
	function multipler_html(multipler_index,multipler_val){
		multipler_val = parseFloat(multipler_val);
		var win_multipler_index = multipler_index;
		new_round_win_multipler_html = new_round_win_multipler_html_temp = "";
		var width = window.innerWidth;
		var space_html = 'space_btw_mul';
		var real_val = two_html =  "";
		
		if(win_multipler_index == 1){
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive win_multi_center '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			real_val = '<span class="color-inactive win_multi_center">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			two_html = '<span class="color-inactive '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>' + '<span class="color-inactive '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 2){
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive win_multi_center '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			real_val = '<span class="color-inactive win_multi_center">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			two_html = '<span class="color-inactive '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>' + '<span class="color-inactive '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 3){
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive win_multi_center '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			real_val = '<span class="color-inactive win_multi_center">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			two_html = '<span class="color-inactive '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>' + '<span class="color-inactive '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 4){
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive win_multi_center '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			real_val = '<span class="color-inactive win_multi_center">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			two_html = '<span class="color-inactive '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>' + '<span class="color-inactive '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 5){
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive win_multi_center '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			new_round_win_multipler_html += '<span class="color-inactive '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			real_val = '<span class="color-inactive win_multi_center">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			two_html = '<span class="color-inactive '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>' + '<span class="color-inactive '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
		}
		for(var j=1;j<=6;j++){
			for(var i = 0;i<=4;i++){
				var precision = 100; // 2 decimals
				var randomnum = Math.floor(Math.random() * (100 * precision - 1 * precision) + 1 * precision) / (1*precision);
				new_round_win_multipler_html_temp += '<span class="color-inactive '+space_html+'">x'+randomnum+'</span>';
			}
		}
		$('.wheel-container-multi.indivi_mul').css('margin-left','0px');
		$('.case1.new').css('width','auto');
		$('.case1.new').html(new_round_win_multipler_html_temp);
		if(width > 930)
			space_bet_mul = width*0.43/100;
		else
			space_bet_mul = 4;
		multipler_width = $('.case1.new').width();
		$('.case1.new').css('width','auto');
		$('.case1.new').html(new_round_win_multipler_html);
		width = $('.case1.new').width();
		$('.case1.new').css('width',width);
		
		$('.case2').html(two_html);
		var two_width = $('.case2').width();
		$('.case2').html(real_val);
		var center_val_width = $('.case2').width();
		mul_margin_width = width/2 - two_width - center_val_width/2;
		mul_margin_width = mul_margin_width*2;
	}
	
	function spin_multipler_html(multipler_index,multipler_val, bonus_multipler){
		multipler_val = parseFloat(multipler_val);
		var win_multipler_index = multipler_index;
		spin_round_win_multipler_html = spin_round_win_multipler_html_temp = "";
		var width = window.innerWidth;
		var space_html = 'space_btw_mul';
		//if(width > 2000)
			//space_html = 'pr-2';
		var font_size_html = '';
		var real_val = two_html =  "";
		if(width > 1023){
			if(parseFloat(bonus_multipler) >= 20){
				font_size_html = "style='font-size:1vw;'";
				total_mul_font = '1vw';
			}
			else if(parseFloat(multipler_val) >= 20){
				font_size_html = "style='font-size:1.1vw;'";
				total_mul_font = '1.1vw';
			}
			else if(parseFloat(bonus_multipler) >= 2){
				font_size_html = "style='font-size:1.2vw;'";
				total_mul_font = '1.2vw';
			}
		}

		if(win_multipler_index == 1){
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 2){
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 3){
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 4){
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 5){
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(2*multipler_val).toFixed(2)+'</span>';
			spin_round_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(4*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(0.815*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(3*multipler_val).toFixed(2)+'</span>';
		}
		
		for(var j=1;j<=6;j++){
			for(var i = 0;i<=4;i++){
				var precision = 100; // 2 decimals
				var randomnum = Math.floor(Math.random() * (100 * precision - 1 * precision) + 1 * precision) / (1*precision);
				spin_round_win_multipler_html_temp += '<span '+font_size_html+' class="color-active '+space_html+'">x'+randomnum+'</span>';
			}
		}
		
		$('.left_mul.new').css('width','auto');
		$('.left_mul.new').html(spin_round_win_multipler_html_temp);
		spin_multipler_width = $('.left_mul.new').width();
		//$('.left_mul.new').css('width','auto');
		//$('.left_mul.new').html(spin_round_win_multipler_html);
		width = $('.spin_left_mul .spin_mul_title').width();
		$('.left_mul.new').css('width',width);
		
		$('.left_mul1.new').html(two_html);
		var two_width = $('.left_mul1.new').width();
		$('.left_mul1.new').css('width','auto');
		$('.left_mul1.new').html(real_val);
		var center_val_width = $('.left_mul1.new').width();
		$('.left_mul1.new').html(spin_round_win_multipler_html);
		var temp_width = $('.left_mul1.new').width();
		if(temp_width < width)
			spin_mul_margin_width = temp_width/2 - two_width - center_val_width/2;
		else
			spin_mul_margin_width = width/2 - two_width - center_val_width/2;
		spin_mul_margin_width = spin_mul_margin_width*2;
	}
	
	function bonus_multipler_html(multipler_index,multipler_val, win_mul){
		multipler_val = parseFloat(multipler_val);
		var win_multipler_index = multipler_index;
		bonus_win_multipler_html = bonus_win_multipler_html_temp = "";
		var width = window.innerWidth;
		var space_html = 'space_btw_mul';
		//if(width > 2000)
			//space_html = 'pr-2';
		var font_size_html = '';
		var real_val = two_html =  "";
		if(width > 1023){
			if(parseFloat(multipler_val) >= 20){
				font_size_html = "style='font-size:1vw;'";
			}
			else if(parseFloat(win_mul) >= 20){
				font_size_html = "style='font-size:1.1vw;'";
			}
			else if(parseFloat(multipler_val) >= 2){
				font_size_html = "style='font-size:1.2vw;'";
			}
		}
		if(win_multipler_index == 1){
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 2){
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 3){
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 4){
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>';
		} else if(win_multipler_index == 5){
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active win_multi_center '+space_html+'">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(8*multipler_val).toFixed(2)+'</span>';
			bonus_win_multipler_html += '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(20*multipler_val).toFixed(2)+'</span>';
			real_val = '<span '+font_size_html+' class="color-active win_multi_center">x'+parseFloat(50*multipler_val).toFixed(2)+'</span>';
			two_html = '<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(5*multipler_val).toFixed(2)+'</span>'+'<span '+font_size_html+' class="color-active '+space_html+'">x'+parseFloat(10*multipler_val).toFixed(2)+'</span>';
		}
		
		for(var j=1;j<=6;j++){
			for(var i = 0;i<=4;i++){
				var precision = 100; // 2 decimals
				var randomnum = Math.floor(Math.random() * (100 * precision - 1 * precision) + 1 * precision) / (1*precision);
				bonus_win_multipler_html_temp += '<span '+font_size_html+' class="color-active '+space_html+'">x'+randomnum+'</span>';
			}
		}
		
		$('.right_mul.new').css('width','auto');
		$('.right_mul.new').html(bonus_win_multipler_html_temp);
		spin_bonus_multipler_width = $('.right_mul.new').width();
		$('.right_mul.new').css('width','auto');
		$('.right_mul.new').html(bonus_win_multipler_html);
		width = $('.spin_right_mul .spin_mul_title').width();
		$('.right_mul.new').css('width',width);
		
		$('.right_mul1.new').html(two_html);
		var two_width = $('.right_mul1.new').width();
		$('.right_mul1.new').css('width','auto');
		$('.right_mul1.new').html(real_val);
		var center_val_width = $('.right_mul1.new').width();
		$('.right_mul1.new').html(bonus_win_multipler_html);
		var temp_width = $('.right_mul1.new').width();
		if(temp_width < width)
			spin_bonus_mul_margin_width = temp_width/2 - two_width - center_val_width/2;
		else
			spin_bonus_mul_margin_width = width/2 - two_width - center_val_width/2;
		spin_bonus_mul_margin_width = spin_bonus_mul_margin_width*2;
	}
	
	
	function spin_cnt_html(data){
		win_bonus_html = win_bonus_html_temp = "";
		var temp_html = temp_html1 = '';
		var win_bonus_index = data[5];
		var width = window.innerWidth;
		var space_html = 'pr-2';
		if(width <= 650 || (width > 1023 && width < 1600))
			space_html = 'pr-1';
		for(var i = win_bonus_index-2;i<=win_bonus_index+2;i++){
			if(i == win_bonus_index){
				var spin_cnt = 0;
				if(data[4] < 5)
					spin_cnt = parseInt(i+7);
				else if(data[4] < 7)
					spin_cnt = parseInt(parseInt(i+7)*1.5);
				else
					spin_cnt = parseInt(parseInt(i+7)*2);
				if(data[1] == 20)
					spin_cnt = spin_cnt * 2;
				win_bonus_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				win_bonus_html_temp += '<span class="color-orange '+space_html+' bonus_spin_roll_size win_bonus_center">'+spin_cnt+'</span>';
				temp_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
			}
			else if(i < 1){
				var spin_cnt = 0;
				if(data[4] < 5)
					spin_cnt = parseInt(i+5+7);
				else if(data[4] < 7)
					spin_cnt = parseInt(parseInt(i+5+7)*1.5);
				else
					spin_cnt = parseInt(parseInt(i+5+7)*2);
				if(data[1] == 20)
					spin_cnt = spin_cnt * 2;
				win_bonus_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				win_bonus_html_temp += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				if(i==win_bonus_index+2){
					temp_html1 += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}else{
					temp_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}	
			}
			else if(i > 5){
				var spin_cnt = 0;
				if(data[4] < 5)
					spin_cnt = parseInt(i-5+7);
				else if(data[4] < 7)
					spin_cnt = parseInt(parseInt(i-5+7)*1.5);
				else
					spin_cnt = parseInt(parseInt(i-5+7)*2);
				if(data[1] == 20)
					spin_cnt = spin_cnt * 2;
				win_bonus_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				win_bonus_html_temp += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				if(i==win_bonus_index+2){
					temp_html1 += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}else{
					temp_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}
			}
			else{
				var spin_cnt = 0;
				if(data[4] < 5)
					spin_cnt = parseInt(i+7);
				else if(data[4] < 7)
					spin_cnt = parseInt(parseInt(i+7)*1.5);
				else
					spin_cnt = parseInt(parseInt(i+7)*2);
				if(data[1] == 20)
					spin_cnt = spin_cnt * 2;
				win_bonus_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				win_bonus_html_temp += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				if(i==win_bonus_index+2){
					temp_html1 += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}else{
					temp_html += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}
			}
		}
		
		var new_round_win_multipler_html_temp1 = "";
		for(var j=1;j<=6;j++){
			for(var i = win_bonus_index-2;i<=win_bonus_index+2;i++){
				if(i == win_bonus_index){
					var spin_cnt = 0;
					if(data[4] < 5)
						spin_cnt = parseInt(i+7);
					else if(data[4] < 7)
						spin_cnt = parseInt(parseInt(i+7)*1.5);
					else
						spin_cnt = parseInt(parseInt(i+7)*2);
					if(data[1] == 20)
						spin_cnt = spin_cnt * 2;
					new_round_win_multipler_html_temp1 += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}
				else if(i < 1){
					var spin_cnt = 0;
					if(data[4] < 5)
						spin_cnt = parseInt(i+5+7);
					else if(data[4] < 7)
						spin_cnt = parseInt(parseInt(i+5+7)*1.5);
					else
						spin_cnt = parseInt(parseInt(i+5+7)*2);
					if(data[1] == 20)
						spin_cnt = spin_cnt * 2;
					new_round_win_multipler_html_temp1 += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}
				else if(i > 5){
					var spin_cnt = 0;
					if(data[4] < 5)
						spin_cnt = parseInt(i-5+7);
					else if(data[4] < 7)
						spin_cnt = parseInt(parseInt(i-5+7)*1.5);
					else
						spin_cnt = parseInt(parseInt(i-5+7)*2);
					if(data[1] == 20)
						spin_cnt = spin_cnt * 2;
					new_round_win_multipler_html_temp1 += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}
				else{
					var spin_cnt = 0;
					if(data[4] < 5)
						spin_cnt = parseInt(i+7);
					else if(data[4] < 7)
						spin_cnt = parseInt(parseInt(i+7)*1.5);
					else
						spin_cnt = parseInt(parseInt(i+7)*2);
					if(data[1] == 20)
						spin_cnt = spin_cnt * 2;
					new_round_win_multipler_html_temp1 += '<span class="color-orange '+space_html+' bonus_spin_roll_size">'+spin_cnt+'</span>';
				}
			}
		}
		$('.case4.new').css('width','auto');
		$('.case4.new').html(new_round_win_multipler_html_temp1+temp_html);
		bonus_multipler_width = $('.case4.new').width();
		
		win_bonus_html_temp = new_round_win_multipler_html_temp1 + win_bonus_html_temp + win_bonus_html_temp;
		$('.case3.new').css('width','auto');
		$('.case3.new').html(temp_html1);
		var width = $('.case3.new').width();
		bonus__result_width  = width;
		$('.case3.new').css('width','auto');
		$('.case3.new').html(win_bonus_html);
		width = $('.case3.new').width();
		$('.case3.new').css('width',width);
	}
	
	function round_result_list(data){
		var round_result_list = "";
		var width = window.innerWidth;
		for(var i =0;i<data.length;i++){
			if(width > 1100 || (width <= 1100 && i < 8)){
				if(data[i].result == "c" || data[i].result == "d" || data[i].result == "e" || data[i].result == "f" || data[i].result == "a1" || data[i].result == "b1")
					round_result_list += "<div class='round_result_section round_result_section_bonus' style=''";
				else
					round_result_list += "<div class='round_result_section'";
				var length = data[i].multi.toString().length;
				var temp_html = "";
				var width = window.innerWidth;
				if(width > 1600){
					if(length == 5)
						temp_html = "style='margin-left:13px'";
					else if(length == 6)
						temp_html = "style='margin-left:18px'";
					else if(length >=7)
						temp_html = "style='margin-left:23px'";
				}else if(width>1023){
					if(length == 5)
						temp_html = "style='margin-left:7px'";
					else if(length == 6)
						temp_html = "style='margin-left:10px'";
					else if(length >=7)
						temp_html = "style='margin-left:13px'";
				}else if(width > 650){
					if(length == 5)
						temp_html = "style='margin-left:13px'";
					else if(length == 6)
						temp_html = "style='margin-left:18px'";
					else if(length >=7)
						temp_html = "style='margin-left:23px'";
				}else{
					if(length == 5)
						temp_html = "style='margin-left:6px'";
					else if(length == 6)
						temp_html = "style='margin-left:9px'";
					else if(length >6)
						temp_html = "style='margin-left:11px'";
				}
					
				if(data[i].result == "a"){
					round_result_list += '><img '+temp_html+' src="<?php echo base_url();?>assets/images/cylinder-empty-white.png"/><span class="round_result_text color-active mr-1 ml-1">'+data[i].multi+'</span>';
				}
				else if(data[i].result == "b"){
					round_result_list += '><img '+temp_html+' src="<?php echo base_url();?>assets/images/cylinder-loaded.png"/><span class="round_result_text color-orange mr-1 ml-1">'+data[i].multi+'</span>';
				}
				else if(data[i].result == "c"){
					if(width > 1023)
						round_result_list += '><label style="line-height:1.4;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">Bonus Roll</label>';
					else
						round_result_list += '><label style="line-height:1.1;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">Bonus Roll</label>';
				}
				else if(data[i].result == "d"){
					if(width > 1023)
						round_result_list += '><label style="line-height:1.4;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">Bonus '+data[i].multi+'</label>';
					else
						round_result_list += '><label style="line-height:1.1;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">Bonus '+data[i].multi+'</label>';
				}
				else if(data[i].result == "e"){
					if(width > 1023)
						round_result_list += '><label style="line-height:1.4;" class="ml-1 mr-1 font-caps color-active round_result_bonus">Double '+data[i].multi+'</label>';
					else
						round_result_list += '><label style="line-height:1.1;" class="ml-1 mr-1 font-caps color-active round_result_bonus">Double '+data[i].multi+'</label>';
				}
				else if(data[i].result == "f"){
					if(width > 1023)
						round_result_list += '><label style="line-height:1.4;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">Double '+data[i].multi+'</label>';
					else
						round_result_list += '><label style="line-height:1.1;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">Double '+data[i].multi+'</label>';
				}else if(data[i].result == "a1"){
					if(width > 1023)
						round_result_list += '><label style="line-height:1.4;" class="ml-1 mr-1 font-caps color-active round_result_bonus">BONUS '+data[i].multi+'</label>';
					else
						round_result_list += '><label style="line-height:1.1;" class="ml-1 mr-1 font-caps color-active round_result_bonus">BONUS '+data[i].multi+'</label>';
				}else if(data[i].result == "b1"){
					if(width > 1023)
						round_result_list += '><label style="line-height:1.4;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">BONUS '+data[i].multi+'</label>';
					else
						round_result_list += '><label style="line-height:1.1;" class="ml-1 mr-1 font-caps color-orange round_result_bonus">BONUS '+data[i].multi+'</label>';
				}
					
				round_result_list += "</div>";
			}
			
		}
		$('.round_result').empty();
		$('.round_result').html(round_result_list);
	}
	
	function show_brag_menu(){
		if($('.brag-menu').css('display') == 'none'){
			$('.brag-menu').css('display','block');
		}else{
			$('.brag-menu').css('display','none');
		}
	}
	
	function close_brag_menu(){
		$('.brag-menu').css('display','none');
	}
	
	function volume(){
		if(is_volume == 0){
			is_volume = 1;
			$('.volume_section').html('<i class="fas fa-volume-mute font-size-30"></i>');
			$('.mobile_volume').html('<i class="fas fa-volume-mute"></i>')
		}else{
			is_volume = 0;
			$('.volume_section').html('<i class="fas fa-volume-up font-size-30"></i>');
			$('.mobile_volume').html('<i class="fas fa-volume-up"></i>')
		}
	}
	
	function sel_autoplay_spin(cnt){
		var cash = parseInt(sessionStorage.getItem("cash"));
		if(is_freespin_round == 0 && cash > 0){
			is_autoplay = 1;
			cur_autoplay_cnt = cnt;
			$('.remain_cnt').html(formatNumber(cur_autoplay_cnt));
			$('.spin_btn').css('display','none');
			$('.autoplay_spin').css('display','none');
			$('.autoplay_spin_mobile').css('display','none');
			if(game_type == 1){
				change_grey('.play_btn');
				change_grey('.mobile_play_btn');
			}else{
				change_orange('.play_btn');
				change_orange('.mobile_play_btn');
			}
			change_grey('.max_bet_btn');
			$('.autoplay_stop_btn').css('display','block');
			spin('auto');
		} else if(cash <= 0){
			is_autoplay = 0;
			message("error", "No balance");
			$('.autoplay_spin').css('display','none');
			$('.autoplay_spin_mobile').css('display','none');
		}
	}
	
	function stop_autoplay(){
		if(is_freespin_round == 0 && sessionStorage.getItem("status") != null){
			$('.autoplay_stop_btn').css('display','none');
			$('.spin_btn').css('display','block');
			$('.spin_btn .fa-sync-alt').css('color','black');
			$('.play_btn span').html("PLAY");
			$('.mobile_play_btn').html("PLAY");
			is_autoplay = 0;
			
			change_orange('.play_btn');
			change_orange('.mobile_play_btn');
			change_orange('.max_bet_btn');
			change_orange('.spin_btn');
		}
	}
	
	function change_layout(){
		layout_mode = layout_mode + 1;
		if(layout_mode > 3)
			layout_mode = layout_mode - 3;
		var width = window.innerWidth;
		if(layout_mode == 1 && width > 1023){
			$('.layout_1').css('display','flex');
			$('.layout_2').css('display','none');
			$('.layout_3').css('display','none');
		}
		else if(layout_mode == 2 && width > 1023){
			$('.layout_1').css('display','none');
			$('.layout_2').css('display','flex');
			$('.layout_3').css('display','none');
		}
		else if(layout_mode == 3 && width > 1023){
			$('.layout_1').css('display','none');
			$('.layout_2').css('display','none');
			$('.layout_3').css('display','flex');
		}
	}
	
	function close_infomodal(){
		$('.info_section').css('display','none');
	}
	
	function open_infomodal(){
		$('.info_section').css('display','block');
	}
	
	function show_freespin_multipler(result_chance,result_multipler,result_bonus_chance,result_bonus_multipler){
		$('.black_fade').css('display','none');
		$('.bonus_spin_result').css('display','none');
		$('.indivi_mul').css('display','none');
		$('.total_spin').css('display','inline-block');
		var total = total_temp = total_temp1 = 0;
		if(result_chance == 1)
			total_temp = parseFloat((0.815 * result_multipler).toFixed(2));
		else
			total_temp = parseFloat((result_chance * result_multipler).toFixed(2));
		
		if(result_bonus_chance == 1)
			total_temp1 = parseFloat((5*result_bonus_multipler).toFixed(2));
		else if(result_bonus_chance == 2)
			total_temp1 = parseFloat((8*result_bonus_multipler).toFixed(2));
		else if(result_bonus_chance == 3)
			total_temp1 = parseFloat((10*result_bonus_multipler).toFixed(2));
		else if(result_bonus_chance == 4)
			total_temp1 = parseFloat((20*result_bonus_multipler).toFixed(2));
		else
			total_temp1 = parseFloat((50*result_bonus_multipler).toFixed(2));
		
		total_multipler = parseFloat((total_temp * total_temp1).toFixed(2));
	}
	
	function remove_character(str){
		var length = str.length;
		var comma_cnt = parseInt(length/3);
		for(var i =0;i<comma_cnt;i++){
			str = str.replace(',','');
		}
		return str;
	}
	
	function hasLetterSpecial(t)
	{
		var pattern = /[a-zA-Z]+[(@!#\$%\^\&*\)\(+=._-]{1,}/;
		if ( t && t.length > 5 && pattern.test(t)) {
			return true;
		} else {
			return false;
		}
	}
	function hasLetterNumber(t)
	{
		var pattern = /[a-zA-Z]+[0-9]/;
		var pattern1 = /[0-9]+[a-zA-Z]/;
		if ( t && t.length > 5 && (pattern.test(t) || pattern1.test(t))) {
			return true;
		} else {
			return false;
		}
	}
	
	function mobile_auto(){
		$('.wager_mobile').css('display','none');
		if(sessionStorage.getItem("status") == null){
			$("#signupmodal1").modal("toggle");
		}else{
			if($('.max_bet_btn').css('color') == 'rgb(0, 0, 0)' && is_freespin_round == 0){
				if($('.autoplay_spin_mobile').css('display') != 'none')
					$('.autoplay_spin_mobile').css('display','none');
				else
					$('.autoplay_spin_mobile').css('display','block');
			}else if(is_freespin_round == 0){
				if($('.autoplay_spin_mobile').css('display') != 'none')
					$('.autoplay_spin_mobile').css('display','none');
				else
					$('.autoplay_spin_mobile').css('display','block');
			}
		}
	}
	
	function change_grey(name){
		$(name).css('color','#7c7a76');
		$(name).css('background-color','#a3a09b');
		if(name != '.mobile_play_btn' && name != '#wager_mobile_sel' && name != '#spin2' ){
			$(name).css('box-shadow','0px 0.2vw 0px 0px #7c7a76');
			$(name).css('-webkit-box-shadow','0px 0.2vw 0px 0px #7c7a76');
			$(name).css('-moz-box-shadow','0px 0.2vw 0px 0px #7c7a76');
		}else if(name == '.mobile_play_btn' || name == '#wager_mobile_sel' || name == '#spin2'){
			$(name).css('box-shadow','0px 0.5vh 0px 0px #7c7a76');
			$(name).css('-webkit-box-shadow','0px 0.5vh 0px 0px #7c7a76');
			$(name).css('-moz-box-shadow','0px 0.5vh 0px 0px #7c7a76');	
		}else{
			$(name).css('box-shadow','0px 3px 0px 0px #7c7a76');
			$(name).css('-webkit-box-shadow','0px 3px 0px 0px #7c7a76');
			$(name).css('-moz-box-shadow','0px 3px 0px 0px #7c7a76');
		}
	}
	
	function change_orange(name){
		$(name).css('color','black');
		$(name).css('background-color','#F8BF60');
		if(name != '.mobile_play_btn' && name != '#wager_mobile_sel' && name != '#spin2' ){
			$(name).css('box-shadow','0px 0.2vw 0px 0px #9F804A');
			$(name).css('-webkit-box-shadow','0px 0.2vw 0px 0px #9F804A');
			$(name).css('-moz-box-shadow','0px 0.2vw 0px 0px #9F804A');	
		}else if(name == '.mobile_play_btn' || name == '#wager_mobile_sel' || name == '#spin2'){
			$(name).css('box-shadow','0px 0.5vh 0px 0px #9F804A');
			$(name).css('-webkit-box-shadow','0px 0.5vh 0px 0px #9F804A');
			$(name).css('-moz-box-shadow','0px 0.5vh 0px 0px #9F804A');	
		}else{
			$(name).css('box-shadow','0px 3px 0px 0px #9F804A');
			$(name).css('-webkit-box-shadow','0px 3px 0px 0px #9F804A');
			$(name).css('-moz-box-shadow','0px 3px 0px 0px #9F804A');	
		}
	}
	
	function show_signup_modal(){
		$("#loginmodal").modal("toggle");
		$("#signupmodal1").modal("toggle");	
	}
	
	function show_login_password(){
		var x = document.getElementById("password");
		if (x.type === "password") {
			$('.login_show_passwd').addClass('fa-eye');
			$('.login_show_passwd').removeClass('fa-eye-slash');
		    x.type = "text";
		} else {
			$('.login_show_passwd').removeClass('fa-eye');
			$('.login_show_passwd').addClass('fa-eye-slash');
		    x.type = "password";
		}
	}
	
	function show_security_password(){
		var x = document.getElementById("change_new_pwd");
		if (x.type === "password") {
			$('.security_pwd').addClass('fa-eye');
			$('.security_pwd').removeClass('fa-eye-slash');
		    x.type = "text";
		} else {
			$('.security_pwd').removeClass('fa-eye');
			$('.security_pwd').addClass('fa-eye-slash');
		    x.type = "password";
		}
	}
	
	function show_fa_password(){
		var x = document.getElementById("fa_pwd");
		if (x.type === "password") {
			$('.fa_pwd').addClass('fa-eye');
			$('.fa_pwd').removeClass('fa-eye-slash');
		    x.type = "text";
		} else {
			$('.fa_pwd').removeClass('fa-eye');
			$('.fa_pwd').addClass('fa-eye-slash');
		    x.type = "password";
		}
	}
	
	function show_signup_password(type){
		var x = document.getElementById("signup_password");
		var x1 = document.getElementById("signup_password1_1");
		if(type == 2){
			x = document.getElementById("signup_password2_1");
			x1 = document.getElementById("signup_password2_2");
		}
		if (x.type === "password") {
			$('.signup_pass').addClass('fa-eye');
			$('.signup_pass').removeClass('fa-eye-slash');
		    x.type = "text";
		    x1.type = "text";
		} else {
			$('.signup_pass').removeClass('fa-eye');
			$('.signup_pass').addClass('fa-eye-slash');
			x.classList.remove('fa-eye');
			x.classList.add('fa-eye-slash');
		    x.type = "password";
		    x1.type = "password";
		}
	}
	
	function win_countup(total_amount,duration_time,mul_index, round_double){
		$('.counter').css('display','table');
		$('.counter_title').css('display','table');
		$('.black_fade').css('display','block');
		
		change_grey('.max_bet_btn');
		animItem_shoot.goToAndStop(12, true);
		$({ countNum: win_countup_val}).animate({countNum: total_amount,fontSize:win_countup_font_size+"px"},
		{
		    duration: duration_time,
		    easing:'linear',
		    step: function() {
		    	win_countup_val = total_amount;
		      	$('.counter span.counter_section').html(formatNumber(Math.floor(this.countNum)) + ' <i class="fas fa-gem"></i>');
		    },
		    complete: function() {
		      	$('.counter span.counter_section').html(formatNumber(this.countNum) + ' <i class="fas fa-gem"></i>');
		    }
		});
		var step_amount = parseFloat((solo_wager *mul_index).toFixed(2));
		if(round_double == 20)
			step_amount = step_amount * 2;
		step_amount = parseInt(step_amount);
		var cnt = 0;
		$({ countNum1: win_countup_val1}).animate({countNum1: step_amount},
		{
		    duration: duration_time,
		    easing:'linear',
		    step: function() {
		    	cnt++;
		    	if(cnt == 840){
					$('.counter_title').html("<span style='font-size:10px;'>BIG WIN</span>");
					$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
				}
				if(cnt == 1200){
					$('.counter_title').html("<span style='font-size:10px;'>MEGA WIN</span>");
					$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
				}
				if(cnt == 1800){
					$('.counter_title').html("<span style='font-size:10px;'>EPIC WIN</span>");
					$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
				}
				if(cnt == 2400){
					$('.counter_title').html("<span style='font-size:10px;'>LEGENDARY WIN</span>");
					$('.counter_title span').animate({fontSize:win_countup_font_size+"px"},1000);
				}
		    	win_countup_val1 = step_amount;
		      	if(sessionStorage.getItem('status') != null)
		      		$('.bestwager').text(formatNumber(Math.floor(this.countNum1)));
		    },
		    complete: function() {
		      	if(sessionStorage.getItem('status') != null){
		      		if(this.countNum1 >= 1000000000000)
						$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000000000).toFixed(2)+"T");
		      		else if(this.countNum1 >= 1000000000)
						$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000000).toFixed(2)+"B");
					else if(this.countNum1 >= 100000000)
						$('.bestwager').html(parseFloat(parseFloat(this.countNum1)/1000000).toFixed(2)+"M");
					else
						$('.bestwager').text(formatNumber(this.countNum1.toFixed(2)));
				}
		    }
		});
		
		$('.counter span').animate({fontSize:win_countup_font_size+"px"},500);
	}
	
	function end_solo_win_countup(){
		change_grey('.max_bet_btn');
		if(sessionStorage.getItem("status") != null){
			if(parseFloat(sessionStorage.getItem("cash")) >= 1000000000000)
				$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash")/1000000000000).toFixed(2)+"T"));
			else if(parseFloat(sessionStorage.getItem("cash")) >= 1000000000)
				$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash")/1000000000).toFixed(2)+"B"));
			else if(parseFloat(sessionStorage.getItem("cash")) >= 100000000)
				$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash")/1000000).toFixed(2)+"M"));
			else{
				//var width = window.innerWidth;
				//if(width > 1400)
					$(".totalgems").html(formatNumber(parseFloat(sessionStorage.getItem("cash")).toFixed(2)));
				//else
				//	$(".totalgems").html(parseFloat(parseFloat(sessionStorage.getItem("cash")/1000)).toFixed(2)+"K");
			}
		}
	}
	
	async function solo_show_spin_result(solo_round_result){
		var width = window.innerWidth;
		$('.total_spin').css('display','inline-block');
		$('.spin_round').css('display','block');
		
		$('#revolver').css('background-color','#f5bc71');
		$('.bodypart2-2').css('background-color','#f5bc71');
		$('.normal_round').css('display','none');				
				
		$('#iframe_section').css('display','none');
		$('#iframe_section_shoot').css('display','block');
		animItem_shoot.goToAndStop(12,true);
				
		$('.spin_round_trophy').css('display','none');
		if($('.roulette').css('display') != 'none')
			$('.roulette').css('display','none');
		if($('.black_fade').css('display') != 'block')
			$('.black_fade').css('display','block');
		if($('.count_time').css('display') != 'none')
			$('.count_time').css('display','none');
		
		
		if(is_solo_bonus_multipler == 0){
			is_solo_bonus_multipler = 1;
			$('#revolver .bonus_spin_result').css('display','table');
			var spin_html = spin_cnt_html(solo_round_result);
			$(".bonus_win .case3.new").empty();
			$(".bonus_win .case3.new").html(win_bonus_html_temp);
			if(is_solo_freespin_round == 0){
				$('.bonus_spin_result .fa-trophy').addClass('color-orange');
			}
			if(sessionStorage.getItem('wager_index') == -1)
				sessionStorage.setItem('wager_index',wager_index);
		}
		for(var i = 0;i<300;i++){
			if(game_type == 2){
				await new Promise(done => setTimeout(function(){
					var distance = bonus_multipler_width / 171;
					if(i < 170){
						$('.bonus_win .case3.new').css('left',-1 * distance *i+"px");
					}
					else if(i == 170){
						$('.bonus_win .case3.new').css('left',-1 * bonus_multipler_width+"px");
					}
					else if(i <= 250){
						distance1 = bonus__result_width / 81;
						if(i == 250)
							$('.case3.new').css('left', (-1 * distance1 * parseInt(i - 170)).toFixed(2) -bonus_multipler_width+2+"px");
						else
							$('.case3.new').css('left', (-1 * distance1 * parseInt(i - 170)).toFixed(2) -bonus_multipler_width+"px");
					}
					else{
						if(is_attend_round == true && sessionStorage.getItem('status') != null){
							sessionStorage.setItem('is_solo_freespin_round',1);
						}
						$('.win_bonus_center').css('color','#F8BF60');
					}
					if(i == 299){
						if(solo_round_result[4] < 5){
							if(solo_round_result[1] == 20)
								solo_remain_freespin_cnt = solo_remain_freespin_cnt + parseInt(solo_round_result[5]+7) * 2;
							else
								solo_remain_freespin_cnt = solo_remain_freespin_cnt + parseInt(solo_round_result[5]+7);
						}else if(solo_round_result[4] < 7){
							if(solo_round_result[1] == 20)
								solo_remain_freespin_cnt = solo_remain_freespin_cnt + parseInt(1.5 * parseInt(solo_round_result[5]+7) * 2);
							else
								solo_remain_freespin_cnt = solo_remain_freespin_cnt + parseInt(parseInt(solo_round_result[5]+7) * 1.5);
						} else{
							if(solo_round_result[1] == 20)
								solo_remain_freespin_cnt = solo_remain_freespin_cnt + parseInt(2 * parseInt(solo_round_result[5]+7) * 2);
							else
								solo_remain_freespin_cnt = solo_remain_freespin_cnt + parseInt(parseInt(solo_round_result[5]+7) * 2);
						}
						var total_spins = solo_remain_freespin_cnt;
						is_solo_freespin_round = 1;
						sessionStorage.setItem('free_spins',total_spins);
						
						$('#reward_spin_cnt').text(total_spins);
						$('#account_spins_cnt').text(total_spins);
						
						var width = window.innerWidth;
						if(width > 1023 && is_solo_freespin_round == 1 && sessionStorage.getItem('is_solo_freespin_round') == 1){	
							$('.play_bottom_part').css('display','none');
							$('.spin_btn').css('display','none');
							$('.remain_cnt').css('display','none');
							$('.autoplay_stop_btn').css('display','none');
							$('.bonus_spin_cnt_section').css('display','block');
							$('.bonus_spin_cnt').html(total_spins+" REMAINING");	
						}
					}
					done();
				},16));
			}
		}
		socket.emit('end solo new gun animation', {solo_round_result:solo_round_result, pre_round_result:pre_round_result, wager: solo_wager, user_id : sessionStorage.getItem('status'), is_shoot : 1});	
	}
	
	function help_fair_modal(type){
		if(type == 'help'){
			$('#help_menu').addClass('active');
			$('#help_menu').addClass('show');
			$('#provably_menu').removeClass('active');
			$('#provably_menu').removeClass('show');
			$('#help_section').addClass('active');
			$('#help_section').addClass('show');
			$('#provably_section').removeClass('active');
			$('#provably_section').removeClass('show');	
		}else{
			$('#help_menu').removeClass('active');
			$('#help_menu').removeClass('show');
			$('#provably_menu').addClass('active');
			$('#provably_menu').addClass('show');
			$('#help_section').removeClass('active');
			$('#help_section').removeClass('show');
			$('#provably_section').addClass('active');
			$('#provably_section').addClass('show');
		}
	}
</script>
</body>
</html>