<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Russian Roulette</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<meta content="no" name="apple-touch-fullscreen">
		<meta name="MobileOptimized" content="320"/>
		<meta name="format-detection" content="telephone=no">
		<meta name=apple-mobile-web-app-capable content=yes>
		<meta name=apple-mobile-web-app-status-bar-style content=black>
		<meta http-equiv="pragma" content="nocache">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">
        
        <!-- Plugins css-->
        <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/vendor/fontawesome/css/all1.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/custom/style.css" rel="stylesheet" type="text/css" />
		<!--link href="<?php echo base_url();?>assets/custom/animate.css" rel="stylesheet" type="text/css" /-->
		<link href="<?php echo base_url();?>assets/custom/toastr.min.css" rel="stylesheet" type="text/css" />
		<!--link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/tooltipster/dist/css/tooltipster.bundle.min.css" /-->

<link href="<?php echo base_url();?>assets/vendor/progressbar/jsRapBar.css" rel="stylesheet" type="text/css" />
		
    </head>
  	<body>
  		