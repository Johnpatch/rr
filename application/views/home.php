<section class="container-fluid">
	<div class="flexible" id="flowarea">

			<div class="left-side">
				
				<div class="chat-header-part" >
					<span></span>
					<img src="<?php echo base_url();?>assets/images/rrlogo.svg"/>
				</div>
				<div id="speedregisterregion">
					<div class="font-caps text-center reg_instant">
						Register instantly
					</div>
					<div class="reg_instant_input pos-relative">
						<input type="text" class="color-active left_input" id="left_reg_name" placeholder=" " />
						<label class="form-control-left login_name" for="left_reg_name">Choose a username</label>
					</div>
				
					<div class="text-center reg_instant_btn">
						<a class="font-caps" id="speedregisterbutton">PLAY FREE INSTANTLY</a>
					</div>
					<div class="reg_instant_desc">By accessing this site, I attest that I am at least 18 years old and agree to the Terms and Conditions.</div>
				</div>
				<div class="personal-level-part" style="display: none;">
					<div class="level-icon-part">
						<div class="level-icon-square" id="level_circel_prog">
							<img class="level-icon">
						</div>
					</div>
					<div class="level-mark-part">
						<span id="left_menu_level"></span>
					</div>
				</div>
				<ul class="nav nav-tabs left-chat" style="">
					<li class="nav-item">
						<div data-target="#social" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-tasks left-menu-icon"></i><br/>
						</div>
					</li>
					
					<li class="nav-item">
						<div data-target="#rooms" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-trophy left-menu-icon"></i><br/>
						</div>
					</li>
					<li class="nav-item">
						<div data-target="#rules" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-book left-menu-icon"></i><br/>
						</div>
					</li>
					<li class="nav-item nav-item-text">
						<div data-target="#chat" data-toggle="tab" class="nav-link small active color-inactive text-center left-side-chat">
							<span class="left-menu-chat-label">English</span><br/>
							<span style="color:white;" class=" left-menu-chat-label">Chat Room</span>
						</div>
					</li>
					<li class="nav-item">
						<div data-target="#social" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-users left-menu-icon"></i><br/>
						</div>
					</li>
					<li class="nav-item">
						<div data-target="#rooms" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-door-open left-menu-icon"></i><br/>
						</div>
					</li>
					<li class="nav-item">
						<div class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-times left-menu-icon"></i><br/>
						</div>
					</li>
				</ul>
				<div class="scrollable tab-content chatbox pt-2 small">
					<div id="rules" class="tab-pane fade">

					</div>
					<div id="rooms" class="tab-pane fade">

					</div>
					<div id="chat" class="tab-pane fade active show" style="font-weight: 500;">

					</div>
					<div id="social" class="tab-pane fade">

					</div>
				</div>
				<div class="brag-menu p-0 left-side1">
					<li class="text-center brag_top_section">
						<img style="width:20px;float:right;cursor:pointer;" src="<?php echo base_url();?>assets/images/close.png" onclick="close_brag_menu()">
						<p class="color-orange" style="padding-top:10px;margin-bottom: 0;">BRAG</p>
						<p class="color-inactive" style="font-size:11px;margin-bottom: 0px;">Send a BRAG to show</p>
						<p class="color-inactive" style="font-size:11px;margin-bottom: 0px;">your <label class="color-orange">SKILL</label> and <label class="color-orange">SUCCESS</label></p>
						<label class="color-active brag_describe">To send a God Message, select one of the God Message Tiers below and then type the message you want to show up Example:"brag1250 I'M ON A ROLL!"</label><br>
						
					</li>
                    <li class="color-orange brags text-center">
                        <a class="brag_section" style="color: #F8BF60;"><p>1,000 <i class="far fa-gem"></i></p><p class="color-active" style="font-size:10px;">Tier 6</p></a>
                        <input type="hidden" value="1000" />
                    </li>
                    <li class="color-red brags text-center">
                        <a class="brag_section" style="color: red;"><p>800 <i class="far fa-gem"></i></p><p class="color-active" style="font-size:10px;">Tier 5</p></a>
                        <input type="hidden" value="800" />
                    </li>
                    <li class="color-dark-pink brags text-center">
                        <a class="brag_section" style="color: #AF60F8;"><p>600 <i class="far fa-gem"></i></p><p class="color-active" style="font-size:10px;">Tier 4</p></a>
                        <input type="hidden" value="600" />
                    </li>
                    <li class="color-dark-blue brags text-center">
                        <a class="brag_section" style="color: #2989AD;"><p>400 <i class="far fa-gem"></i></p><p class="color-active" style="font-size:10px;">Tier 3</p></a>
                        <input type="hidden" value="400" />
                    </li>
                    <li class="color-red-orange brags text-center">
                        <a class="brag_section" style="color: #CD5334;"><p>200 <i class="far fa-gem"></i></p><p class="color-active" style="font-size:10px;">Tier 2</p></a>
                        <input type="hidden" value="200" />
                    </li>
                    <li class="color-inactive brags text-center">
                        <a class="brag_section" style="color: white;"><p>Normal <i class="far fa-gem"></i></p><p class="color-active" style="font-size:10px;">Tier 1</p></a>
                    </li>
                </div>
				<div id="chatbox">
					<textarea type="text" class="color-active chattext" rows="1" placeholder="Type here..." autocomplete="off" style="resize:none;"></textarea>
					<ul class="nav navbar-nav chat_emo">
						<li class="dropdown">
							<a class="dropdown-toggle" style="color: #F8BF60;cursor:pointer;" onclick="show_brag_menu()"><i class="far fa-gem"></i></a>
							
						</li>
						<li class="dropdown" style="margin-left: 6px;">
							<a class="dropdown-toggle" data-toggle="dropdown" style="color: #F8BF60;"><i class="far fa-grin"></i></a>
						</li>
					</ul>
				</div>
				<div id="status" class="flexible" style="margin-top: 9px;">
					<div class="pl-2" id="onlineusers">
						<span class="color-orange"><i class="fas fa-circle"></i> </span>Online: 0
					</div>
					<div class="chat_btn_section color-orange">
						<a class="color-orange "><i class="fab fa-instagram"></i></a>
						<a class="color-orange "><i class="fab fa-twitter"></i></a>
						<a class="color-orange "><i class="fab fa-facebook"></i></a>
						<a class="color-orange "><i class="fab fa-youtube"></i></a>
						<a id="chatsubmit" class="pl-2 pr-2 pt-1 pb-1">CHAT</a>
					</div>
				</div>
			</div>
			<!-- Main area -->
			<!--div class="col-md-10"-->
				<div id="mainboard">
					<!-- Top header part -->
					<div class="flexible top_header">
						<div class="flexible" id="loggedin">
							<div class="dropdown more_btn">
								<li class="dropdown-toggle" data-toggle="dropdown" style="list-style: none;color: #5d616b;"><i class="fa fa-bars"></i></li>
								<ul class="dropdown-menu">
								    <li data-toggle="modal" data-target="#helpmodal" onclick="help_fair_modal('help')">HELP</li>
								    <li data-toggle="modal" data-target="#helpmodal" onclick="help_fair_modal('fair')">FAIRNESS</li>
								    <li data-toggle="modal" data-target="#rewardmodal">REWARDS</li>
								    <li data-toggle="modal" data-target="#accountmodal">ACCOUNT</li>
								    <li>WITHDRAW</li>
								    <li data-toggle="modal" data-target="#depositmodal">DEPOSIT</li>
								</ul>
							</div>
						</div>
						<div class="flexible" style="position: relative; margin: auto;">
							<a class="color-active font-caps top_header_left_menu" data-toggle="modal" data-target="#helpmodal" onclick="help_fair_modal('help')">Help</a>
							<a class="color-active font-caps top_header_left_menu" data-toggle="modal" data-target="#helpmodal" onclick="help_fair_modal('fair')">Fairness</a>
							<a class="color-active font-caps top_header_left_menu" data-toggle="modal" data-target="#rewardmodal" id="rewardsbutton">Rewards</a>
							<a class="color-black font-caps top_header_btn" data-toggle="modal" data-target="#loginmodal" id="loginbutton">Sign in</a>
							<a class="color-black font-caps top_header_btn mr-2" data-toggle="modal" data-target="#signupmodal1" id="signupbutton">Sign up</a>
							<a data-toggle="modal" data-target="#depositmodal" class="color-black font-caps top_header_btn" id="header_depositbutton">Deposit</a>
							<a class="color-active font-caps top_header_left_menu" id="withdrawbutton">Withdraw</a>
							<a data-toggle="modal" data-target="#accountmodal" class="color-active font-caps top_header_left_menu" id="accountbutton">Account</a>
						</div>
						<div class="flexible" id="loggedout">
							<a class="color-active" data-toggle="modal" data-target="#loginmodal" id="loginbutton_mobile">Login</a>
							<a class="color-black top_header_btn mr-2" data-toggle="modal" data-target="#signupmodal1" id="signupbutton_mobile" style="color: black;">Register</a>
						</div>
						
					</div>
					
					<div class="before_login">
						<div class="before_login_left">
							<table class="gem_table">
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td colspan="2" class="color-orange before_login_x2">X2</td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
							</table>
						</div>
						<div class="before_login_adv">
							<div class="bonus_adv">
								<div class="text-center welcome">
									<label class="color-orange" >YOUR WELCOME BONUS</label>
								</div>
								<div class="text-center describe">
									<div><label class="color-orange">100% BONUS <label class="color-active">UP TO</label> $300</label></div>
									<div style="line-height: 0;"><label class="color-active">+</label></div>
									<div><label class="color-orange">30 FREE SPINS!</label></div>
								</div>
								<div class="text-center btn_section">
									<div class="each_part adv_nickname_section pos-relative">
										<textarea class="adv_input" id="adv_nickname" placeholder=" "></textarea>
										<label class="form-control-adv name_put" for="adv_nickname">Choose a username</label>
									</div>
					
									<div class="each_part">
										<a class="color-active font-caps orange_btn adv_login_btn">PLAY NOW</a>
									</div>
								</div>
								<div class="text-center bonus_term">
									<a href="#" class="color-active underline">Bonus terms</a>
								</div>
							</div>
							<div class="text-center bonus_desc">
								<label class="color-active signup_describe">New customers only. Opt-in required. 30 Free Spins on sign up. 100% Deposit bonus up to $300 (30,000 gems) 35x wagering requirements apply on bonus and free spins. Max bet applies. Unused spins expire after 7 days. Deposit bonus expires after 30 days. Terms and conditions apply.</label>
							</div>
						</div>
						<div class="before_login_right">
							<table class="gem_table">
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td colspan="2" class="color-orange before_login_x2">X2</td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="before_login_mobile">
						<div class="gem_part">
							<table class="gem_table">
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td class="before_login_x2 color-orange">X2</td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
							</table>
						</div>
						<div class="before_login_adv">
							<div class="adv_section">
								<div class="text-center welcome">
									<label class="color-orange" >WELCOME BONUS FOR ALL NEW PLAYERS</label>
								</div>
								<div class="text-center describe">
									<div><label class="color-orange">100% BONUS </label></div>
									<div class="color-orange"><label class="color-active">UP TO</label> $300</div>
									<div style="line-height: 0;"><label class="color-active">+</label></div>
									<div><label class="color-orange">30 FREE SPINS!</label></div>
								</div>
								<div class="text-center btn_section">
									<div class="each_part adv_nickname_section pos-relative">
										<textarea class="adv_input" id="adv_nickname1" placeholder=" "></textarea>
										<label class="form-control-adv name_put" for="adv_nickname1">Choose a username</label>
									</div>
									
									<div class="each_part">
										<a class="color-active font-caps orange_btn adv_login_btn">PLAY NOW</a>
									</div>
								</div>
								<div class="text-center bonus_term">
									<a href="#" class="color-active underline">Bonus terms</a>
								</div>
							</div>
							<div class="text-center desc_section">
								<label class="color-active signup_describe">New customers only. Opt-in required. 30 Free Spins on sign up. 100% Deposit bonus up to $300 (30,000 gems) 35x wagering requirements apply on bonus and free spins. Max bet applies. Unused spins expire after 7 days. Deposit bonus expires after 30 days. Terms and conditions apply.</label>
							</div>
						</div>
						<div class="gem_part">
							<table class="gem_table">
								<tr>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td class="before_login_x2 color-orange">X2</td>
									<td><i class="fas fa-gem color-orange"></i></td>
									<td><i class="fas fa-gem color-orange"></i></td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="flexible" id="bodypart0">
						<div class="color-inactive font-caps levelpart1-section text-left levelpart-section">
							<span class="color-red-orange" id="levelpart1"></span>
						</div>
						<div class="color-inactive" id="exppart">
							<div id="expbar-fit"></div>
							<div class="color-black" id="expcur" style="font-weight: bold;"></div>
							<div class="color-black" id="exptot" style="font-weight: bold;"></div>
							<div class="color-black" id="expnxt" style="font-weight: bold;"></div>
						</div>
						<div class="color-inactive font-caps levelpart2-section text-right levelpart-section">
							<span class="color-orange" id="levelpart2"></span>
						</div>
					</div>
					<div id="mainbodypart">
						<div class="flexible" id="bodypart1">
							<div class="bodypart-side bodypart1-side">
								<span class="color-orange font-caps">Russian Roulette<br>Players</span>
								
							</div>
							<div class="bodypart1-2">
								<span class="group_game text-center" onclick="game_type_sel(1)">RUSSIAN ROULETTE<br>GROUP</span>
								<span class="solo_game text-center" onclick="game_type_sel(2)">RUSSIAN ROULETTE<br>SOLO</span>
								<span class="group_game_mobile text-center" onclick="game_type_sel(1)">RUSSIAN ROULETTE GROUP</span>
								<span class="solo_game_mobile text-center" onclick="game_type_sel(2)">RUSSIAN ROULETTE SOLO</span>
							</div>
							<div class="bodypart-side bodypart1-side bodypart1-3">
								<span class="color-orange font-caps">All-time<br/>top winners</span>
							</div>
						</div>
						<div class="flexible" id="bodypart2">
							<div class="flexible bodypart-side bodypart2-1">
								<div class="bodypart2-1-1">
									<div class="font-caps color-orange text-center" style="width: 100%;">
										<i class="color-orange fas fa-users player_icon"></i>
									</div>
									<div class="color-active text-center player_list_left player_count">
										0
									</div>
								</div>
								<div class="bodypart2-1-2">
									<div class="font-caps color-orange" style="width: 100%;">
										<p style="margin-bottom: 0;text-align:center;"><i class="color-orange fas fa-gem player_icon"></i></p>
									</div>
									<div class="player_list_left" style="text-align: center;">
										<span class="color-active player_wager">0</span>&nbsp;<span class="color-orange"><i class="fas fa-gem" ></i></span>
									</div>
								</div>
								<div class="bodypart2-1-3">
									<div class="font-caps color-orange text-center" style="width: 100%;">
										<i class="color-orange fas fa-gem player_icon"></i>
									</div>
									<div class="color-active text-center player_list_left">
										<span class="color-orange player_win">---</span> <i class="fas fa-gem color-orange"></i>
									</div>
								</div>
							</div>
							<div class="bodypart2-2">
								<div class="normal_round">
									<div class="font-caps color-orange text-center win_multi_title">
									</div>
									<div class="text-center win_multipler">
										<div class="roulette_multi" style="display: inline-block;">
											<div class="roulette_wheel">
												<div class="wheel-container-multi indivi_mul">
													<div class="case timer-off">
														<div class="case1 new"></div>
														<div class="case2" style="display: none;"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="spin_round" style="display: none;">
									<div class="font-caps color-orange text-center">
										<div class="spin_left_mul">
											<div class="spin_mul_title">WIN MULTIPLER</div>
											<div class="text-center win_multipler">
												<div class="roulette_multi" style="display: inline-block;">
													<div class="roulette_wheel">
														<div class="wheel-container-multi total_spin total_left_mul">
															<div class="case timer-off">
																<div class="left_mul new spin_multipler"></div>
																<div class="left_mul1 new spin_multipler" style="display: none;"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="spin_center_mul">
											<div class="spin_mul_title">TOTAL MULTIPLER</div>
											<div class="text-center win_multipler">
												<div class="roulette_multi" style="display: inline-block;">
													<div class="roulette_wheel">
														<div class="wheel-container-multi total_spin total_center_mul">
															<div class="case timer-off">
																<div class="center_mul new spin_multipler"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="spin_right_mul">
											<div class="spin_mul_title">BONUS MULTIPLER</div>
											<div class="text-center win_multipler">
												<div class="roulette_multi" style="display: inline-block;">
													<div class="roulette_wheel">
														<div class="wheel-container-multi total_spin total_right_mul">
															<div class="case timer-off">
																<div class="right_mul new spin_multipler"></div>
																<div class="right_mul1 new spin_multipler" style="display: none;"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="flexible bodypart-side bodypart2-3">
								<div class="bodypart2-3-child bodypart2-3-child-1">
									<span class="font-caps color-orange"><i class="color-orange fas fa-users"></i></span>
								</div>
								<div class="bodypart2-3-child bodypart2-3-child-2">
									<span class="font-caps color-orange"><i class="color-orange fas fa-gem"></i></span>
								</div>
								<div class="bodypart2-3-child bodypart2-3-child-3">
									<span class="font-caps color-orange"><i class="color-orange fas fa-gem"></i></span>
								</div>
							</div>
						</div>
						<div class="flexible" style="width: 100%; margin-top: 5px;" id="bodypart3">
							<div class="scrollable player_list bodypart-side">
								
							</div>
							<div class="bodypart3-2">
								<div id="revolver">
									<i class="fas fa-trophy color-black spin_round_trophy" style="left:10px;top:5px;"></i>
									<i class="fas fa-trophy color-black spin_round_trophy" style="right:10px;top:5px;"></i>
									<i class="fas fa-trophy color-black spin_round_trophy" style="left:10px;bottom:5px;"></i>
									<i class="fas fa-trophy color-black spin_round_trophy" style="right:10px;bottom:5px;"></i>
									<div style="width:100%;height: 100%;position:absolute;display: none;" id="iframe_section">
										
									</div>
									<div style="width:100%;height: 100%;display:none;position:absolute;" id="iframe_section_shoot">
										
									</div>
                                    <div style="width:100%;height: 100%;position:absolute;" id="progress">

                                    </div>
									<div class="counter color-orange"><span class="counter_section" id="counter_section" ></span></div>
									<div class="counter_title color-orange"><span></span></div>
																		
									<div class="color-oragne count_time">
									</div>
									<div class="color-oragne bonus_spin_result" style="display: none;">
										
										<label class="color-orange bonus_roll bonus_won_top">BONUS WON</label><label class="color-orange bonus_roll bonus_won_bottom">BONUS WON</label><i class="fas fa-trophy color-orange ml-2 mt-2 bonus_win_left" style="z-index:11;"></i><i class="fas fa-trophy color-orange ml-2 bonus_win_left bonus_win_bottom" style="z-index:11;"></i>	<div class="bonus_win color-orange"><label class="color-orange bonus_win_cong">CONGRATULATIONS</label><br><label class="color-orange bonus_win_won">YOU HAVE WON</label><br> 
											<div class="text-center win_multipler">
												<div class="roulette_multi" style="display: inline-block;">
													<div class="roulette_wheel">
														<div class="wheel-container-multi">
															<div class="case timer-off">
																<div class="case3 new"></div>
																<div class="case4 new" style="display: none;">
																</div>
															</div>
														</div>
														
													</div>
												</div>
											</div><label class="color-orange bonus_win_free">FREE SPINS</label></div><i class="fas fa-trophy color-orange mt-2 bonus_win_right" style="z-index:11;"></i><i class="fas fa-trophy color-orange bonus_win_right bonus_win_bottom" style="z-index:11;"></i>
									</div>
									
									<div class="roulette">
										<label class="color-orange bonus_roll bonus_roll_top">BONUS ROLL</label>
										<label class="color-orange bonus_roll bonus_roll_bottom">BONUS ROLL</label>
										<div class="roultte_wheel">
											<div class="wheel-container">
												<div id="pointer"></div>
												<div id="case" class="case timer-off">
													<div class="case new">
														<div id="counter"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="text-center round_result">
									
								</div>
							</div>
							<div class="scrollable top_player_list bodypart-side">
								
							</div>
							<div style="display: none;" id="top_test"></div>
							<div style="display: none;" id="player_test"></div>
						</div>
						
						<div class="black_fade"></div>
					</div>
					
					<div class="flexible footer_mobile">
						<div class="flexible footerbody mobile_gem_data">
							<div class="mobile_wager_section">
								<div class="mobile_wager_body">
									<div class="mobile_wager_icon_1">
										<i class="fas fa-wallet color-orange first_icon"></i>
									</div>
									<div class="mobile_wager_icon_2">
										<span class="color-active totalgems"></span>
									</div>
									<div class="mobile_wager_icon_3">
										<i class="fas fa-gem color-orange last_icon"></i>
									</div>
								</div>
							</div>
							<div class="mobile_wager_section">
								<div class="mobile_wager_body">
									<div class="mobile_wager_icon_1">
										<i class="fas fa-trophy color-orange first_icon"></i>
									</div>
									<div class="mobile_wager_icon_2">
										<span class="color-active bestwager"></span>
									</div>
									<div class="mobile_wager_icon_3">
										<i class="fas fa-gem color-orange last_icon"></i>
									</div>
								</div>
							</div>
							<div class="mobile_wager_section">
								<div class="mobile_wager_body">
									<div class="mobile_wager_icon_1">
										<i class="fas fa-hand-holding-usd color-orange first_icon"></i>
									</div>
									<div class="mobile_wager_icon_2">
										<span class="color-active curwager">300</span>
									</div>
									<div class="mobile_wager_icon_3">
										<i class="fas fa-gem color-orange last_icon"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="flexible bodypart-side footer-3 mobile_spin">
							<div class="autoplay_spin_mobile">
								<li class="text-center color-black" onclick="sel_autoplay_spin(10)">10</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(25)">25</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(50)">50</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(100)">100</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(250)">250</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(500)">500</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(1000)">1,000</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(10000)">10,000</li>
							</div>
							
							<div class="wager_mobile">
								<li class="text-center color-black" onclick="sel_wager(0)">30</li>
								<li class="text-center color-black" onclick="sel_wager(1)">50</li>
								<li class="text-center color-black" onclick="sel_wager(2)">85</li>
								<li class="text-center color-black" onclick="sel_wager(3)">100</li>
								<li class="text-center color-black" onclick="sel_wager(4)">125</li>
								<li class="text-center color-black" onclick="sel_wager(5)">175</li>
								<li class="text-center color-black" onclick="sel_wager(6)">200</li>
								<li class="text-center color-black" onclick="sel_wager(7)">300</li>
								<li class="text-center color-black" onclick="sel_wager(8)">600</li>
								<li class="text-center color-black" onclick="sel_wager(9)">850</li>
								<li class="text-center color-black" onclick="sel_wager(10)">1000</li>
								<li class="text-center color-black" onclick="sel_wager(11)">1500</li>
								<li class="text-center color-black" onclick="sel_wager(12)">3000</li>
								<li class="text-center color-black" onclick="sel_wager(13)">6000</li>
								<li class="text-center color-black" onclick="sel_wager(14)">9000</li>
								<li class="text-center color-black" onclick="sel_wager(15)">15000</li>
								<li class="text-center color-black" onclick="sel_wager(16)">30000</li>
								<li class="text-center color-black" onclick="sel_wager(17)">60000</li>
								<li class="text-center color-black" onclick="sel_wager(18)">90000</li>
								<li class="text-center color-black" onclick="sel_wager(19)">150000</li>
								<li class="text-center color-black" onclick="sel_wager(20)">300000</li>
								<li class="text-center color-black" onclick="sel_wager(21)">600000</li>
								<li class="text-center color-black" onclick="sel_wager(22)">1000000</li>
							</div>
							
							<div class="flexible mobile-spin-section">
								<div class="mobile-spin-part mobile-spin-part-left-padding" >
									<div style="height: 50%;">
										<div class="text-center color-black mobile_small_play_btn float-right mobile_spin_top_btn" onclick="open_infomodal()">HOW TO<br>PLAY</div>
									</div>
									<div style="height: 50%;">
										<div class="text-center color-black mobile_small_play_btn float-right mobile_spin_bottom_btn mobile_volume" onclick="volume()"><i class="fas fa-volume-up" style="margin-top: 1vh;"></i></div>
									</div>
								</div>
								<div class="mobile-spin-part-center">
									<div class="text-center color-black mobile_play_btn" onclick="spin('')">PLAY</div>
								</div>
								<div class="mobile-spin-part mobile-spin-part-right-padding">
									<div style="height: 50%;">
										<div id="wager_mobile_sel" class="text-center color-black mobile_small_play_btn float-left mobile_spin_top_btn" style="padding-top:1vh;" onclick="wager_count_mobile()">WAGER</div>
									</div>
									<div style="height: 50%;">
										<div id="spin2" class="text-center color-black mobile_small_play_btn float-left mobile_spin_bottom_btn" style="padding-top:1vh;" onclick="mobile_auto()">AUTO</div>
									</div>
							</div>							
							
						</div>
					</div>
					<!-- Layout 1 -->
					<div class="flexible footer layout_1">
						<div class="flexible footer-1">
							<div class="footer-icon" onclick="change_layout()">
								<i class="fas fa-exchange-alt font-size-30"></i>
							</div>
							<div class="footer-icon volume_section" onclick="volume()">
								<i class="fas fa-volume-up font-size-30"></i>
							</div>
							<div class="footer-icon" onclick="open_infomodal()">
								<i class="fas fa-info font-size-30" style="cursor: pointer;"></i>
							</div>
						</div>
						<div class="flexible footerbody">
							<div class="footer_center">
								<div class="font-caps title">gems</div>
								<span class="cost totalgems"></span>&nbsp;<i class="fas fa-gem color-orange cost"></i>
							</div>
							<div class="footer_center bonus_wait_section bonus_total_win_section">
								<div class="font-caps title">win</div>
								<span class="color-orange cost bestwager"> </span>&nbsp;<i class="color-orange cost fas fa-gem"></i>
								<span class="color-orange bonus_wait">
									FREE SPINS <span class="color-active">IN PROGRESS<br>PLEASE WAIT...</span>
								</span>
							</div>
							<div class="footer_center padding_right_30 curwager-section bonus_wait_section">
								<div class="wager_amount" style="position: absolute;">
									<div class="font-caps title">wager</div>
									<span class="curwager curwager_desktop">300</span>&nbsp;<i class="fas fa-gem cost color-orange"></i>
								</div>
								<div class="wager_set">
									<div class="wager_up" onclick="wager_count(1)">
										<button><i class="fas fa-caret-up"></i></button>
									</div>
									<div class="wager_down" onclick="wager_count(0)">
										<button><i class="fas fa-caret-down"></i></button>
									</div>
								</div>
								<span class="color-orange bonus_wait1">
									FREE SPINS <span class="color-active">IN PROGRESS<br>PLEASE WAIT...</span>
								</span>
							</div>
						</div>
						<div class="flexible bodypart-side footer-3">
							<div class="autoplay_spin">
								<li class="text-center color-black" onclick="sel_autoplay_spin(10)">10</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(25)">25</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(50)">50</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(100)">100</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(250)">250</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(500)">500</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(1000)">1,000</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(10000)">10,000</li>
							</div>
							<div class="play_bottom_part max_bet_btn color-black" onclick="spin('max')">
								<span>MAX BET</span>
							</div>
							<div class="play_bottom_part play_btn color-black" onclick="spin('')">
								<span>PLAY</span>
							</div>
							<div class="spin_btn" id="spin1">	
								<span><i class="fas fa-sync-alt font-size-30 color-black"></i></span>
							</div>
							<div class="autoplay_stop_btn" onclick="stop_autoplay()">
								<div class="stop_button">
									<i class="fas fa-stop font-size-20 color-black stop_play"></i>
								</div>
								<div class="remain_cnt text-center color-black"></div>
							</div>
							
							<div class="bonus_spin_cnt_section color-orange text-center">
								<span class="bonus_spin_title">FREE SPINS</span><br>
								<span class="bonus_spin_cnt"></span>
							</div>
							<div class="flexible mobile-spin-section">
								<div class="mobile-spin-side">
									<i class="fas fa-gem"></i>
								</div>
								<div style="width:50%;">
									<div class="spin_btn_mobile" onclick="spin('')">
										<a class="a_black"><i class="fas fa-sync-alt "></i></a>
									</div>
								</div>
								<div class="mobile-spin-side">
									<i class="far fa-play-circle mobile-spin-side"></i>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Layout 2 -->
					<div class="flexible footer layout_2">
						<div class="flexible footer-1">
							<div class="footer-icon" onclick="change_layout()">
								<i class="fas fa-exchange-alt font-size-30"></i>
							</div>
							<div class="footer-icon volume_section" onclick="volume()">
								<i class="fas fa-volume-up font-size-30"></i>
							</div>
							<div class="footer-icon" onclick="open_infomodal()">
								<i class="fas fa-info font-size-30" style="cursor: pointer;"></i>
							</div>
						</div>
						<div class="flexible footerbody">
							<div class="footer_center padding_right_30 curwager-section bonus_wait_section">
								<div class="wager_amount" style="position: absolute;">
									<div class="font-caps title">wager</div>
									<span class="curwager curwager_desktop">300</span>&nbsp;<i class="fas fa-gem cost color-orange"></i>
								</div>
								<div class="wager_set">
									<div class="wager_up" onclick="wager_count(1)"><i class="fas fa-caret-up"></i></div>
									<div class="wager_down" onclick="wager_count(0)"><i class="fas fa-caret-down"></i></div>
								</div>
								<span class="color-orange bonus_wait1">
									FREE SPINS <span class="color-active">IN PROGRESS<br>PLEASE WAIT...</span>
								</span>
							</div>
							<div class="footer_center layout_2_play_section">
								<div class="autoplay_spin">
									<li class="text-center color-black" onclick="sel_autoplay_spin(10)">10</li>
									<li class="text-center color-black" onclick="sel_autoplay_spin(25)">25</li>
									<li class="text-center color-black" onclick="sel_autoplay_spin(50)">50</li>
									<li class="text-center color-black" onclick="sel_autoplay_spin(100)">100</li>
									<li class="text-center color-black" onclick="sel_autoplay_spin(250)">250</li>
									<li class="text-center color-black" onclick="sel_autoplay_spin(500)">500</li>
									<li class="text-center color-black" onclick="sel_autoplay_spin(1000)">1,000</li>
									<li class="text-center color-black" onclick="sel_autoplay_spin(10000)">10,000</li>
								</div>
								<div class="play_bottom_part max_bet_btn color-black" onclick="spin('max')">
									<span>MAX BET</span>
								</div>
								<div class="play_bottom_part play_btn color-black" onclick="spin('')">
									<span>PLAY</span>
								</div>
								<div class="spin_btn" id="spin3">	
									<span><i class="fas fa-sync-alt font-size-30 color-black"></i></span>
								</div>
								<div class="autoplay_stop_btn" onclick="stop_autoplay()">
									<div class="stop_button">
										<i class="fas fa-stop font-size-20 color-black stop_play"></i>
									</div>
									<div class="remain_cnt text-center color-black"></div>
								</div>
								
								<div class="bonus_spin_cnt_section color-orange text-center">
									<span class="bonus_spin_title">FREE SPINS</span><br>
									<span class="bonus_spin_cnt"></span>
								</div>
							</div>
							<div class="footer_center bonus_wait_section">
								<div class="font-caps title">win</div>
								<span class="color-orange cost bestwager"> </span>&nbsp;<i class="color-orange cost fas fa-gem"></i>
								<span class="color-orange bonus_wait">
									FREE SPINS <span class="color-active">IN PROGRESS<br>PLEASE WAIT...</span>
								</span>
							</div>
						</div>
						<div class="flexible bodypart-side footer-3">
							<div class="font-caps title">gems</div>
							<span class="cost totalgems"></span>&nbsp;<i class="fas fa-gem cost color-orange"></i>
							
							<div class="flexible mobile-spin-section">
								<div class="mobile-spin-side">
									<i class="fas fa-gem"></i>
								</div>
								<div style="width:50%;">
									<div class="spin_btn_mobile" onclick="spin('')">
										<a class="a_black"><i class="fas fa-sync-alt "></i></a>
									</div>
								</div>
								<div class="mobile-spin-side">
									<i class="far fa-play-circle mobile-spin-side"></i>
								</div>
							</div>
						</div>
					</div>
					
					<!-- Layout 3 -->
					<div class="flexible footer layout_3">
						<div class="flexible footer-1">
							<div class="autoplay_spin">
								<li class="text-center color-black" onclick="sel_autoplay_spin(10)">10</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(25)">25</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(50)">50</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(100)">100</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(250)">250</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(500)">500</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(1000)">1,000</li>
								<li class="text-center color-black" onclick="sel_autoplay_spin(10000)">10,000</li>
							</div>
							<div class="play_bottom_part max_bet_btn color-black" onclick="spin('max')">
								<span>MAX BET</span>
							</div>
							<div class="play_bottom_part play_btn color-black" onclick="spin('')">
								<span>PLAY</span>
							</div>
							<div class="spin_btn"  id="spin5">	
								<span><i class="fas fa-sync-alt font-size-30 color-black"></i></span>
							</div>
							<div class="autoplay_stop_btn" onclick="stop_autoplay()">
								<div class="stop_button">
									<i class="fas fa-stop font-size-20 color-black stop_play"></i>
								</div>
								<div class="remain_cnt text-center color-black"></div>
							</div>
							
							<div class="bonus_spin_cnt_section color-orange text-center">
								<span class="bonus_spin_title">FREE SPINS</span><br>
								<span class="bonus_spin_cnt"></span>
							</div>
						</div>
						<div class="flexible footerbody">
							<div class="footer_center">
								<div class="font-caps title">gems</div>
								<span class="cost totalgems"></span>&nbsp;<i class="fas fa-gem cost color-orange"></i>
							</div>
							<div class="footer_center bonus_wait_section">
								<div class="font-caps title">win</div>
								<span class="color-orange cost bestwager"> </span>&nbsp;<i class="color-orange cost fas fa-gem"></i>
								<span class="color-orange bonus_wait">
									FREE SPINS <span class="color-active">IN PROGRESS<br>PLEASE WAIT...</span>
								</span>
							</div>
							<div class="footer_center padding_right_30 curwager-section bonus_wait_section">
								<div class="wager_amount" style="position: absolute;">
									<div class="font-caps title">wager</div>
									<span class="curwager curwager_desktop">300</span>&nbsp;<i class="fas fa-gem cost color-orange"></i>
								</div>
								<div class="wager_set">
									<div class="wager_up" onclick="wager_count(1)"><i class="fas fa-caret-up"></i></div>
									<div class="wager_down" onclick="wager_count(0)"><i class="fas fa-caret-down"></i></div>
								</div>
								<span class="color-orange bonus_wait1">
									FREE SPINS <span class="color-active">IN PROGRESS<br>PLEASE WAIT...</span>
								</span>
							</div>
						</div>
						<div class="flexible bodypart-side footer-3">
							
							<div class="footer-icon" onclick="change_layout()">
								<i class="fas fa-exchange-alt font-size-30"></i>
							</div>
							<div class="footer-icon volume_section" onclick="volume()">
								<i class="fas fa-volume-up font-size-30"></i>
							</div>
							<div class="footer-icon" onclick="open_infomodal()">
								<i class="fas fa-info font-size-30" style="cursor: pointer;"></i>
							</div>
							<div class="flexible mobile-spin-section">
								<div class="mobile-spin-side">
									<i class="fas fa-gem"></i>
								</div>
								<div style="width:50%;">
									<div class="spin_btn_mobile" onclick="spin('')">
										<a class="a_black"><i class="fas fa-sync-alt "></i></a>
									</div>
								</div>
								<div class="mobile-spin-side">
									<i class="far fa-play-circle mobile-spin-side"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			<div class="left-side-mobile">
				<ul class="nav nav-tabs" style="background-color: #1C2127;width: 100%;display: table;text-align: center;">	
					<li class="nav-item">
						<div data-target="#social" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-gamepad font-size-20"></i><br/>
						</div>
					</li>
					<li class="nav-item">
						<div data-target="#rooms" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-trophy font-size-20"></i><br/>
						</div>
					</li>
					<li class="nav-item">
						<div data-target="#social" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-tasks font-size-20"></i><br/>
						</div>
					</li>
					<li class="nav-item">
						<div data-target="#social" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-users font-size-20"></i><br/>
						</div>
					</li>
					<li class="nav-item">
						<div data-target="#rooms" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							<i class="fas fa-door-open font-size-20"></i><br/>
						</div>
					</li>
					<li class="nav-item" style="line-height: 18px;">
						<div data-target="#rooms" data-toggle="tab" class="nav-link small color-inactive text-center" style="font-weight: 500;">
							English x
						</div>
					</li>
				</ul>
				<div class="tab-content chatbox pt-2 pb-2 small">
					<div id="rules1" class="tab-pane fade"></div>
					<div id="rooms1" class="tab-pane fade"></div>
					<div id="chat1" class="tab-pane fade active show" style="font-weight: 500;"></div>
					<div id="social1" class="tab-pane fade"></div>
				</div>
				<div id="chatbox" class="m-2 p-2">
					<textarea type="text" rows="1" class="color-active chattext chattext1" placeholder="Type here..." autocomplete="off"  style="resize:none;"></textarea>
					<ul class="nav navbar-nav chat_emo">
						<li class="dropdown">
							<a class="dropdown-toggle" style="color: #F8BF60;cursor:pointer;" onclick="show_brag_menu()"><i class="far fa-gem"></i></a>
						</li>
						<li class="dropdown" style="margin-left: 6px;">
							<a class="dropdown-toggle" data-toggle="dropdown" style="color: #F8BF60;"><i class="far fa-grin"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<!--/div-->
	</div>
</section>

<!-- Preloader -->
<div id="preloader">
	<span></span>
</div>

<!-- Help modal -->
<div class="modal fade" id="helpmodal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content" role="tablist" aria-multiselectable="true">
			<div class="modal-header mb-2">
				<ul class="nav nav-tabs help">
					<li class="nav-item font-caps ">
						<div data-target="#help_section" id="help_menu" class="nav-link color-help-menu" data-toggle="tab">
							<i class="fas fa-question"></i> Help
						</div>
					</li>
					<li class="nav-item font-caps">
						<div data-target="#provably_section" id="provably_menu" class="nav-link color-help-menu" data-toggle="tab">
							<i class="fas fa-balance-scale"></i> PROVABLY FAIR
						</div>
					</li>
				</ul>
			</div>
			<div class="modal-body scrollable p-0">
				<div id="help_section" class="tab-pane fade active show">
					<div class="help_title text-center" role="tab" id="q1">
						<a class="color-orange" data-toggle="collapse" data-parent="#helpmodal" href="#q1_1" aria-expanded="false" aria-controls="q1_1,q1_1_1">
					        <div>Top 5 Questions <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
					    </a>
					</div>
					
					<div id="q1_1" class="collapse help_sub" role="tabpanel" aria-labelledby="q1" data-parent="#q1">
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q1" href="#q1_1_1" aria-expanded="false" aria-controls="q1_1_1">
						        <div>How do I deposit money on RussianRoulette.gg? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q1_1_1" class="collapse"  role="tabpanel" aria-labelledby="q1_1" data-parent="#q1_1">
							<div class="help_sub_desc text-left">
								Click on DEPOSIT at the top of the screen or in the top right menu, select the desired deposit method and follow the instructions. on RussianRoulette.gg you can deposit currently deposit using Bitcoin (BTC) or VGO skins, more will be added in the future!
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q1" href="#q1_1_2" aria-expanded="false" aria-controls="q1_1_2">
						        <div>I have made a withdrawal on RussianRoulette.gg. When will I receive my money? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q1_1_2" class="collapse"  role="tabpanel" aria-labelledby="q1_1" data-parent="#q1_1">
							<div class="help_sub_desc text-left">
								Withdrawal times are usually instant, however depending on the servers, network, the blockchain network or other issues, it may take up to 5 minutes for the transaction to be fully completed, if you face any issues please contact support and we will be happy to assist you!
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q1" href="#q1_1_3" aria-expanded="false" aria-controls="q1_1_3">
						        <div>What welcome bonus do I get on RussianRoulette.gg? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q1_1_3" class="collapse"  role="tabpanel" aria-labelledby="q1_1" data-parent="#q1_1">
							<div class="help_sub_desc text-left">
								On RussianRoulette.gg we give a very generous welcome bonus which gives new users a total of $100 extra and 30 free spins, if you use it to its full potential. It works like this:
	When				Bonus
	Directly on registration	30 Free spins
	1:st deposit			100% deposit bonus (max bonus $100)
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q1" href="#q1_1_4" aria-expanded="false" aria-controls="q1_1_4">
						        <div>How do I activate my spins? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q1_1_4" class="collapse"  role="tabpanel" aria-labelledby="q1_1" data-parent="#q1_1">
							<div class="help_sub_desc text-left">
								To activate your spins – go to your rewards (select ‘Rewards’ at the top of the page, or in the menu on the top right). Choose to use the free spins and you will be taken to the game where you can play as normal and see how many free spins you have remaining in the balance area. Make sure you take note of any restrictions or wagering requirements the spins may have.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q1" href="#q1_1_5" aria-expanded="false" aria-controls="q1_1_5">
						        <div>How do i level up? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q1_1_5" class="collapse"  role="tabpanel" aria-labelledby="q1_1" data-parent="#q1_1">
							<div class="help_sub_desc text-left">
								Every time you play on RussianRoulette.gg you collect experience points that will take you to new levels in the adventure. Your progress bar will display how far you are, and for each new level you achieve you will win great rewards which increase in value and rarity such as free spins and free money!
							</div>
						</div>
						
					</div>
					
					
					<div class="help_title text-center" role="tab" id="q2">
						<a class="color-orange" data-toggle="collapse" data-parent="#helpmodal" href="#q2_1" aria-expanded="false" aria-controls="q2_1">
					        <div>Customer Support <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
					    </a>
					</div>
					
					<div id="q2_1" class="collapse help_sub" role="tabpanel" aria-labelledby="q2" data-parent="#q2">
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q2" href="#q2_1_1" aria-expanded="false" aria-controls="q2_1_1">
						        <div>When is the customer support available? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q2_1_1" class="collapse"  role="tabpanel" aria-labelledby="q2_1" data-parent="#q2_1">
							<div class="help_sub_desc text-left">
								RussianRoulette.gg staff are answering all your enquiries every day of the week 24/7. Just send an email to support@russianroulette.gg and we’ll get back to you as soon as we can.
							</div>
						</div>
					</div>
					
					
					<div class="help_title text-center" role="tab" id="q3">
						<a class="color-orange" data-toggle="collapse" data-parent="#helpmodal" href="#q3_1" aria-expanded="false" aria-controls="q3_1">
					        <div>Account <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
					    </a>
					</div>
					
					<div id="q3_1" class="collapse help_sub" role="tabpanel" aria-labelledby="q3" data-parent="#q3">
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q3" href="#q3_1_1" aria-expanded="false" aria-controls="q3_1_1">
						        <div>How do I create an account on RussianRoulette.gg? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q3_1_1" class="collapse"  role="tabpanel" aria-labelledby="q3_1" data-parent="#q3_1">
							<div class="help_sub_desc text-left">
								Select “sign up” in the main menu and follow the instructions.You should be completed in just one click. Easy as pie!
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q3" href="#q3_1_2" aria-expanded="false" aria-controls="q3_1_2">
						        <div>I cannot open an account. What is wrong? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q3_1_2" class="collapse"  role="tabpanel" aria-labelledby="q3_1" data-parent="#q3_1">
							<div class="help_sub_desc text-left">
								Make sure that all fields are correctly filled in. Is the username you chose available? If the email address or username is already registered on RussianRoulette.gg it means you already have an account with us. Go to login and select “I forgot my password” or contact us directly and we will reactivate the account for you.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q3" href="#q3_1_3" aria-expanded="false" aria-controls="q3_1_3">
						        <div>How do I turn off my subscription to bonuses and promotions? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q3_1_3" class="collapse"  role="tabpanel" aria-labelledby="q3_1" data-parent="#q3_1">
							<div class="help_sub_desc text-left">
								Click on "Account" (Also available in the top right menu on certain resolutions) and here you can turn on / off your subscription to bonuses and promotions.
							</div>
						</div>
					</div>
					
					
					<div class="help_title text-center" role="tab" id="q4">
						<a class="color-orange" data-toggle="collapse" data-parent="#helpmodal" href="#q4_1" aria-expanded="false" aria-controls="q4_1">
					        <div>Deposits <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
					    </a>
					</div>
					
					<div id="q4_1" class="collapse help_sub" role="tabpanel" aria-labelledby="q4" data-parent="#q4">
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q4" href="#q4_1_1" aria-expanded="false" aria-controls="q4_1_1">
						        <div>How do I deposit money on RussianRoulette.gg? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q4_1_1" class="collapse"  role="tabpanel" aria-labelledby="q4_1" data-parent="#q4_1">
							<div class="help_sub_desc text-left">
								Click on DEPOSIT at the top of the screen or in the top right menu, select the desired deposit method and follow the instructions. on RussianRoulette.gg you can deposit currently deposit using Bitcoin (BTC) or VGO skins, more will be added in the future!
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q4" href="#q4_1_2" aria-expanded="false" aria-controls="q4_1_2">
						        <div>I deposited but the funds did not show in my RussianRoulette account. What do I do? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q4_1_2" class="collapse"  role="tabpanel" aria-labelledby="q4_1" data-parent="#q4_1">
							<div class="help_sub_desc text-left">
								Make sure all the information is filled out correctly. Are all the characters in the wallet address correct? Are you using an accepted currency and value? If these questions haven't helped you solve the problem yet, no need to worry. First make sure that the funds were deducted from your wallet/account and then contact us. Most likely we will be able to trace and solve the problem with your username and link to the transaction to be able to investigate and fix the problem quickly.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q4" href="#q4_1_3" aria-expanded="false" aria-controls="q4_1_3">
						        <div>I have made a deposit on RussianRoulette.gg. When are the funds available? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q4_1_3" class="collapse"  role="tabpanel" aria-labelledby="q4_1" data-parent="#q4_1">
							<div class="help_sub_desc text-left">
								Deposits are creditted instantly as soon as the transaction if available on the network, sometimes the network takes longer (the bitcoin network takes on average 10 minutes from the time of transaction. If there are any problems with a deposit, please contact support and we will help you fix the issue.
							</div>
						</div>
					</div>
					
					
					<div class="help_title text-center" role="tab" id="q5">
						<a class="color-orange" data-toggle="collapse" data-parent="#helpmodal" href="#q5_1" aria-expanded="false" aria-controls="q5_1">
					        <div>Bonus & Free Spins <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
					    </a>
					</div>
					
					<div id="q5_1" class="collapse help_sub" role="tabpanel" aria-labelledby="q5" data-parent="#q5">
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_1" aria-expanded="false" aria-controls="q5_1_1">
						        <div>What is a bonus? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
						    </a>
						</div>
						<div id="q5_1_1" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								A bonus on RussianRoulette.gg can mean rewards such as wager free spins, free spins, bonus spins, bonus money or gems. The type of Bonus and the size of it varies as do the requirements and restrictions attached to the Bonus. Should there be restrictions or requirements for the Bonus, these will be highlighted to you when you get the Bonus. A Bonus may be convertible to gems in your Account (and available for withdrawal), subject to you fulfilling a specific set of terms and conditions stipulated for that Bonus.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_2" aria-expanded="false" aria-controls="q5_1_2">
						        <div>When do I get bonuses on RussianRoulette.gg? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_2" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								on RussianRoulette.gg you can get a bonus in many different ways. Besides the welcome bonus you receive upon joining and weekly new bonus promotions, you can get lots of bonuses simply by playing games and climbing in levels. You will receive get a bonus when you level up, and sometimes you have a bunch of free spins waiting for you when you log in. Make sure you are subscribing to our offers and news through email to not miss out.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_3" aria-expanded="false" aria-controls="q5_1_3">
						        <div>What welcome bonus do I get on RussianRoulette.gg? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_3" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								On RussianRoulette.gg we give a very generous welcome bonus which gives new users a total of $100 extra and 30 free spins, if you use it to its full potential. It works like this:
	When				Bonus
	Directly on registration	30 Free spins
	1:st deposit			100% deposit bonus (max bonus $100)
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_4" aria-expanded="false" aria-controls="q5_1_4">
						        <div>How do I get my welcome bonus? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_4" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								Once you have registered you will get a reward in your ‘rewards’ page. If you received a deposit bonus you have to choose to use the valuable before you deposit. Once your deposit is made, the bonus will automatically appear in your account.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_5" aria-expanded="false" aria-controls="q5_1_5">
						        <div>What are free spins? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_5" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								Free Spins are spins rewarded to you (somtimes for a specific game). These spins have Wagering Requirements of 30x. Bonus Gems will convert automatically to Gems once the Wagering Requirement is completed. This means that all winnings made on these spins are converted to Bonus Gems which will need to be wagered 30x. Free spins must be activated through the account, when you are logged in. Once these are activated, the Free Spins are set off and the game will start automatically.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_6" aria-expanded="false" aria-controls="q5_1_6">
						        <div>How do I activate my free spins? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_6" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								To activate your free spins – go to your rewards (find it at the top or the page or in the top right menu). Choose to use the free spins and and the game will start automatically.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_7" aria-expanded="false" aria-controls="q5_1_7">
						        <div>Do all games count towards the wagering requirements? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_7" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								Currently all games count towards the wagering requirements
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_8" aria-expanded="false" aria-controls="q5_1_8">
						        <div>What are Bonus Spins? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_8" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								Bonus Spins are spins that you will be rewarded after you have made a deposit of a certain stipulated amount that we will tell you beforehand. Winnings on Bonus Spins are converted to Bonus Gems and are usually subject to a Wagering Requirement of 30 times. Bonus Money will convert automatically to gems once the Wagering Requirement is completed. Bonus Spins must be activated through the account, when you are logged in. Once these are activated, the Bonus Spins are set off and the particular game will start automatically.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_9" aria-expanded="false" aria-controls="q5_1_9">
						        <div>How does the bonus turnover work on RussianRoulette.gg? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_9" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								Only bonus gems wagered will count towards the bonus Wager Requirements of Bonus Gems. If you wager 1,000 in bonus gems, 1,000 gems will be counted towards the Wager Requirements .If you get several bonuses at the same time the bonus money and the wagering requirements are bundled.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q5" href="#q5_1_10" aria-expanded="false" aria-controls="q5_1_10">
						        <div>What are Wager-Free Spins? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q5_1_10" class="collapse"  role="tabpanel" aria-labelledby="q5_1" data-parent="#q5_1">
							<div class="help_sub_desc text-left">
								Wager Free Spins are spins rewarded to be used on a specific game. These spins have no Wagering Requirements and do not require a deposit. Wager Free Spins must be activated through the account, when you are logged in. Once these are activated, the Wager Free Spins are set off and the particular game will start automatically.
							</div>
						</div>
					</div>
					
					
					<div class="help_title text-center" role="tab" id="q6">
						<a class="color-orange" data-toggle="collapse" data-parent="#helpmodal" href="#q6_1" aria-expanded="false" aria-controls="q6_1">
					        <div>Technical Questions <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
					    </a>
					</div>
					
					<div id="q6_1" class="collapse help_sub" role="tabpanel" aria-labelledby="q6" data-parent="#q6">
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q6" href="#q6_1_1" aria-expanded="false" aria-controls="q6_1_1">
						        <div>Games are slow. What’s wrong? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q6_1_1" class="collapse"  role="tabpanel" aria-labelledby="q6_1" data-parent="#q6_1">
							<div class="help_sub_desc text-left">
								Make sure you have the latest version of your browser installed (we recommend Google Chrome). Also make sure that it’s not your internet connection that is the problem (try to play with the network cable if the wireless connection is not working like it should) and close any background programs that might drain your bandwidth.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q6" href="#q6_1_2" aria-expanded="false" aria-controls="q6_1_2">
						        <div>What happens if the game freezes in the middle of a round? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q6_1_2" class="collapse"  role="tabpanel" aria-labelledby="q6_1" data-parent="#q6_1">
							<div class="help_sub_desc text-left">
								If your game freezes in solo, you will end up exactly where you left off next time you open the game. If there is no option for you to do so, the round will be completed on the server even if your computer is frozen or your internet connection stopped working. Group rounds are always completed on the server. Any winnings are paid out to your account as usual.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q6" href="#q6_1_3" aria-expanded="false" aria-controls="q6_1_3">
						        <div>I cannot open the game. What is wrong? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q6_1_3" class="collapse"  role="tabpanel" aria-labelledby="q6_1" data-parent="#q6_1">
							<div class="help_sub_desc text-left">
								This may mean that you have lost contact with the server. Try refreshing the page, or try to log out and log back in and try again. Sometimes you may need to shut down the browser and start over from scratch for the game to load. Contact support if problem still persists.
							</div>
						</div>
						
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q6" href="#q6_1_4" aria-expanded="false" aria-controls="q6_1_4">
						        <div>How do I take a screenshot? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q6_1_4" class="collapse"  role="tabpanel" aria-labelledby="q6_1" data-parent="#q6_1">
							<div class="help_sub_desc text-left">
								<span>Taking a screenshot means taking a photo of what is currently visible on your screen . The image is saved as a file on your mobile and can easily be attached to an email and sent to us. This is how you take a screenshot in iOS (for an iPhone)<br></span>

	<span>Step 1: Make sure that the current view on the screen is what you want to take a screenshot of.<br></span>

	<span>Step 2: Press the phone’s home screen button and the top button (the one used for locking / power) simultaneously<br></span>

	<span>Step 3: Go to your photos, you should see the screenshot saved in your camera roll<br></span>

	<span>Step 4: Send us the screenshot in an email<br></span><br>
							
						        <span>Instructions for android phones<br></span><br>
						    
								<span>Step 1: Make sure that the current view on the screen is what you want to take a screenshot of.<br></span>

								<span>Step 2: Press the phone’s power button and the volume button simultaneously, until you hear a click or screenshot sound.<br></span>

								<span>Step 3: Go to your gallery, you should see the screenshot saved<br></span>

								<span>Step 4: Send us the screenshot in an email<br></span>
							</div>
						</div>
					</div>
					
					
					<div class="help_title text-center" role="tab" id="q7">
						<a class="color-orange" data-toggle="collapse" data-parent="#helpmodal" href="#q7_1" aria-expanded="false" aria-controls="q7_1">
					        <div>The Adventure <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i></div>
					    </a>
					</div>
					
					<div id="q7_1" class="collapse help_sub" role="tabpanel" aria-labelledby="q7" data-parent="#q7">
						<div class="help_sub_title color-orange">
							<a class="color-orange" data-toggle="collapse" data-parent="#q7" href="#q7_1_1" aria-expanded="false" aria-controls="q7_1_1">
						        <div>How do i level up? <i class="fas fa-angle-down rotate-icon color-orange help_collap_icon"></i>    </div>
						    </a>
						</div>
						<div id="q7_1_1" class="collapse"  role="tabpanel" aria-labelledby="q7_1" data-parent="#q7_1">
							<div class="help_sub_desc text-left">
								Every time you play on RussianRoulette.gg you collect experience points that will take you to new levels in the adventure. Your progress bar will display how far you are, and for each new level you achieve you will win great rewards which increase in value and rarity such as free spins and free money!
							</div>
						</div>
					</div>
				</div>
				
				<div id="provably_section" class="tab-pane fade">
					<div class="prob_menu_section">
						<ul class="nav nav-tabs help">
							<li class="nav-item">
								<div data-target="#exp_section" class="nav-link active prob_menu" data-toggle="tab">
									Explanation
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#result_section" class="nav-link prob_menu" data-toggle="tab">
									Result Verification Calculator
								</div>
							</li>
						</ul>
					</div>
					<div id="exp_section" class="tab-pane fade active show">
						<div class="prob_title color-orange">Explanation of Provably Fair (Group)</div>
						<div class="prob_desc">
							<label>We have generated a chain of 100 million SHA256 hashes, starting with a server secret that has been repeatedly fed the output of SHA256 back into itself 100 million times.<br>
The game server is playing through this chain of 100 million SHA256 hashes in reverse order, using the values as the source data for the game outcome calculation function.<br></label>

<label>Anyone can easily verify the integrity of the chain as the hashes used to calculate each
round’s outcome are automatically published after each round start (<span class="color-orange">PREVIOUS ROUNDS</span>).
If you apply a SHA256 function to the round’s hash, you will get hash for previously played round, and then again if you apply a SHA256 function to that round’s hash, you will get the hash for the next previously played game, and so on until you get the hash for the first ever played game round.</label>

<label>The bottom line is that the results for all future games are already randomly generated and randomly distributed due to math that backs up a strong cryptographic SHA256 function.<br></label>

<label>It is IMPOSSIBLE for any party to manipulate this chain of results and we publish each game’s results one by one as they are played so anyone can verify the results of the game.<br></label>

<label>Each potential player can see history of the games and choose when to participate in new game round at any time whenever the player wants to.<br></label>

<label>As results are already predefined and we can't force any player to join the game, every single game becomes a 100% secure and provably fair game of luck.<br></label>

For even more information you can read an explanation on Wikipedia:<br>
<span class="color-orange">https://www.wikipedia.org/wiki/Provably_fair</span><br>
<span class="color-orange">https://www.wikipedia.org/wiki/Secure_Hash_Algorithms</span><br>
<span class="color-orange">https://www.wikipedia.org/wiki/SHA-2</span><br>
						</div>
						<div class="prob_title color-orange">Hash Integrity Verification (First ever Game Hashes)</div>
						<div class="prob_desc">
							<label>By publicating the group version (solo versions do not use a chain of hashes, it generates a new verifiable hash every single round) of each game’s first ever game hashes, we are 
preventing any ability to pick an alternate SHA256 chain or modify parts of it from our side, 
as this would cause a change in each hash from the chain and any person would be able to check if the chain has been modified or changed in any way.</label>

Game: Russian Roulette (Group)<br>
<span style="word-break: break-all;">Hash: 68DE9C6B2B73D14D01954A70E694A7D99B45731454EADC23688C3504B241CD27</span><br>
Archive Verification Link: https://www.archive.is/86208372<br>
						</div>
						<div class="prob_title color-orange">Explanation of Provably Fair (Solo)</div>
						<div class="prob_desc">
							<label>For each verifiable bet, a client seed, a server seed and a nonce are used as the input parameters for the random number generator. Bytes are generated using the HMAC_SHA256 function. This function creates 32 random bytes (0-255) from a given server seed and a message. The message is created using the client seed, the nonce and an incremental 
number (round). This number starting with 0 gets increased by one every time the 32 bytes returned by the HMAC_SHA256 function are used. The message is then created by concatenating the client seed, a colon, the nonce, another colon and the incremental 
number.<br></label>
							<span class="color-active">Client Seed</span><br>
							<label>This is a passphrase or a randomly generated selection of text that is determined by the player or their browser. This can be edited and changed regularly (if desired) to create a new chain of randomness.<br></label>
							<span class="color-active">Server Seed</span><br>
							<label>The server seed is generated by our system as a random 64-character hex string. You will 
then be provided with an encrypted hash of the server seed before you place any bets. The seed is encrypted to ensure that the outcome cannot be calculated by the player 
beforehand, but still provide the guarantee that the outcome has been pre-determined and not changed after the bet has been made.<br></label>
							<label>To reveal the hashed server seed, the seed must be rotated by the player,which triggers the replacement with a newly generated seed. From this point you are able to verify any bets made with the previous server seed to verify both the legitimacy of the server seed with the encrypted hash that was provided, and the legitimacy of each outcomes of each bet when it was active.<br></label>
							<span class="color-active">Nonce</span><br>
							<label>The nonce is simply a number that increments as every new bet is made. This generates a unique input per game, without having to generate a new client seed and server seed each time you play.<br></label>
						</div>
					</div>
					<div id="result_section" class="tab-pane fade">
						<div class="text-center prob_desc">
							<div class="result_main_part">
								<div class="result_title text-center">
								    <div style="text-align: left;">Game <i class="fas fa-angle-down rotate-icon help_collap_icon"></i></div>
								</div>
								<div class="result_title text-center">
								    <div style="text-align: left;">Version <i class="fas fa-angle-down rotate-icon help_collap_icon"></i></div>
								</div>
								<div class="result_title text-center">
								    <div style="text-align: left;">Hash</div>
								</div>
								<div class="result_title text-center">
								    <div style="text-align: left;">Seed</div>
								</div>
								<div class="shoot_part text-center">
									<label class="color-active single_normal">Single</label><br>
									<label class="color-orange center_result">x10.25</label><br>
									<label class="color-active single_normal">Normal</label><br>
								</div>
								<div class="seed_detail">
									<span>Final Result</span><br>
									<span class="color-orange">Fire</span><span class="color-active">,</span><span class="color-orange"> x10.25</span><span class="color-active">,Single, Normal</span><br>
									<span>Seeds to Bytes</span><br>
									<span class="color-active">HMAC_SHA256(serverseed, :23:0)</span><br>
									<span>Bytes to Numbers</span><br>
								</div>
								<table class="seed_calc_table">
									<tr>
										<td></td>
										<td colspan="2">(168, 204, 187, 39) -> [0, ..., 99999999] = 65937394</td>
									</tr>
									<tr>
										<td></td>
										<td>0.656250000000</td>
										<td>(168 / (256^1))</td>
									</tr>
									<tr>
										<td>+</td>
										<td>0.003112792969</td>
										<td>(204 / (256^2))</td>
									</tr>
									<tr>
										<td>+</td>
										<td>0.000011146069</td>
										<td>(187 / (256^3))</td>
									</tr>
									<tr>
										<td>+</td>
										<td>0.000000009080</td>
										<td>(039 / (256^4))</td>
									</tr>
									<tr>
										<td>=</td>
										<td>0.659373948118</td>
										<td>(* 100000000)</td>
									</tr>
									<tr>
										<td>=</td>
										<td colspan="2">65937394.811771810055</td>
									</tr>
								</table>
								<table class="version_table">
									<tr>
										<td>Versions</td>
										<td>Date</td>
										<td>Link</td>
									</tr>
									<tr>
										<td class="color-orange">v10</td>
										<td class="color-active">01/01/01 01:01:01</td>
										<td class="color-orange underline">Verifier</td>
									</tr>
									<?php for($i = 9;$i >=1;$i--){ ?>
										<tr>
											<td class="color-active">v<?php echo $i;?></td>
											<td class="color-active">01/01/01 01:01:01</td>
											<td class="color-orange underline">Verifier</td>
										</tr>	
									<?php } ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<a data-dismiss="modal" class="modal_close_btn">Close</a>
			</div>
		</div>
	</div>
</div>

<!-- Information modal -->
<div class="modal fade" id="informationmodal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-caps">GAME INFO</h5>
			</div>
			<div class="modal-body scrollable p-0">
				<div class="modal-text">
					<span style="font-weight: bold;">HOW TO PLAY?</span>
					<p class="mb-0 pt-2" style="color: #A3A09B;">To play a game, press the PLAY button. Alternatively press the keyboard SPACE BAR.<br>
						To increase or decrease a wager amount, press the up arrow or down arrow next to the wager number.</p>
				</div>
				<div class="modal-text">
					<span style="font-weight: bold;">HOW TO WIN?</span>
					<p class="mb-0 pt-2" style="color: #A3A09B;">When the REVOLVER fires a shot, a WIN occurs. All players that in the round will have their wager multiplied by the MULTIPLIER displayed above the gun. After any additional WINS and MULTIPLIERS, the players will receive the TOTAL WIN AMOUNT.</p>
				</div>
				<div class="modal-text">
					<span style="font-weight: bold;">AUTOPLAY</span>
					<p class="mb-0 pt-2" style="color: #A3A09B;">To play a number of games in succession, press the AUTOPLAY button, then select the number of games to play.<br>
To cancel AUTOPLAY at any time, press the STOP AUTOPLAY button.</p>
				</div>
				<div class="modal-text">
					<span style="font-weight: bold;">QUICKPLAY (solo only)</span>
					<p class="mb-0" style="color: #A3A09B;">To skip certain animations during a game, press the SKIP button if available.<br>
Alternatively, press the keyboard SPACE BAR.
					</p>
				</div>
				<div class="modal-text">
					<span style="font-weight: bold;">FREE GAMES FEATURE</span>
					<p class="mb-0 pt-2" style="color: #A3A09B;">
						If the BONUS ROLL rolls a BONUS, the players will receive a random amount of free spins. The player can win up to 3x the
maximum amount of free spins if the BONUS ROLL rolls “BONUS + 3x FREE SPINS”.<br>
If the BONUS is won, the game will play through the amount of FREE SPINS rounds won, automatically or manually (SOLO).<br>
The BONUS will have an additional BONUS MULTIPLIER, which will be multiplied with the NORMAL MULTIPLIER to give a huge TOTAL MULTIPLIER during the BONUS rounds.
At the end of the BONUS, the TOTAL WIN AMOUNT will be totalled up, displayed and given to all players that won the BONUS.
					</p>
				</div>
				<div class="modal-text">
					<span style="font-weight: bold;">DOUBLE GUN FEATURE</span>
					<p class="mb-0 pt-2" style="color: #A3A09B;">Before the REVOLVER fires, a second REVOLVER can appear, then each REVOLVER will fire separately. If both REVOLVERS fire then the WIN MULTIPLIER displayed above will be DOUBLED!
					</p>
				</div>
				<div class="modal-text">
					<span style="font-weight: bold;">RANDOMIZATION/FAIRNESS</span>
					<p class="mb-0 pt-2" style="color: #A3A09B;">EVERY game’s result is generated with a FAIR and equal chance of a win. The game’s results can NEVER be manipulated or tampered with by ANY person.<br>
ANY individual can VERIFY that each game is fair for themselves by checking that the hash of the previous game is the SHA256 hash of the game before that.<br>
For more information go to russianroulette.gg/faq</p>
					<table class="info_modal_table">
						<thead>
							<tr style="border-bottom: 1px solid white;">
								<th style="width:25%;padding-left: 10px;">ITEM</th>
								<th style="width:75%;">FUNCTION</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><div class="modal_play_btn">PLAY</div></td>
								<td>Click to start a game round with the current wager(alternatively press the SPACE BAR)</td>
							</tr>
							<tr>
								<td><div class="modal_autoplay_btn"><i class="fas fa-sync-alt" style="color:black;"></i></div></td>
								<td>Click to select the number of spins to AUTOPLAY and play the game automatically.</td>
							</tr>
							<tr>
								<td class="text-center"><i class="fas fa-question color-active"></i></td>
								<td>Click to view information about the game.</td>
							</tr>
							<tr>
								<td class="text-center"><i class="fas fa-volume-up color-active"></i></td>
								<td>Click to mute the game sounds or use the slider to adjust the sound volume.</td>
							</tr>
							<tr>
								<td class="text-center"><i class="fas fa-exchange-alt color-active"></i></td>
								<td>Click to change the layout of the game</td>
							</tr>
						</tbody>
					</table>
					
				</div>
				<div class="modal-text">
					<p class="mb-0 pt-2" style="color: #A3A09B;"> In the event of malfunction of gaming hardware/software, all affected game bets and payouts are rendered void and all affects bets refunded.<br>
 
Version 1.0.00 01-01-2000
					</p>
				</div>
			</div>
			<div class="modal-footer text-center">
				<a data-dismiss="modal" class="modal_close_btn">Close</a>
			</div>
		</div>
	</div>
</div>


<!-- Reward modal -->
<div class="modal fade" id="rewardmodal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header mb-2">
				<h5 class="modal-title font-caps">REWARDS</h5>
			</div>
			<div class="modal-body scrollable p-0">
				<div class="flexible">
					<div class="reward-section reward-desc">
						<label style="color:white;">EARN FREE <i class="fas fa-gem color-orange"></i> BY SHARING YOUR CODE!</label>
						<p class="mb-0" style="color: #A3A09B;">When people sign up through your link, they become your referral!</p>
						<p class="mb-0" style="color: #A3A09B;">New users will receive a <span style="color:#f8bf60;">100%</span> deposit bonus on their first deposit</p>
						<p class="mb-0" style="color: #A3A09B;">You will receive free gems depending on your affiliate rank.</p>
						<p class="mb-0 color-orange font-caps">YOU WILL RECEIVE GEMS EVERYTIME ANY OF YOUR REFERALS BET, ALWAYS EVEN IF THEY WIN OR LOSE!</p>
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="reward-section" style="background: none;">
						<div class="reward-compain-section1 mr-1 pb-3">
							<label style="color:white;">CREATE YOUR OWN CAMPAIGN!</label><br>
							<input type="text" placeholder="Enter your Campaign Name..." class="reward-campaign-input">
							<input type="text" placeholder="Enter your Campaign Code..." class="reward-campaign-input reward-campaign-input-r"><br>
							<a class="color-active font-caps reward_mobile_camp create_camp">CREATE CAMPAIGN</a>
							<a class="color-active font-caps reward_mobile_camp reward_download_banner">DOWNLOAD BANNERS</a>
						</div>
						<div class="reward-compain-section2 ml-1 pb-3">
							<label style="color:white;">152,000 <i class="fas fa-gem color-orange"></i></label>
							<p class="font-caps color-inactive">YOUR EARNINGS</p>
							<a class="font-caps collect_comission">COLLECT COMISSION</a>
						</div>
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="reward-section">
						<ul class="nav nav-tabs text-center" style="width: 100%;display:table">
							<li class="nav-item">
								<div data-target="#campaign" data-toggle="tab" class="nav-link color-inactive text-center show active" style="font-weight: 500;">
									Campaigns
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#analytics" data-toggle="tab" class="nav-link color-inactive text-center" style="font-weight: 500;">
									Analytics
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#reffered_users" data-toggle="tab" class="nav-link color-inactive text-center" style="font-weight: 500;">
									Reffered Users
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#funds" data-toggle="tab" class="nav-link color-inactive text-center" style="font-weight: 500;">
									Funds
								</div>
							</li>
						</ul>
						<div class="table_section" style="width: 100%;">
							<div id="campaign" class="reward_each_menu_section tab-pane fade active show">
								<table style="width: 100%;">
									<thead>
										<tr>
											<th class="color-inactive">Campaing Name</th>
											<th class="color-inactive">Link</th>
											<th class="color-inactive">Code</th>
											<th class="color-inactive">Created</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i = 0;$i < 4;$i++){ ?>
											<tr>
												<td class="color-active">juicy</td>
												<td><input class="color-orange campaign_link" value="russianroulette.gg/?r=juicy"/><i class="fas fa-copy color-active campaign_copy"></i></td>
												<td><input class="color-orange campaign_link" value="juicy"/><i class="fas fa-copy color-active campaign_copy"></i></td>
												<td class="color-active">01/01/01</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<div id="analytics" class="reward_each_menu_section tab-pane fade">
								<table style="width: 100%;">
									<thead>
										<tr>
											<th class="color-inactive">Name</th>
											<th class="color-inactive">Hits</th>
											<th class="color-inactive">Referrals</th>
											<th class="color-inactive">Available</th>
											<th class="color-inactive">Commission</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div id="reffered_users" class="reward_each_menu_section tab-pane fade">
								<table style="width: 100%;">
									<thead>
										<tr>
											<th class="color-inactive">Campaign Name</th>
											<th class="color-inactive">Sort by</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div id="funds" class="reward_each_menu_section tab-pane fade">
								<table style="width: 100%;">
									<thead>
										<tr>
											<th class="color-inactive">Available</th>
											<th class="color-inactive">Withdrawn</th>
											<th class="color-inactive">Total Commission</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="reward-section" style="background: none;">
						<div class="" style="width:100%;text-align:center;">
							<div class="reward-earning-part mr-1">
								<label style="color:white;">6,100</label>
								<label style="color:#A3A09B;">REFERRALS</label>
							</div>
							<div class="reward-earning-part ml-1 mr-1">
								<label style="color:white;">1,525</label>
								<label style="color:#A3A09B;">DEPOSITORS</label>
							</div>
							<div class="reward-earning-part ml-1">
								<label style="color:white;">2,132,545 <i class="fas fa-gem color-orange"></i></label>
								<label style="color:#A3A09B;">TOTAL EARNINGS</label>
							</div>
						</div>
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="progress_section reward-section">
						<div class="progress">
							<div class="win_progress">
							</div>
							<div class="lose_progress">
							</div>
							<div class="progress_circle circle_1"></div>
							<div class="progress_circle circle_2"></div>
							<div class="progress_circle circle_3"></div>
							<div class="progress_circle circle_4"></div>
							<div class="progress_circle circle_5"></div>
						</div>
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="reward-section reward_mark_section">
						<div style="width: 100%;">
							<div class="reward_mark circle_1">0</div>
							<div class="reward_mark circle_2" style="color: #F8BF60;">5</div>
							<div class="reward_mark circle_3">10</div>
							<div class="reward_mark circle_4">20</div>
							<div class="reward_mark circle_6">40</div>
						</div>
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="reward-section text-center">
						<p class="font-caps color-inactive" style="margin-bottom: 0;padding: 6px 0;">UNIQUE MONTHLY DEPOSITORS</p>	
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="reward-section reward_mark_section">
						<div style="width: 100%;">
							<div class="reward_mark circle_1">5%</div>
							<div class="reward_mark circle_2" style="color: #F8BF60;">10%</div>
							<div class="reward_mark circle_3">15%</div>
							<div class="reward_mark circle_4">20%</div>
							<div class="reward_mark circle_6">25%</div>
						</div>
					</div>
				</div>
				<div class="flexible mt-2">
					<div class="reward-section text-center">
						<p class="font-caps color-inactive" style="margin-bottom: 0;padding: 6px 0;">YOUR CURRENT COMMISION</p>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<a data-dismiss="modal" class="reward_close">Close</a>
			</div>
		</div>
	</div>
</div>

<!-- Account modal -->
<div class="modal fade" id="accountmodal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" style="padding: 0;background-color: #191E24;">	
				<span class="account-header1 color-active logout font-caps" onclick="logout()"">LOGOUT</span>
				<h5 class="account-header2 modal-title font-caps"><i class="fas fa-user" style="margin-right: 5px;"></i>ACCOUNT</h5>
			</div>
			<div class="modal-body scrollable p-0">
				<div class="flexible">
					<div class="account-info">
						<label class="color-active account_modal_userid mr-1"></label>
						<label class="color-active welcome font-weight-bold">Welcome <span class="color-red" id="user_name"></span></label><br>
						<label class="color-active font-weight-bold" >Level:&nbsp;<span class="user_level"></span></label>
					</div>
				</div>
				<div class="flexible account_margin_top_10">
					<div class="account-info account_menu">
						<ul class="nav nav-tabs text-center" style="width: 100%;display:table">
							<li class="nav-item">
								<div data-target="#security" data-toggle="tab" class="nav-link color-inactive text-center show active" style="font-weight: 500;">
									Security
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#fa" data-toggle="tab" class="nav-link color-inactive text-center" style="font-weight: 500;">
									2FA
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#preference" data-toggle="tab" class="nav-link color-inactive text-center" style="font-weight: 500;">
									Preferences
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#ignored_user" data-toggle="tab" class="nav-link color-inactive text-center" style="font-weight: 500;">
									Ignored Users
								</div>
							</li>
							<li class="nav-item">
								<div data-target="#transaction" data-toggle="tab" class="nav-link color-inactive text-center" style="font-weight: 500;">
									Transactions
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="flexible account_margin_top_10">
					<div class="account-info account_mainbody">
						<div id="fa" class="tab-pane fade">
							<div style="width: 100%;">
								<p class="color-active mt-2 fa_copy_code">Copy this code to your authenticator app or scan the QR Code.</p>
								<div class="qr_code_section">
									<input type="text" readonly class="auth_code" value="1NJzVRpXq1DR8FhGoYA2qhj5LdTnhPjUaX">
									<i class="fas fa-copy color-active fa2_copy"></i>
									<div class="auth_reg_btn">
										<i class="fas fa-sync-alt color-black"></i>
									</div>
								</div>
								<p class="color-active fa_desc">Don't let anyone see this!<br>You can click the refresh button to generate new codes.</p>
								<div style="width: 100%;" class="mt-2">
									<img class="qr_image" src="<?php echo base_url();?>assets/images/qrcode.png" />
								</div>
								<div class="mt-2" style="width: 100%;">
									<div class="text-center pos-relative signup_input_section" >
										<p class="color-active inputbox_desc">2FA Code</p>
										<input type="text" class="signup_input white_background" id="fa_code" autofocus/>
									</div>
								
									<div class="text-center pos-relative signup_input_section">
										<p class="color-active inputbox_desc">Password</p>
										<input type="password" class="signup_input white_background" id="fa_pwd" />
										<label class="color-black show_pwd fa_show">Show</label>
										<i onclick="show_fa_password()" class="fas fa-eye-slash color-black signup_show_password fa_pwd"></i>
									</div>
									<div class="text-center button-second-section signup_input_section">
										<button class="color-active font-caps login_btn fa_btn">ENABLE 2FA</button>
									</div>
								</div>
							</div>
						</div>
						<div id="security" class="tab-pane fade active show">
							<div style="width: 100%;">
								<div class="text-center signup_input_section color-active" style="margin-top: 0px;">
									<p class="security_title">Secure your account</p>
								</div>
								<div class="text-center signup_input_section color-active">
									<p class="security_desc">Feel free to change your password here.<br>Be sure to enable 2FA for the highest security.</p>
								</div>
								
								<div class="text-center pos-relative signup_input_section">
									<p class="color-active inputbox_desc">Old Password</p>
									<input type="password" class="signup_input white_background" id="old_password" autofocus="autofocus"/>
								</div>
								
								<div class="text-center pos-relative signup_input_section">
									<p class="color-active inputbox_desc">New Password</p>
									<input type="password" class="signup_input white_background" name="change_new_pwd" id="change_new_pwd"/>
									<label class="color-black show_pwd security_show">Show</label>
									<i onclick="show_security_password()" class="fas fa-eye-slash color-black signup_show_password security_pwd"></i>
								</div>
								
								
								<div class="security_valid security_old_password">
									<i class="fas fa-exclamation-circle"></i> Old password is not correct
								</div>
								<div class="security_valid signup_valid_password">
									<i class="fas fa-exclamation-circle"></i> Please use 8 or more characters
								</div>
								<div class="security_valid signup_valid_password_strong">
									<i class="fas fa-exclamation-circle"></i> Please use a stronger password
								</div>
								<div class="security_valid signup_wrong_password">
									<i class="fas fa-exclamation-circle"></i> Those passwords didn't match. Try again!
								</div>
								<div class="text-center button-second-section signup_input_section">
									<button class="color-active login_btn" id="security_password">Change Password</button>
								</div>
							</div>
						</div>
						<div id="preference" class="tab-pane fade">
							<div style="width: 100%;">
								<div class="account_setting_part_1">
									<div class="squaredPre">
										<input type="checkbox" name="optionsRadios" id="pre1" value="other"  >
										<label for="pre1"></label>
								    </div> 
									<label>Hide total profit from public</label>
								</div>
								<div class="account_setting_part_1">
									<div class="squaredPre">
										<input type="checkbox" name="optionsRadios" id="pre2" value="other"  >
										<label for="pre2"></label>
								    </div> <label>Hide total wagered from public</label>
								</div>
								<div class="account_setting_part_1">
									<div class="squaredPre">
										<input type="checkbox" name="optionsRadios" id="pre3" value="other"  >
										<label for="pre3"></label>
								    </div> <label>Hide bets from public</label>
								</div>
								<div class="account_setting_part_1">
									<div class="squaredPre">
										<input type="checkbox" name="optionsRadios" id="pre4" value="other"  >
										<label for="pre4"></label>
								    </div> <label>Allow private messages from non friends</label>
								</div>
								<div class="text-center mt-2">
									<a class="color-black preference_btn">Update</a>
								</div>
							</div>
						</div>
						<div id="ignored_user" class="tab-pane fade" style="background-color: #191E24">
							<div class="flexible">
								<div class="account-info" style="width: calc(100% - 10px);">
									<div class="account_history_header" >
										<label class="mt-2 account_history_header_text">Action</label>
									</div>
									<div class="account_history_header">
										<label class="mt-2 account_history_header_text" >User</label>
									</div>
								</div>
							</div>
							<div class="flexible mt-2 scrollable ignore_user">
								<div class="account-info transaction_history" style="background-color: #191E24">
									<?php for ($i = 0;$i < 18;$i++){ ?>
										<div class="ignored_user_list">
											<label style="color:white;">Unignore</label>
											<label>User</label>
										</div>
									<?php } ?>

								</div>
							</div>
						</div>
						<div id="transaction" class="tab-pane fade">
							<div class="flexible">
								<div class="account-info">
									<div class="account_history_header">
										<label class="mt-2 account_history_header_text">Bets</label>	
									</div>
									<div class="account_history_header">
										<label class="mt-2 account_history_header_text color-inactive" >Transactions</label>	
									</div>
								</div>
							</div>
							<div class="flexible mt-2">
								<div class="account-info" style="background: none;">
									<div class="account_history_header_title mr-1">
										<label class="mt-2">GAME</label>	
									</div>
									<div class="account_history_header_title mr-1 ml-1">
										<label class="mt-2">ID</label>	
									</div>
									<div class="account_history_header_title mr-1 ml-1">
										<label class="mt-2">TIME</label>	
									</div>
									<div class="account_history_header_title mr-1 ml-1">
										<label class="mt-2">AMOUNT</label>	
									</div>
									<div class="account_history_header_title mr-1 ml-1">
										<label class="mt-2">PAYOUT</label>	
									</div>
									<div class="account_history_header_title ml-1">
										<label class="mt-2">PROFIT</label>	
									</div>
									
								</div>
							</div>
							<div class="flexible mt-2 scrollable transaction_table">
								<div class="account-info transaction_history">
									<?php for ($i = 0;$i < 18;$i++){ ?>
										<div class="account_bet_history">
											<label>RR (GROUP)</label>
											<label>RR-<span class="color-active">146398</span></label>
											<label class="account_time">12:30:00<br>SUN 11/12/18</label>
											<label class="one_line" style="color:white;">1000 <i class="fas fa-gem color-orange ml-1" style="padding-top:2px;"></i></label>
											<label class="one_line"><span class="color-active">15.23 </span>X</label>
											<label class="one_line" >+ 1523 <i class="fas fa-gem" style="padding-top:2px;"></i></label>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
                <div class="flexible account_margin_top_10"  style="transform: translate(-50%,0);left:50%;position:relative;">
                    <button class="modal_close_btn" data-dismiss ="modal" style="margin-bottom: 5px;">Close</button>
                </div>
			</div>
		</div>
	</div>
</div>

<!-- Signup modal - Step 1 -->
<div class="modal fade" id="signupmodal1" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered signup_modal_section" role="document" >
		<div class="modal-content p-0" style="background-color: transparent;border:none;">
			<div class="modal-body p-0 signup_modal_body">
				<div class="signup_modal1_top_left">
					<div class="text-center signup1_modal_logo">
						<img src="<?php echo base_url();?>assets/images/rrlogo.svg"/>
					</div>
					<div class="text-center login_modal_log_desc">
						<label class="color-orange">LOTS <label class="color-active" style="margin-bottom: 0;">of </label> CASH <label class="color-active" style="margin-bottom: 0;">to be</label> WON<label class="color-active" style="margin-bottom: 0;">, join in on the </label> FUN<label class="color-active" style="margin-bottom: 0;">!</label></label>
					</div>
					<div class="text-center login_signup">
						<span onclick="show_signup_modal()">Log In</span>
						<span class="active">Sign Up</span>
					</div>
					<div class="text-center pos-relative signup_input_section">
						<p class="color-active inputbox_desc">Username</p>
						<input id="signup_nickname" class="signup_input white_background"/>
					</div>
					<div class="signup_valid signup_valid_username">
						<i class="fas fa-exclamation-circle"></i> Username must be length of 2 - 15 characters
					</div>
					<div class="signup_valid signup_valid_username1">
						<i class="fas fa-exclamation-circle"></i> Username can only contain letters and numbers
					</div>
					<div class="signup_valid signup_valid_exist">
						<i class="fas fa-exclamation-circle"></i> Username already exists
					</div>
					
					<div class="text-center pos-relative signup_input_section">
						<p class="color-active inputbox_desc">Email</p>
						<input type="email" class="signup_input white_background" id="signup_email"/>
					</div>
					<div class="signup_valid signup_valid_email">
						<i class="fas fa-exclamation-circle"></i> Please enter a valid e-mail address
					</div>
					<div class="signup_valid signup_valid_emailexist">
						<i class="fas fa-exclamation-circle"></i> Email already exists
					</div>
					
					<div class="text-center pos-relative signup_input_section">
						<p class="color-active inputbox_desc">Password</p>
						<input type="password" class="signup_input white_background" id="signup_password"/>
						<label class="color-black show_pwd signup_show">Show</label>
						<i onclick="show_signup_password(1)" class="fas fa-eye-slash color-black signup_show_password signup_pass"></i>
					</div>
					<div class="signup_valid signup_valid_password">
						<i class="fas fa-exclamation-circle"></i> Please use 8 or more characters
					</div>
					<div class="signup_valid signup_valid_password_strong">
						<i class="fas fa-exclamation-circle"></i> Please use a stronger password
					</div>
					<div class="signup_valid signup_wrong_password">
						<i class="fas fa-exclamation-circle"></i> Those passwords didn't match. Try again!
					</div>
					<div class="text-center button-second-section signup_input_section">
						<button class="color-active font-caps login_btn signin_btn" id="button-to-second">Play now</button>
					</div>
					<div class="text-center signup_label">
						Already have an account? <a style="text-decoration: none; color: #F8BF60;" id="button-to-login-1">Login here</a>
					</div>
				</div>
				<div class="text-center signup_modal1_top_right">
					<div class="signup_star_section">
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
					</div>
					<div>
						<label class="welcome_bonus">YOUR WELCOME OFFER</label>
					</div>
					<div>
						<label class="x2">X2</label>
					</div>
					<div>
						<label class="color-active signup_desc"><label class="color-orange" style="margin-bottom: 0;">DOUBLE</label> YOUR FIRST FOUR DEPOSITS UP TO <label class="color-orange">$800</label></label>
					</div>
					<div>
						<label class="signup_bonus">+100 FREE SPINS!</label>
					</div>
					<div class="text-center">
						<span class="underline signup_bonus_term">bonus terms</span>
					</div>
					<div class="signup_star_section">
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
					</div>
				</div>
			</div>
			<div class="close_btn signup_close_btn">
				<label data-dismiss ="modal" class="color-active">X</label>
			</div>
			<div class="text-center color-inactive">
				<label style="font-size: 12px;margin-top: 5px;">By accessing the site I attest that I am at least 18 years old and have read the <a class="color-inactive" style="text-decoration: underline;">Terms & Conditions.</a></label>
			</div>
		</div>
	</div>
</div>

<!-- Signup modal - Step 2 -->
<div class="modal fade" id="signupmodal2" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered signup_modal_section" role="document">
		<div class="modal-content p-0" style="background-color: transparent;">
			<div class="modal-body p-0 signup_modal_body" >
				<div class="signup_modal1_top_left">
					<div class="text-center signup1_modal_logo">
						<img src="<?php echo base_url();?>assets/images/rrlogo.svg"/>
					</div>
					<div class="text-center login_modal_log_desc">
						<div class="color-active">The most <label class="color-orange">profitable</label> and <label class="color-orange">unique</label> gambling website</div>
					</div>
					<div class="text-center">
						<label class="welcome_user">WELCOME <label class="color-active" id="signup_modal2_name"></label></label>
					</div>
					<div class="text-center pos-relative">
						<input type="email" class="signup_input" id="signup2_email1" placeholder=" " />
						<label class="form-control-placeholder email1_put" for="signup2_email1">Enter your email</label>
					</div>
					
					<div class="text-center pos-relative">
						<input type="password" class="signup_input" id="signup_password2_1" placeholder=" " />
						<label class="form-control-placeholder pwd2_put" for="signup_password2_1">Password *</label>
					</div>
					<div class="text-center pos-relative">
						<input type="password" class="signup_input" id="signup_password2_2" placeholder=" " />
						<label class="form-control-placeholder pwd3_put" for="signup_password2_2">Re-enter password*</label><i onclick="show_signup_password(2)" class="fas fa-eye-slash color-inactive signup_show_password"></i>
					</div>
					<div class="color-inactive" style="position: relative;">
						<div class="squaredThree">
							<input type="checkbox" name="optionsRadios" id="checkOther2" value="other"  >
							<label for="checkOther2"></label>
					    </div> 
						<label class="signup_service">I want to receive free money, free spins, promotions and bonuses</label>
					</div>
					<div class="text-center button-second-section">
						<a class="color-active font-caps login_btn" id="button-to-third">Play now</a>
					</div>
					<div class="text-center signup_label">
						Already have an account? <a style="text-decoration: none; color: #F8BF60;" id="button-to-login-2">Login here</a>
					</div>
				</div>
				<div class="text-center signup_modal1_top_right">
					<div class="signup_star_section">
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
					</div>
					<div>
						<label class="welcome_bonus">YOUR WELCOME OFFER</label>
					</div>
					<div>
						<label class="x2">X2</label>
					</div>
					<div>
						<label class="color-active signup_desc"><label class="color-orange" style="margin-bottom: 0;">DOUBLE</label> YOUR FIRST FOUR DEPOSITS UP TO <label class="color-orange">$800</label></label>
					</div>
					<div>
						<label class="signup_bonus">+100 FREE SPINS!</label>
					</div>
					<div class="text-center">
						<span class="underline signup_bonus_term">bonus terms</span>
					</div>
					<div class="signup_star_section">
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
						<i class="fa fa-star color-orange singup_star"></i>
					</div>
				</div>
			</div>
			<div class="text-center color-inactive">
				<label style="font-size: 12px;margin-top: 5px;">By accessing the site I attest that I am at least 18 years old and have read the <a class="color-inactive" style="text-decoration: underline;">Terms & Conditions.</a></label>
			</div>
		</div>
	</div>
</div>

<!-- Signup modal - Step 3 -->
<div class="modal fade" id="signupmodal3" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="background-color: transparent;">
	<div class="modal-dialog modal-dialog-centered gift_modal" role="document">
		<div class="modal-content p-0" style="background-color: transparent;border: none;">
			<div class="modal-body p-0 gif_modal_body text-center">
				<div class="text-center" style="border-bottom:2px solid;">
					<label class="gift_modal_title">START YOUR ADVENTURE!</label>
				</div>
				<div class="text-center gift_modal_desc">
					<span class="font-caps color-orange">Welcome </span><span id="signupmodal3_name"></span>
				</div>
				<div class="text-center gift_modal_desc gift_modal_desc1">
					<label class="color-active">To start you off, we have gifted you:</label><br>
					<label class="color-orange">30 FREE SPINS</label><label class="color-active">!</label><br>
					<label class="color-active">In addition to that, we will DOUBLE</label><br>
					<label class="color-active">your first deposit up to </label><label class="color-orange">$100</label><label class="color-active">!</label><br>
					<label class="color-active">That's</label><label class="color-orange"> $100 FOR FREE</label><label class="color-active">!</label><br>
				</div>
				<div class="text-center gift_modal_desc">
					<span class="font-caps color-active">HOW TO PLAY</span>
				</div>
				<div class="text-left gift_modal_desc play_desc">
					<span class="color-active">STEP 1 - PRESS THE <span class="color-orange square-orange-btn">PLAY</span> BUTTON</span>
				</div>
				<div class="text-left gift_modal_desc play_desc">
					<label class="color-active">STEP 2 - IF <img src="<?php echo base_url();?>assets/images/white_gun.png" class="gift_gun"/> FIRES YOU </label><label class="color-orange">WIN</label><label class="color-active"> THE</label><br>
					<label>CURRENT VALUE OF THE WIN MULTIPLIER</label>
				</div>
				<div class="text-left gift_modal_desc play_desc gift_modal_desc1">
					<span class="color-active">STEP 3 - </span><span class="color-orange">$ PROFIT $</span><span class="color-active"> WIN OVER </span><span class="color-orange">x1,000,000</span><span class="color-active">!</span>
				</div>
				<div class="text-center gift_bottom">
					<div class="color-active chceck_part">
						<div class="squaredCheck">
							<input type="checkbox" name="optionsRadios" id="checkOther1" value="other"  >
							<label for="checkOther1"></label>
					    </div> 
						<label class="gift_service">I understand how to play and i'm ready to win!</label>
					</div>
					<span class="color-active font-caps" id="button-to-final">LET'S PLAY!</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="logoutmodal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="background-color: transparent;">
	<div class="modal-dialog modal-dialog-centered gift_modal" role="document">
		<div class="modal-content p-0" style="background-color: transparent;border: none;">
			<div class="modal-body p-0 accept_gif_body text-center">
				<div class="text-center" style="border-bottom:2px solid;">
					<label class="gift_modal_title">Are you sure you want to log out?</label>
				</div>
				<div class="logout_section">
					<div class="accept_modal_decline_btn">
						<a class="color-active font-caps" id="logout-cancel">Cancel</a>
					</div>
					<div class="accept_modal_accept_btn">
						<a class="color-active font-caps" id="logout-accept">Log out</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Login modal -->
<div class="modal fade background_transparent" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered background_transparent login_modal_container" role="document">
		<div class="modal-content p-0 background_transparent">
			<div class="modal-body p-0 login_modal_body">
				<div class="text-center login_modal_logo">
					<img src="<?php echo base_url();?>assets/images/rrlogo.svg"/>
				</div>
				<div class="text-center login_modal_log_desc">
					<label class="color-orange">LOTS <label class="color-active" style="margin-bottom: 0;">of </label> CASH <label class="color-active" style="margin-bottom: 0;">to be</label> WON<label class="color-active" style="margin-bottom: 0;">, join in on the </label> FUN<label class="color-active" style="margin-bottom: 0;">!</label></label>
				</div>
				
				<div class="text-center login_signup">
					<span class="active" >Log In</span>
					<span onclick="show_signup_modal()">Sign Up</span>
				</div>
				<form>
					<div class="text-center pos-relative signup_input_section" >
						<p class="color-active inputbox_desc">Username</p>
						<input type="text" class="signup_input white_background" id="login_name" name="username" autofocus="autofocus"/>
					</div>
					<div class="login_valid login_valid_username">
						<i class="fas fa-exclamation-circle"></i>  Enter a username
					</div>
					<div class="login_valid login_valid_nouser">
						<i class="fas fa-exclamation-circle"></i>  Username does not exist
					</div>
					<div class="login_valid login_valid_length">
						<i class="fas fa-exclamation-circle"></i>  Username must be length of 2 - 15 characters
					</div>
					<div class="text-center pos-relative signup_input_section">
						<p class="color-active inputbox_desc">Password</p>
						<input type="password" name="password" class="signup_input white_background" id="password" />
						<label onclick="show_login_password()" class="color-black show_pwd login_show">Show</label>
						<i onclick="show_login_password()" class="fas fa-eye-slash color-black signup_show_password login_show_passwd"></i>
					</div>
					<div class="login_valid login_valid_passwd">
						<i class="fas fa-exclamation-circle"></i>  Enter a password
					</div>
					<div class="login_valid login_valid_wrong_passwd">
						<i class="fas fa-exclamation-circle"></i>  Password incorrect!
					</div>
					<div class="text-center button-second-section signup_input_section">
						<button class="color-active font-caps login_btn" id="button-login">PLAY NOW</button>
					</div>
				</form>
				
				<div class="text-center login_modal_footer">
					<span class="text_orange_a" id="button-to-signup">Sign Up</span> or <span class="text_orange_a" id="button-forgot">Forgot password</span>?
				</div>
			</div>
			<div class="close_btn">
				<label data-dismiss ="modal" class="color-active">X</label>
			</div>
			<div class="text-center color-inactive login_desc">
				<label style="font-size: 12px;">By accessing the site I attest that I am at least 18 years old and have read the <a class="color-inactive" style="text-decoration: underline;">Terms & Conditions.</a></label>
			</div>
		</div>
	</div>
</div>

<!-- Deposit Modal -->
<div class="modal fade" id="depositmodal" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true" data-keyboard="false">
	<div class="modal-dialog modal-dialog-centered signup_modal_section" role="document">
		<div class="modal-content p-0" style="background-color: transparent;">
			<div class="modal-body p-0" style="position: relative;" >
				<div class="text-center depositmodal_left_side depositmodal_section">
					<label class="color-active mt-2 font-size-20">Deposit Method</label><br>
					<div class="float-left ml-4 deposit_menu" onclick="deposit_menu(1);" id="deposit_crypto">
						Cryptocurrency
					</div>
					<div class="float-left ml-4 color-inactive deposit_menu" onclick="deposit_menu(2);" id="deposit_vgo">
						VGO skins
					</div>
				</div>
				<div class="depositmodal_center">
					
				</div>
				<div class="depositmodal_right_side depositmodal_section">
					<div style="border-bottom:2px solid #F8BF60;padding:5px 0;">
						<ul class="nav nav-tabs font-size-20">
							<li class="nav-item">
								<a data-target="#withdraw" data-toggle="tab" class="nav-link small color-active text-center" style="font-weight: 500;">WITHDRAW</a>
							</li>
							<li class="nav-item">
								<a data-target="#deposit" data-toggle="tab" class="nav-link small color-active active show text-center" style="font-weight: 500;">DEPOSIT</a>
							</li>
							<li class="nav-item color-active">
								<a data-target="#history" data-toggle="tab" class="nav-link small color-active text-center" style="font-weight: 500;">HISTORY</a>
							</li>
						</ul>
						
					</div>
					<div>
						<div id="withdraw" class="tab-pane fade">
							
						</div>
						<div id="crypto_deposit" class="tab-pane fade" style="display: none;">
							<div style="border-bottom:2px solid #F8BF60;">
								<div class="text-center" style="margin:5px 0;with:100%;background-color:#F8BF60;">
									<label class="color-black" style="margin:auto;">100% EXTRA DEPOSIT BONUS ACTIVATED (MIN 10,000 <i class="fas fa-gem ml-1"></i>)</label>
								</div>
							</div>
							<div style="border-bottom:2px solid #F8BF60;">
								<img src="<?php echo base_url();?>assets/images/demo.png" style="width:100%;"/>
							</div>
							<div class="text-center" style="border-bottom:2px solid #F8BF60;padding-top:10px;">
								<label class="color-active margin-bottom-0">Deposit <input class="color-orange" style="background-color: #1c2127;border:none;"/><label class="color-orange">₿</label> and receive <label class="color-orange">15,312 <i class="fas fa-gem"></i></label> in your account</label><br>
								<label class="color-inactive margin-bottom-0">Your personal Bitcoin address is</label><br>
								<input id="my_bitcoin_address" value="2342323232323232323232" readonly>
								<label id="my_bitcoin_address_btn" onclick="copy_bitcoin_address()">COPY TO CLIPBOARD</label><br>
								<label style="font-size:12px;" class="color-inactive">Deposits are credited within 1 confirmation depending on risk.Most deposits are credited within 30 seconds.</label><br>
								<a class="color-orange" style="text-decoration:underline;">SHOW QR CODE</a>
							</div>
							<div class="text-center" style="padding-top:10px;padding-bottom:20px;">
								<label class="color-inactive">OR</label><br>
								<a data-dismiss="modal" style="display: block; margin-left: auto; margin-right: auto; padding: 5px; font-size: 2vh; width: 25%; font-weight: bold; color: black; background-color: #f8bf60; -moz-box-shadow: 0px 4px 0px 0px #907548; -webkit-box-shadow: 0px 4px 0px 0px #907548; box-shadow: 0px 4px 0px 0px #907548; text-decoration: none;">Deposit with altcoins</a>
							</div>
						</div>
						<div id="vgo_deposit" class="tab-pane fade" style="display: none;">
							<div style="border-bottom:2px solid #F8BF60;">
								<div class="text-center" style="margin:5px 0;with:100%;background-color:#F8BF60;">
									<label class="color-black" style="margin:auto;">100% EXTRA DEPOSIT BONUS ACTIVATED (MIN 1,000 <i class="fas fa-gem ml-1"></i>)</label>
								</div>
							</div>
							<div style="border-bottom:2px solid #F8BF60;">
								<img src="<?php echo base_url();?>assets/images/vgo_temp.png" style="width:100%;"/>
							</div>
							<div class="row" style="height: 600px;background-color:#20252b;margin-left:0px;margin-right:0px;">
								<div class="col-sm-8" style="padding-top:10px;">
									<div style="background-color:#282a30;width:38%;float:left;">
										<i class="fas fa-search ml-2 color-inactive" style="width:13px;"></i><input type="text" placeholder="SEARCH FOR A SKIN..." style="border:none;font-size:10px;background:transparent;color:#a3a09b;" class="ml-2"/>
									</div>
									<div style="width:2%;float:left;height:24px;">
										
									</div>
									<div style="background-color:#282a30;width:60%;float:left;height:24px;">
										
									</div>
									<div class="text-center" style="width:100%;margin-top:30px;">
										<div id="vgo_item_section">
											
										</div>
										
									</div>
								</div>
								<div class="col-sm-4" style="padding-top:10px;">
									<div style="background-color:#282a30;">
										<label class="color-active text-left ml-2 margin-bottom-0" style="font-size:12px;">SELECTED SKINS</label>
										<label class="color-inactive float-right mr-2 margin-bottom-0" style="font-size:12px;margin-top:5px;">REMOVE ALL<i class="fa fa-trash"></i></label>
									</div>
									<div class="selected_vgo_skin_section mt-2" style="height: 417px;background-color:#282a30;">
										
									</div>
									<div class="text-center selected_vgo_skin_total_section mt-2" style="height: 70px;background-color:#282a30;">
										<span style="font-size:12px;margin-bottom:0;">TOTAL SKINS: 15</span><br>
										<span style="font-size:12px;margin-bottom:0;line-height:1;">TOTAL VALUES: 3,685 <i class="fas fa-gem"></i></span><br>
										<span style="font-size:12px;margin-bottom:0;line-height:1;">(1,755+175+1,755) BONUS</span>
									</div>
									<div class="text-center selected_vgo_skin_deposit_section mt-2" style="height: 50px;background-color:#282a30;text-align:center;padding-top:11px;">
										<a data-toggle="modal" data-target="#depositmodal" class="color-active font-caps top_header_btn">DEPOSIT SKINS</a>
									</div>
								</div>
							</div>
						</div>
						<div id="deposit" class="tab-pane fade active show">
							<div style="width:70%;border:2px solid #F8BF60;margin:auto;margin-bottom:20px;">
								<div class="row" style="margin:10px 0;">
									<div class="text-center col-sm-3" style="margin:auto;font-size:40px;">
										X2
									</div>
									<div class="col-sm-9">
										<label style="font-size:25px;">100% bonus up to $200</label><br>
										<label class="color-active margin-bottom-0">This is your personal welcome bonus.</label><br>
										<label class="color-active margin-bottom-0">Active this bonus to receive a 100% deposit bonus</label><br>
										<label class="color-active margin-bottom-0">(Deposit $100 and receive $200)</label><br>
										<label class="color-active margin-bottom-0">to DOUBLE your first deposit.</label><br>
									</div>
								</div>
							</div>
							<div class="text-center">
								<a data-target="#crypto_deposit" data-toggle="tab" class="depositmodal_decline">Decline offer</a>
								<a data-target="#crypto_deposit" data-toggle="tab" class="depositmodal_activate">ACTIVATE FREE MONEY</a>
							</div>
						</div>
						
						<div id="tradeUrl" class="tab-pane fade" style="display: none;">
							<div class="text-center" style="border-bottom:2px solid #F8BF60;padding-top:10px;padding-bottom:10px;">
								<label class="color-active" style="font-size:25px;">Your personal OPSKINS trade url</label><br>
								<input class="color-orange" style="background-color: #1c2127;border:none;margin-bottom:20px;font-size:20px;width:50%;" id="user_trade_url"/><br>
								<a id="trade_url_confirm">OK</a>
							</div>
						</div>
						
						<div id="history" class="tab-pane fade">
							
						</div>
					</div>
				</div>
				<a data-dismiss="modal" style="text-decoration: none;">
					<img src="<?php echo base_url();?>assets/images/close.png" class="depositmodal_close"/>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="info_section">
	<div class="info_body text-center">
		<div class="info_header">
			<span>GAME INFO</span>
		</div>
		<div class="pr-3 info_close" onclick="close_infomodal()">
			X
		</div>
		<div class="info_main">
			<div class="info_title">
				HOW TO PLAY
			</div>
			<div class="info_desc">
				To play a game, press the PLAY button. Alternatively press the keyboard SPACE BAR.<br>
				To increase or decrease a wager amount, press the up arrow or down arrow next to the wager number.
			</div>
			<div class="info_title">
				HOW TO WIN
			</div>
			<div class="info_desc">
				When the REVOLVER fires a shot, a WIN occurs. All players that in the round will have their wager multiplied by the MULTIPLIER displayed above the gun. After any additional WINS and MULTIPLIERS, the players will receive the TOTAL WIN AMOUNT.
			</div>
			<div class="info_title">
				AUTOPLAY
			</div>
			<div class="info_desc">
				To play a number of games in succession, press the AUTOPLAY button, then select the number of games to play.<br>
To cancel AUTOPLAY at any time, press the STOP AUTOPLAY button.
			</div>
			<div class="info_title">
				QUICKPLAY (solo only)
			</div>
			<div class="info_desc">
				To skip certain animations during a game, press the SKIP button if available.<br>
				Alternatively, press the keyboard SPACE BAR.
			</div>
			<div class="info_title">
				FREE GAMES FEATURE
			</div>
			<div class="info_desc">
				If the BONUS ROLL rolls a BONUS, the players will receive a random amount of free spins. The player can win up to 3x the maximum amount of free spins if the BONUS ROLL rolls “BONUS + 3x FREE SPINS”.<br><br>
If the BONUS is won, the game will play through the amount of FREE SPINS rounds won, automatically or manually (SOLO).<br>
The BONUS will have an additional BONUS MULTIPLIER, which will be multiplied with the NORMAL MULTIPLIER to give a huge TOTAL MULTIPLIER during the BONUS rounds.<br><br> At the end of the BONUS, the TOTAL WIN AMOUNT will be totalled up, displayed and given to all players that won the BONUS.
			</div>
			<div class="info_title">
				DOUBLE GUN FEATURE
			</div>
			<div class="info_desc">
				Before the REVOLVER fires, a second REVOLVER can appear, then each REVOLVER will fire separately. If both REVOLVERS fire then the WIN MULTIPLIER displayed above will be DOUBLED!
			</div>
			<div class="info_title">
				RANDOMIZATION/FAIRNESS
			</div>
			<div class="info_desc">
				EVERY game’s result is generated with a FAIR and equal chance of a win. The game’s results can NEVER be manipulated or tampered with by ANY person.<br>ANY individual can VERIFY that each game is fair for themselves by checking that the hash of the previous game is the SHA256 hash of the game before that.<br>
For more information go to russianroulette.gg/faq
			</div>
			<div class="info_title">
				<table class="info_modal_table">
					<thead>
						<tr style="border-bottom: 1px solid white;">
							<th style="width:25%;">ITEM</th>
							<th style="width:75%;">FUNCTION</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><div class="modal_play_btn">PLAY</div></td>
							<td class="info_desc">Click to start a game round with the current wager(alternatively press the SPACE BAR)</td>
						</tr>
						<tr>
							<td><div class="modal_autoplay_btn"><i class="fas fa-sync-alt" style="color:black;"></i></div></td>
							<td class="info_desc">Click to select the number of spins to AUTOPLAY and play the game automatically.</td>
						</tr>
						<tr>
							<td class="text-center"><i class="fas fa-question color-active"></i></td>
							<td class="info_desc">Click to view information about the game.</td>
						</tr>
						<tr>
							<td class="text-center"><i class="fas fa-volume-up color-active"></i></td>
							<td class="info_desc">Click to mute the game sounds or use the slider to adjust the sound volume.</td>
						</tr>
						<tr>
							<td class="text-center"><i class="fas fa-exchange-alt color-active"></i></td>
							<td class="info_desc">Click to change the layout of the game</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="info_desc">
				In the event of malfunction of gaming hardware/software, all affected game bets and payouts are rendered void and all affects bets refunded.<br><br>
				Version 1.0.00 01-01-2000
			</div>
		</div>
	</div>
</div>